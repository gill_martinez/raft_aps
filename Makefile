GGO=src/main.ggo

all: build

build: src/*
	mkdir -p build
	gengetopt -i ${GGO} -F cmdline 
	mv cmdline* src/common/
	cd build/ && cmake ../ && make 
	@echo "Build Raft: Done"

clean:
	rm -f *~ src/*~ bin/*~ src/*/*~ 
	rm build -rf


