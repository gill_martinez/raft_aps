#!/bin/sh

CODETORUN=${PWD}/raft/bin/raft
DATA=/apps/IMX/ReconSoft/teste/dado/

INISLICE=0
ENDSLICE=2047

NEXEC=10

NBLOCKS=6
BLOCKSIZE=(5 10 15 20 25 30)

GPUS=(0 3 6 7)
NUMGPUS=4


echo "##################################################################"
echo "Slant-stack: check/full"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --check)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_check.dat
rm -rf column*

echo "##################################################################"
echo "BST: check/full"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --check --bst)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_bst_check.dat
rm -rf column*
#########################################################################

NEXEC=3

echo "##################################################################"
echo "Slant-stack: full"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --doNotSave)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_full.dat
rm -rf column*

echo "##################################################################"
echo "Slant-stack: doNotCenter"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --doNotCenter --doNotSave)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_doNotCenter.dat
rm -rf column*

echo "##################################################################"
echo "Slant-stack: doNotRings"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --doNotRings --doNotSave)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_doNotRings.dat
rm -rf column*

echo "##################################################################"
echo "Slant-stack: doNotCenteRings"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --doNotCenter --doNotRings --doNotSave)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_doNotCenteRings.dat
rm -rf column*

echo "##################################################################"
echo "BST: full"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --doNotSave --bst)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_full.dat
rm -rf column*

echo "##################################################################"
echo "BST: doNotCenter"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --doNotCenter --doNotSave --bst)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_doNotCenter.dat
rm -rf column*

echo "##################################################################"
echo "BST: doNotRings"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --doNotRings --doNotSave --bst)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_doNotRings.dat
rm -rf column*

echo "##################################################################"
echo "BST: doNotCenteRings"
echo "" > times.dat
rm -rf column*

cmd=""

for((k=0; k<${NBLOCKS};k++)); do

    c=0

    for((m=1; m <${NUMGPUS}+1; m++ )); do

	List="${GPUS[@]:0:$m}"
	mygpus=$(echo ${List} | sed 's/\>/,/g;s/,$//') 
	GpuList=${mygpus//[[:blank:]]/}
	
	for((e=0; e<${NEXEC}; e++)); do
	    #print command-line:
	    #echo "${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} "
    
	    output=$( ${CODETORUN} -p ${DATA} -i ${INISLICE} -f ${ENDSLICE} -b "${BLOCKSIZE[$k]}" -c 2 -d 3 -o 110 -g ${GpuList} --doNotCenter --doNotRings --doNotSave --bst)
	    elapsed=$(echo ${output} | awk '{print $4}')

	    echo "GPU's:" ${GpuList} "- Execution:" $e "Time:" ${elapsed}
	    echo ${elapsed} >> column$k
	done
    done    
    cmd=$(echo ${cmd} column$k)    
done

paste ${cmd} > times_ss_doNotCenteRings.dat
rm -rf column*




