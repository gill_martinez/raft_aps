#ifndef RAFT_TYPES_H
#define RAFT_TYPES_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <hdf5.h>
#include <fftw3.h>
#include <cuda.h>
#include <cufft.h>
#include <cuComplex.h>
#include <cuda_runtime.h>
#include <helper_cuda.h>

extern "C" {
#include "cmdline.h"
#include <time.h>
}

#include "../pipeline/raft_gpu_ids.h"

/* The TPBX, TPBY and TPBZ  are parameters that can be changed and may interfere the performance */
#define TPBX 16
#define TPBY 16
#define TPBZ 4

#define DATASET  "images"
//#define DATASET "exchange/data"
#define DATADARK "darks"
#define DATAFLAT "flats"
#define DISP 0

#define PI 3.141592653589793238462643383279502884

#define NUMBEROFSTREAMS 20
#define NTHREADS 8
#define SEARCH4CENTER 256

#define MIN( a, b ) ( ( ( a ) < ( b ) ) ? ( a ) : ( b ) )
#define SQUARE(x) ((x)*(x))
#define RAFTMAX( x, y ) ( ( ( x ) > ( y ) ) ? ( x ) : ( y ) )
#define SIGN( x ) ( ( ( x ) > 0.0 ) ? 1.0 : ( ( ( x ) < 0.0 ) ? -1.0 : 0.0 ) )
#define FMATRIX(array, i, j, CSTRIDE) array[i*CSTRIDE + j]
#define FBLOCK(array,k,i,j,CSTRIDE,OFFSET) array[k*OFFSET+i*CSTRIDE+j]

#define BILLION 1E9
#define CLOCK  CLOCK_REALTIME
#define TIME(End,Start) (End.tv_sec - Start.tv_sec) + (End.tv_nsec-Start.tv_nsec)/BILLION

typedef struct{
  
  float *conv, *conv_, *mean, *zeros, *sum;
  float *kernel, *flip_kernel;
  float *cgm_x[2], *cgm_r, *cgm_z, *cgm_w;
  float *alpha;

}devRing;


typedef struct{

  int   maxiter;
  int   niter;
  int   length_kernel;
  float eps;
  float *conv, *conv_, *mean, *zeros, *sum;
  float *kernel, *flip_kernel;
  float *cgm_x, *cgm_r, *cgm_z, *cgm_w;
  float *sinoTemp, *sinoTemp2;
  float minValue;
  int withBlock;

  devRing block;

}raft_ring;


typedef struct {

  float *radon;

  cudaChannelFormatDesc channelDesc_tomo;
  cudaChannelFormatDesc channelDesc_flatAfter;
  cudaChannelFormatDesc channelDesc_flatBefore;
  cudaChannelFormatDesc channelDesc_dark;

  cudaArray *dst;
  cudaChannelFormatDesc channelDesc;
  cudaTextureObject_t   textureObject;

  cudaArray *flatAfterArray;
  cudaArray *flatBeforeArray;
  
  cudaArray *darkArray;
  cudaArray *tomoArray;

  cudaTextureObject_t flatAfterTexture;
  cudaTextureObject_t flatBeforeTexture;
  cudaTextureObject_t darkTexture;
  cudaTextureObject_t tomoTexture;

}radonGPU;

typedef struct{

  float *backp;

  cudaChannelFormatDesc channelDesc;
  cudaArray            *dst;
  cudaTextureObject_t   TextureObject;

}backpGPU;


typedef struct{

  float *iter;
  float *div;
  float *prod;
  float *e, *w;
  float *radon, *back;

}emGPU;

typedef struct{

  fftw_complex *in, *in_;
  fftw_complex *out, *out_;
  fftw_complex *fkernel;

  fftw_plan plan_forw, plan_forw_;
  fftw_plan plan_back, plan_back_;

  cufftComplex *SinoCU;
  cufftHandle SinoCUplan;
  
}fft;


typedef struct{
  
  int nrays;
  int nangles;
  int sizeImage;
  int blockSize;
  int zpad;
  int paddingCoeff;
  float pi;
  float dsigma;
  float beta;
  
  /*host pointers*/
  float *sigma;
  cufftComplex *d_sino_polar;
  
  /*device pointers*/
  cufftComplex *d_block_zpad;
  float *d_reconstruct_block;
  cufftComplex *d_cartesian_block;
  float *d_sigma;
  float *dev_mesh_theta;
  
  /*fourier plans*/
  cufftHandle plan_R2C,plan_C2R;

  /*texture stuff*/

  cudaArray *dst[2];
  cudaChannelFormatDesc channelDesc[2];
  cudaTextureObject_t   textureObject[2];
  
}bst;


typedef struct {

  hid_t file,dataspace,dataset,xfer_plist;
  int nblock;
  int blockSize;
  int Nangles, Nrays, Nslices;
  int li,ls;
  int sizeImage;
  int sliceInit;
  int sliceFinal;
  char *path;
  char *outputPath;
  int numberOfstreams, streamSize;
  
  /*MPI*/
  int processId;
  int qtdProcess;
  int processNumberBlock;
  int blocksLastProcess;
  
  float dt, dth, dxy, tmin, tmax, th0, thf, xymin;

  radonGPU data;  // gpu memory to build sinograms
  backpGPU backp; // gpu memory to build backprojection
  emGPU em;       // gpu memory for EM (emission)

  float *tomo, *dark, *flatAfter, *flatBefore;
  float *blockRecon;
  float *blockSino;

  float *shift;
  
  float *A, *B, *TA, *TB;
  float reg;
  fft   fourier;
  float threshold;
  int   numerical;
  int   ringComposition;
  int   ringBlocks;
  int   searchForCenter;
  int   hybrid;
  int   numberofblocks;
  float *centerblock;
  float **kernel;

  gpu_ids_t* gpus;
  int gpuId;
  
  bool  printTiming, printTimingK;
  bool  fromSinogram, doNotCenter, doNotRings, doNotSave;

  float offset;
  int axis;
  int zpad;
  int measured_nrays;
  int measured_nangles;  
  int numberOfIterations;
  
  raft_ring planRings;

  bst *bst;

  float elapsedTimes[50];

}raft_tomo;

enum{
  RAFT_CONV_SAME = 0,
  RAFT_CONV_FULL = 1
};


#endif // #ifndef RAFT_TYPES_H

