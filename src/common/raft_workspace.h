#ifndef RAFT_WORKSPACE_H
#define RAFT_WORKSPACE_H

#include "raft_types.h"

__host__ void raft_destroy_workspace(raft_tomo workspace);

__host__ void raft_create_workspace(raft_tomo *workspace,
				    float tmin,
				    float tmax,
				    float xymin,
				    float th0,
				    float thf,
				    int Nrays,
				    int Nangles,
				    int Nslices,
				    int sizeImage,
				    int blockSize,
				    int sliceInit,
				    int sliceFinal,
				    int numberOfstreams,
				    hid_t file,
				    hid_t dataspace,
				    hid_t dataset,
				    hid_t xfer_plist,
				    char *path,
				    char *outputPath,
				    float reg,
				    float threshold,
				    int numerical,
				    int ringComposition,
				    int ringBlocks,
				    int hybrid,
				    float offset,
				    int axis,
				    int zpad,
				    int nblocks,
				    int processNumberBlock,
				    gpu_ids_t* gpus,
				    int maxGpuPerNode,
				    int numberOfIterations,
				    bool printTiming,
				    bool printTimingK,
				    bool fromSinogram,
				    bool doNotCenter,
				    bool doNotRings,
				    bool doNotSave);


#endif // RAFT_WORKSPACE_H

