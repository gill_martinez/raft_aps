#ifndef RAFT_MEMORY_H
#define RAFT_MEMORY_H

#include "../common/raft_types.h"


__host__ void raft_initialize_texture_description(cudaTextureDesc* textureDescription);

__host__ void raft_create_texture_object_for_array(
        cudaTextureObject_t* textureObject, cudaArray* array);

__host__ void raft_load_shared_gpu_data(raft_tomo* workspace);

#endif // RAFT_MEMORY_H
