#ifndef RAFT_SAVE_H
#define RAFT_SAVE_H

#include "raft_types.h"


__host__ void raft_save_backprojection_block(raft_tomo* workspace);


#endif // RAFT_SAVE_H

