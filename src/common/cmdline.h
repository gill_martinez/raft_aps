/** @file cmdline.h
 *  @brief The header file for the command line option parser
 *  generated by GNU Gengetopt version 2.22.6
 *  http://www.gnu.org/software/gengetopt.
 *  DO NOT modify this file, since it can be overwritten
 *  @author GNU Gengetopt by Lorenzo Bettini */

#ifndef CMDLINE_H
#define CMDLINE_H

/* If we use autoconf.  */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h> /* for FILE */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef CMDLINE_PARSER_PACKAGE
/** @brief the program name (used for printing errors) */
#define CMDLINE_PARSER_PACKAGE "raft_imx"
#endif

#ifndef CMDLINE_PARSER_PACKAGE_NAME
/** @brief the complete program name (used for help and version) */
#define CMDLINE_PARSER_PACKAGE_NAME "raft_imx"
#endif

#ifndef CMDLINE_PARSER_VERSION
/** @brief the program version */
#define CMDLINE_PARSER_VERSION "Jan.2017"
#endif

/** @brief Where the command line options are stored */
struct gengetopt_args_info
{
  const char *help_help; /**< @brief Print help and exit help description.  */
  const char *version_help; /**< @brief Print version and exit help description.  */
  char * path_arg;	/**< @brief Tomographic folder.  */
  char * path_orig;	/**< @brief Tomographic folder original value given at command line.  */
  const char *path_help; /**< @brief Tomographic folder help description.  */
  char * outpath_arg;	/**< @brief Reconstruction output folder (default=`<path>/recon').  */
  char * outpath_orig;	/**< @brief Reconstruction output folder (default=`<path>/recon') original value given at command line.  */
  const char *outpath_help; /**< @brief Reconstruction output folder (default=`<path>/recon') help description.  */
  int initial_arg;	/**< @brief Initial Slice.  */
  char * initial_orig;	/**< @brief Initial Slice original value given at command line.  */
  const char *initial_help; /**< @brief Initial Slice help description.  */
  int final_arg;	/**< @brief Final Slice.  */
  char * final_orig;	/**< @brief Final Slice original value given at command line.  */
  const char *final_help; /**< @brief Final Slice help description.  */
  int numerical_arg;	/**< @brief Numerical precision (default='1').  */
  char * numerical_orig;	/**< @brief Numerical precision original value given at command line.  */
  const char *numerical_help; /**< @brief Numerical precision help description.  */
  float max_arg;	/**< @brief Threshold for reconstruction (default='50').  */
  char * max_orig;	/**< @brief Threshold for reconstruction original value given at command line.  */
  const char *max_help; /**< @brief Threshold for reconstruction help description.  */
  int block_arg;	/**< @brief Blocksize (default='10').  */
  char * block_orig;	/**< @brief Blocksize original value given at command line.  */
  const char *block_help; /**< @brief Blocksize help description.  */
  float reg_arg;	/**< @brief Regularization factor for FBP (default='0').  */
  char * reg_orig;	/**< @brief Regularization factor for FBP original value given at command line.  */
  const char *reg_help; /**< @brief Regularization factor for FBP help description.  */
  int threads_arg;	/**< @brief Threads (default='8').  */
  char * threads_orig;	/**< @brief Threads original value given at command line.  */
  const char *threads_help; /**< @brief Threads help description.  */
  int size_arg;	/**< @brief Reconstruction size (default='2048').  */
  char * size_orig;	/**< @brief Reconstruction size original value given at command line.  */
  const char *size_help; /**< @brief Reconstruction size help description.  */
  int comp_arg;	/**< @brief Ring Composition (default='1').  */
  char * comp_orig;	/**< @brief Ring Composition original value given at command line.  */
  const char *comp_help; /**< @brief Ring Composition help description.  */
  int div_arg;	/**< @brief Ring division by block of angles (default='1').  */
  char * div_orig;	/**< @brief Ring division by block of angles original value given at command line.  */
  const char *div_help; /**< @brief Ring division by block of angles help description.  */
  int* gpu_arg;	/**< @brief GPU device indexes.  */
  char ** gpu_orig;	/**< @brief GPU device indexes original value given at command line.  */
  unsigned int gpu_min; /**< @brief GPU device indexes's minimum occurreces */
  unsigned int gpu_max; /**< @brief GPU device indexes's maximum occurreces */
  const char *gpu_help; /**< @brief GPU device indexes help description.  */
  int hybrid_arg;	/**< @brief Hybrid version (CPU/GPU/GPU-Only/360) (default='0').  */
  char * hybrid_orig;	/**< @brief Hybrid version (CPU/GPU/GPU-Only/360) original value given at command line.  */
  const char *hybrid_help; /**< @brief Hybrid version (CPU/GPU/GPU-Only/360) help description.  */
  float offset_arg;	/**< @brief Offset for centering sinograms (default='0').  */
  char * offset_orig;	/**< @brief Offset for centering sinograms original value given at command line.  */
  const char *offset_help; /**< @brief Offset for centering sinograms help description.  */
  int axis_arg;	/**< @brief Rotation Axis for 360 degree scans (used only with -h 1).  */
  char * axis_orig;	/**< @brief Rotation Axis for 360 degree scans (used only with -h 1) original value given at command line.  */
  const char *axis_help; /**< @brief Rotation Axis for 360 degree scans (used only with -h 1) help description.  */
  int zero_arg;	/**< @brief Zero Padding (default='0').  */
  char * zero_orig;	/**< @brief Zero Padding original value given at command line.  */
  const char *zero_help; /**< @brief Zero Padding help description.  */
  int iterations_arg;	/**< @brief Number of iterations (used only with -h 2).  */
  char * iterations_orig;	/**< @brief Number of iterations (used only with -h 2) original value given at command line.  */
  const char *iterations_help; /**< @brief Number of iterations (used only with -h 2) help description.  */
  const char *timing_help; /**< @brief Print out timing measurements help description.  */
  const char *ktiming_help; /**< @brief Print out timing measurements for CPU/GPU kernel help description.  */
  const char *sinogram_help; /**< @brief Reconstruct from sinogram data help description.  */
  const char *check_help; /**< @brief Check GPU usage help description.  */
  const char *bst_help; /**< @brief Backprojection with BST  help description.  */
  const char *doNotCenter_help; /**< @brief No centering function help description.  */
  const char *doNotRings_help; /**< @brief No ring filtering function help description.  */
  const char *doNotSave_help; /**< @brief Not saving data help description.  */
  
  unsigned int help_given ;	/**< @brief Whether help was given.  */
  unsigned int version_given ;	/**< @brief Whether version was given.  */
  unsigned int path_given ;	/**< @brief Whether path was given.  */
  unsigned int outpath_given ;	/**< @brief Whether outpath was given.  */
  unsigned int initial_given ;	/**< @brief Whether initial was given.  */
  unsigned int final_given ;	/**< @brief Whether final was given.  */
  unsigned int numerical_given ;	/**< @brief Whether numerical was given.  */
  unsigned int max_given ;	/**< @brief Whether max was given.  */
  unsigned int block_given ;	/**< @brief Whether block was given.  */
  unsigned int reg_given ;	/**< @brief Whether reg was given.  */
  unsigned int threads_given ;	/**< @brief Whether threads was given.  */
  unsigned int size_given ;	/**< @brief Whether size was given.  */
  unsigned int comp_given ;	/**< @brief Whether comp was given.  */
  unsigned int div_given ;	/**< @brief Whether div was given.  */
  unsigned int gpu_given ;	/**< @brief Whether gpu was given.  */
  unsigned int hybrid_given ;	/**< @brief Whether hybrid was given.  */
  unsigned int offset_given ;	/**< @brief Whether offset was given.  */
  unsigned int axis_given ;	/**< @brief Whether axis was given.  */
  unsigned int zero_given ;	/**< @brief Whether zero was given.  */
  unsigned int iterations_given ;	/**< @brief Whether iterations was given.  */
  unsigned int timing_given ;	/**< @brief Whether timing was given.  */
  unsigned int ktiming_given ;	/**< @brief Whether ktiming was given.  */
  unsigned int sinogram_given ;	/**< @brief Whether sinogram was given.  */
  unsigned int check_given ;	/**< @brief Whether check was given.  */
  unsigned int bst_given ;	/**< @brief Whether bst was given.  */
  unsigned int doNotCenter_given ;	/**< @brief Whether doNotCenter was given.  */
  unsigned int doNotRings_given ;	/**< @brief Whether doNotRings was given.  */
  unsigned int doNotSave_given ;	/**< @brief Whether doNotSave was given.  */

} ;

/** @brief The additional parameters to pass to parser functions */
struct cmdline_parser_params
{
  int override; /**< @brief whether to override possibly already present options (default 0) */
  int initialize; /**< @brief whether to initialize the option structure gengetopt_args_info (default 1) */
  int check_required; /**< @brief whether to check that all required options were provided (default 1) */
  int check_ambiguity; /**< @brief whether to check for options already specified in the option structure gengetopt_args_info (default 0) */
  int print_errors; /**< @brief whether getopt_long should print an error message for a bad option (default 1) */
} ;

/** @brief the purpose string of the program */
extern const char *gengetopt_args_info_purpose;
/** @brief the usage string of the program */
extern const char *gengetopt_args_info_usage;
/** @brief the description string of the program */
extern const char *gengetopt_args_info_description;
/** @brief all the lines making the help output */
extern const char *gengetopt_args_info_help[];

/**
 * The command line parser
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser (int argc, char **argv,
  struct gengetopt_args_info *args_info);

/**
 * The command line parser (version with additional parameters - deprecated)
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @param override whether to override possibly already present options
 * @param initialize whether to initialize the option structure my_args_info
 * @param check_required whether to check that all required options were provided
 * @return 0 if everything went fine, NON 0 if an error took place
 * @deprecated use cmdline_parser_ext() instead
 */
int cmdline_parser2 (int argc, char **argv,
  struct gengetopt_args_info *args_info,
  int override, int initialize, int check_required);

/**
 * The command line parser (version with additional parameters)
 * @param argc the number of command line options
 * @param argv the command line options
 * @param args_info the structure where option information will be stored
 * @param params additional parameters for the parser
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_ext (int argc, char **argv,
  struct gengetopt_args_info *args_info,
  struct cmdline_parser_params *params);

/**
 * Save the contents of the option struct into an already open FILE stream.
 * @param outfile the stream where to dump options
 * @param args_info the option struct to dump
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_dump(FILE *outfile,
  struct gengetopt_args_info *args_info);

/**
 * Save the contents of the option struct into a (text) file.
 * This file can be read by the config file parser (if generated by gengetopt)
 * @param filename the file where to save
 * @param args_info the option struct to save
 * @return 0 if everything went fine, NON 0 if an error took place
 */
int cmdline_parser_file_save(const char *filename,
  struct gengetopt_args_info *args_info);

/**
 * Print the help
 */
void cmdline_parser_print_help(void);
/**
 * Print the version
 */
void cmdline_parser_print_version(void);

/**
 * Initializes all the fields a cmdline_parser_params structure 
 * to their default values
 * @param params the structure to initialize
 */
void cmdline_parser_params_init(struct cmdline_parser_params *params);

/**
 * Allocates dynamically a cmdline_parser_params structure and initializes
 * all its fields to their default values
 * @return the created and initialized cmdline_parser_params structure
 */
struct cmdline_parser_params *cmdline_parser_params_create(void);

/**
 * Initializes the passed gengetopt_args_info structure's fields
 * (also set default values for options that have a default)
 * @param args_info the structure to initialize
 */
void cmdline_parser_init (struct gengetopt_args_info *args_info);
/**
 * Deallocates the string fields of the gengetopt_args_info structure
 * (but does not deallocate the structure itself)
 * @param args_info the structure to deallocate
 */
void cmdline_parser_free (struct gengetopt_args_info *args_info);

/**
 * Checks that all the required options were specified
 * @param args_info the structure to check
 * @param prog_name the name of the program that will be used to print
 *   possible errors
 * @return
 */
int cmdline_parser_required (struct gengetopt_args_info *args_info,
  const char *prog_name);


#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* CMDLINE_H */
