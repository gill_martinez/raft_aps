#include <stdlib.h>
#include <math.h>
#include <hdf5.h>

#include "../common/raft_types.h"
#include "raft_memory.h"

extern "C" {
#include <time.h>
}

/*--------------------------------------------
  Load shared data to GPU
  -------------------------------------------*/

__host__ void raft_load_shared_gpu_data(raft_tomo* workspace)
{
  int Nslices = workspace->Nslices;
  int Nrays = workspace->Nrays;
  
  cudaMemcpyToArray(workspace->data.flatBeforeArray,
		    0,
		    0,
		    workspace->flatBefore,
		    Nslices * Nrays * sizeof(float),
		    cudaMemcpyHostToDevice);
    
  cudaMemcpyToArray(workspace->data.flatAfterArray,
		    0,
		    0,
		    workspace->flatAfter,
		    Nslices * Nrays * sizeof(float),
		    cudaMemcpyHostToDevice);
    
  cudaMemcpyToArray(workspace->data.darkArray,
		    0,
		    0,
		    workspace->dark,
		    Nslices * Nrays * sizeof(float),
		    cudaMemcpyHostToDevice);
   
  raft_create_texture_object_for_array(&workspace->data.flatBeforeTexture,
				       workspace->data.flatBeforeArray);

  raft_create_texture_object_for_array(&workspace->data.flatAfterTexture,
				       workspace->data.flatAfterArray);

  raft_create_texture_object_for_array(&workspace->data.darkTexture,
				       workspace->data.darkArray);
}


__host__ void raft_initialize_texture_description(cudaTextureDesc* textureDescription) 
{
  const int FALSE = 0;
  
  textureDescription->addressMode[0] = cudaAddressModeBorder;
  textureDescription->addressMode[1] = cudaAddressModeBorder;
  textureDescription->addressMode[2] = cudaAddressModeBorder;
  textureDescription->filterMode = cudaFilterModeLinear;
  textureDescription->readMode = cudaReadModeElementType;
  textureDescription->normalizedCoords = FALSE;
  
  textureDescription->sRGB = FALSE;
  textureDescription->maxAnisotropy = 0;
  textureDescription->mipmapFilterMode = cudaFilterModePoint;
  textureDescription->mipmapLevelBias = 0.f;
  textureDescription->minMipmapLevelClamp = 0.f;
  textureDescription->maxMipmapLevelClamp = 0.f;
}

__host__ void raft_create_texture_object_for_array(cudaTextureObject_t* textureObject, 
						   cudaArray* array) 
{
  cudaResourceDesc resourceDescription;
  cudaTextureDesc textureDescription;
  
  resourceDescription.resType = cudaResourceTypeArray;
  resourceDescription.res.array.array = array;
  
  raft_initialize_texture_description(&textureDescription);
  
  cudaCreateTextureObject(textureObject, 
			  &resourceDescription,
			  &textureDescription, 
			  NULL);
}

