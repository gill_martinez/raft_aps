#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "raft_workspace.h"
#include "raft_types.h"
#include "../reconstruction/raft_rings.h"

/*--------------------------------------------------------
  Destroy workspace for reconstruction 
  --------------------------------------------------------*/

__host__ void raft_destroy_workspace(raft_tomo workspace)
{
  raft_rings_plan_destroy(&workspace.planRings);

  cudaDestroyTextureObject(workspace.data.flatBeforeTexture);
  cudaDestroyTextureObject(workspace.data.flatAfterTexture);
  cudaDestroyTextureObject(workspace.data.darkTexture);

  free(workspace.tomo);
  free(workspace.flatBefore);
  free(workspace.flatAfter);
  free(workspace.dark);
  free(workspace.shift);
  free(workspace.centerblock);

  free(workspace.A);
  free(workspace.B);
  free(workspace.TA);
  free(workspace.TB);

  fftw_destroy_plan( workspace.fourier.plan_forw );
  fftw_destroy_plan( workspace.fourier.plan_back );
  fftw_destroy_plan( workspace.fourier.plan_forw_ );
  fftw_destroy_plan( workspace.fourier.plan_back_ );
 
  fftw_free( workspace.fourier.in );
  fftw_free( workspace.fourier.out );
  fftw_free( workspace.fourier.fkernel );
  fftw_free( workspace.fourier.in_ );
  fftw_free( workspace.fourier.out_ );
  
  for(int i = 0; i < workspace.blockSize; i++)
    free( workspace.kernel[i] );
  
  free(workspace.kernel);

  destroyGpuIds(workspace.gpus);
  
  cudaDeviceReset();
}

/*--------------------------------------------------------
  Create workspace for reconstruction 
  --------------------------------------------------------*/

__host__ void raft_create_workspace(raft_tomo *workspace,
				    float tmin,
				    float tmax,
				    float xymin,
				    float th0,
				    float thf,
				    int Nrays,
				    int Nangles,
				    int Nslices,
				    int sizeImage,
				    int blockSize,
				    int sliceInit,
				    int sliceFinal,
				    int numberOfstreams,
				    hid_t file,
				    hid_t dataspace,
				    hid_t dataset,
				    hid_t xfer_plist,
				    char *path,
				    char *outputPath,
				    float reg,
				    float threshold,
				    int numerical,
				    int ringComposition,
				    int ringBlocks,
				    int hybrid,
				    float offset,
				    int axis,
				    int zpad,
				    int nblocks,
				    int processNumberBlock,
				    gpu_ids_t* gpus,
				    int maxGpuPerNode,
				    int numberOfIterations,
				    bool printTiming,
				    bool printTimingK,
				    bool fromSinogram,
				    bool doNotCenter,
				    bool doNotRings,
				    bool doNotSave)
{
  workspace->gpus = gpus;
  workspace->gpuId = gpus->gpuIds[0];
  workspace->gpus->maxGpuPerNode = maxGpuPerNode;
  workspace->fromSinogram = fromSinogram;
  workspace->doNotCenter = doNotCenter;
  workspace->doNotRings  = doNotRings;
  workspace->doNotSave   = doNotSave;
  workspace->processNumberBlock = processNumberBlock;

  for(int i=0; i < 50; i++)
    workspace->elapsedTimes[i]=0.0;

  if( hybrid == 1)
    {
      /*-------------*/
      /* 360 degrees */

      int _Nrays_;
      int _Nangles_;
      
      _Nrays_   = 2*axis;
      _Nangles_ = Nangles/2;	

      workspace->outputPath= outputPath;
      workspace->printTiming  = printTiming;
      workspace->printTimingK = printTimingK;
      workspace->hybrid = hybrid;
      workspace->measured_nrays   = Nrays;
      workspace->measured_nangles = Nangles;
      workspace->numberOfIterations = numberOfIterations;
      workspace->offset = offset;

      // plan for rings => create without blocksize: 0
      workspace->planRings =  raft_rings_plan_create(_Nrays_, _Nangles_, 0);
      
      workspace->searchForCenter = SEARCH4CENTER;
      
      workspace->file       = file;
      workspace->dataspace  = dataspace;
      workspace->dataset    = dataset;
      workspace->xfer_plist = xfer_plist;
      workspace->blockSize  = blockSize;
      workspace->Nangles    = Nangles;
      workspace->Nrays      = Nrays;
      workspace->Nslices    = Nslices;
      workspace->axis       = axis;
      workspace->zpad       = zpad;
      workspace->numberofblocks   = nblocks;

      workspace->centerblock     = (float *) malloc(sizeof(float) * workspace->searchForCenter);
      
      workspace->ringComposition = ringComposition;
      workspace->ringBlocks      = ringBlocks;
      
      workspace->threshold = threshold;
      workspace->numerical = numerical;
      workspace->reg       = reg;
      
      workspace->sizeImage = _Nrays_;

      workspace->sliceInit = sliceInit;
      workspace->sliceFinal= sliceFinal;
      workspace->path      = path;
      
      float dt = (tmax-tmin)/(float(workspace->measured_nrays));

      workspace->tmin      = - dt * axis;
      workspace->tmax      =   dt * axis;
      workspace->th0       = th0;
      workspace->thf       = thf;
      workspace->xymin     = - dt * axis;
      
      workspace->tomo = (float*)malloc(blockSize*Nangles*Nrays*sizeof(float));
      workspace->flatBefore = (float*)malloc(Nrays*Nslices*sizeof(float));
      workspace->flatAfter  = (float*)malloc(Nrays*Nslices*sizeof(float));
      workspace->dark = (float*)malloc(Nrays*Nslices*sizeof(float));
      
      workspace->A = (float *) malloc (_Nrays_*sizeof(float));
      workspace->B = (float *) malloc (_Nrays_*sizeof(float));
      workspace->TA = (float *) malloc (_Nrays_*sizeof(float));
      workspace->TB = (float *) malloc (_Nrays_*sizeof(float));
      
      workspace->dt  = (tmax - tmin)/(workspace->measured_nrays - 1);
      workspace->dth = ((workspace->thf - workspace->th0))/(_Nangles_ - 1);
      workspace->dxy = (tmax - tmin)/(workspace->measured_nrays - 1);
      
      workspace->shift = (float *) malloc ((_Nrays_)*sizeof(float));

      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////
      /// alloc stuff to build sinogram
      
      workspace->data.channelDesc_tomo       = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
      workspace->data.channelDesc_flatAfter  = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
      workspace->data.channelDesc_flatBefore = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
      workspace->data.channelDesc_dark       = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);

      /////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////
      ////// alloc stuff for backprojection 
      
      int streamSize = (int) ceil(blockSize/numberOfstreams);
      
      workspace->streamSize      = streamSize;
      workspace->numberOfstreams = numberOfstreams;
      
      workspace->data.channelDesc = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);

      //////////////////////////////////
      //////// FFT on CPU with FFTW3
      
      workspace->fourier.in   = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      workspace->fourier.out  = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      workspace->fourier.fkernel = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      
      workspace->fourier.in_   = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      workspace->fourier.out_  = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      
      workspace->fourier.plan_forw = fftw_plan_dft_1d( _Nrays_, 
						       workspace->fourier.in, 
						       workspace->fourier.out, 
						       FFTW_FORWARD,
						       FFTW_ESTIMATE);
      
      workspace->fourier.plan_back = fftw_plan_dft_1d( _Nrays_, 
						       workspace->fourier.in, 
						       workspace->fourier.out, 
						       FFTW_BACKWARD, 
						       FFTW_ESTIMATE);
      
      workspace->fourier.plan_forw_ = fftw_plan_dft_1d( _Nrays_, 
							workspace->fourier.in_, 
							workspace->fourier.out_, 
							FFTW_FORWARD,
							FFTW_ESTIMATE);
      
      workspace->fourier.plan_back_ = fftw_plan_dft_1d( _Nrays_, 
							workspace->fourier.in_, 
							workspace->fourier.out_, 
							FFTW_BACKWARD, 
							FFTW_ESTIMATE);
      

      //////////////////////////////////////////////////////
      ///////// RING CORRECTION MATRIX: Nrays x blocksize
      ///////// each column represents the correction vector
      ///////// of each sinogram of a given block
      
      workspace->kernel = (float **)malloc(sizeof(float *) * blockSize);
      for(int i = 0; i < blockSize; i ++)
	workspace->kernel[i] = (float *) malloc(sizeof(float) * _Nrays_);
    }
  else
    {
      /*-------------*/
      /* 180 degrees */

      int _Nrays_;
      
      _Nrays_ = Nrays + 2*zpad;
 
      workspace->outputPath= outputPath;
      workspace->printTiming  = printTiming;
      workspace->printTimingK = printTimingK;
      workspace->hybrid = hybrid;
      workspace->measured_nrays   = Nrays;
      workspace->measured_nangles = Nangles;
      workspace->numberOfIterations = numberOfIterations;
            
      workspace->searchForCenter = SEARCH4CENTER;
      workspace->centerblock     = (float *) malloc(sizeof(float) * workspace->searchForCenter);      

      //plan for rings => create without blocksize: 0
      workspace->planRings =  raft_rings_plan_create(_Nrays_, workspace->Nangles, 0);
      	          
      workspace->file      = file;
      workspace->dataspace = dataspace;
      workspace->dataset   = dataset;
      workspace->blockSize = blockSize;
      workspace->Nangles   = Nangles;
      workspace->Nrays     = Nrays;
      workspace->Nslices   = Nslices;
      workspace->axis      = axis;
      workspace->zpad      = zpad;
      workspace->numberofblocks = nblocks;

      workspace->ringComposition = ringComposition;
      workspace->ringBlocks      = ringBlocks;
      
      workspace->threshold = threshold;
      workspace->numerical = numerical;
      workspace->reg       = reg;
      
      workspace->sizeImage = sizeImage;
      workspace->sliceInit = sliceInit;
      workspace->sliceFinal= sliceFinal;
      workspace->path      = path;
      
      float dt = (tmax-tmin)/(float(workspace->measured_nrays-1));
      workspace->tmin      = tmin - dt * zpad;
      workspace->tmax      = tmax + dt * zpad;
      workspace->th0       = th0;
      workspace->thf       = thf;
      workspace->xymin     = tmin - dt * zpad;

      workspace->offset = offset; 
      
      workspace->tomo = (float*)malloc(blockSize*Nangles*Nrays*sizeof(float));
      workspace->flatBefore = (float*)malloc(Nrays*Nslices*sizeof(float));
      workspace->flatAfter  = (float*)malloc(Nrays*Nslices*sizeof(float));
      workspace->dark = (float*)malloc(Nrays*Nslices*sizeof(float));
      
      workspace->shift      = (float *) malloc ((_Nrays_)*sizeof(float));
      
      workspace->A = (float *) malloc(_Nrays_*sizeof(float));
      workspace->B = (float *) malloc(_Nrays_*sizeof(float));
      workspace->TA = (float *) malloc(_Nrays_*sizeof(float));
      workspace->TB = (float *) malloc(_Nrays_*sizeof(float));

      workspace->dt  = ((tmax-tmin))/((float)(workspace->measured_nrays - 1));
      workspace->dth = ((workspace->thf - workspace->th0))/(workspace->Nangles - 1);
      workspace->dxy = ((tmax-tmin))/((float)(workspace->measured_nrays - 1));
      
      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////
      /// alloc stuff to build sinogram @ gpu
            
      workspace->data.channelDesc_tomo = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
      workspace->data.channelDesc_flatBefore = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
      workspace->data.channelDesc_flatAfter  = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
      workspace->data.channelDesc_dark = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
      
      /////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////
      ////// alloc stuff for backprojection 
      
      int streamSize = (int) ceil(blockSize/numberOfstreams);
      
      workspace->streamSize      = streamSize;
      workspace->numberOfstreams = numberOfstreams;
      
      workspace->data.channelDesc = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
            
      //////////////////////////////////
      //////// FFT on CPU with FFTW3
      
      workspace->fourier.in   = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      workspace->fourier.out  = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      workspace->fourier.fkernel = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      
      workspace->fourier.in_   = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      workspace->fourier.out_  = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * _Nrays_ );
      
      workspace->fourier.plan_forw = fftw_plan_dft_1d( _Nrays_, 
						       workspace->fourier.in, 
						       workspace->fourier.out, 
						       FFTW_FORWARD,
						       FFTW_ESTIMATE);
      
      workspace->fourier.plan_back = fftw_plan_dft_1d( _Nrays_, 
						       workspace->fourier.in, 
						       workspace->fourier.out, 
						       FFTW_BACKWARD, 
						       FFTW_ESTIMATE);
      
      workspace->fourier.plan_forw_ = fftw_plan_dft_1d( _Nrays_, 
							workspace->fourier.in_, 
							workspace->fourier.out_, 
							FFTW_FORWARD,
							FFTW_ESTIMATE);
      
      workspace->fourier.plan_back_ = fftw_plan_dft_1d( _Nrays_, 
							workspace->fourier.in_, 
							workspace->fourier.out_, 
							FFTW_BACKWARD, 
							FFTW_ESTIMATE);
      
      //////////////////////////////////////////////////////
      ///////// RING CORRECTION MATRIX: Nrays x blocksize
      ///////// each column represents the correction vector
      ///////// of each sinogram of a given block
      
      workspace->kernel = (float **)malloc(sizeof(float *) * blockSize);
      for(int i = 0; i < blockSize; i ++)
	workspace->kernel[i] = (float *) malloc(sizeof(float) * _Nrays_);      
    }
 
  
}
