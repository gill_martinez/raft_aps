#include <stdlib.h>
#include <math.h>
#include <hdf5.h>

#include "raft_types.h"
#include "raft_save.h"

extern "C" {
#include <time.h>
}

__constant__ float TOL = 0.0001f;
__constant__ float TOL2 = 0.0000001f;


__host__ void raft_save_backprojection_block(raft_tomo* workspace)
{
  if(!workspace->doNotSave)
    {
      char fileName[100];
      char* fileNameNumberPosition;
      FILE* file;

      int firstSlice     = workspace->sliceInit;
      int currentBlock   = workspace->nblock;
      int slicesPerBlock = workspace->blockSize;
      int processId      = workspace->processId;
      int firstSliceInBlock;

      if(processId == 0)
	firstSliceInBlock = firstSlice + slicesPerBlock * currentBlock + ( processId * slicesPerBlock );
      else
	firstSliceInBlock = firstSlice + (workspace->blocksLastProcess * processId * slicesPerBlock) + (currentBlock * slicesPerBlock);

	
      int numSlices = workspace->sliceFinal;
      int lastSliceInBlock = firstSliceInBlock + slicesPerBlock - 1;

      int imageWidth = workspace->sizeImage;
      int imageSize = imageWidth * imageWidth;

      if (lastSliceInBlock >= numSlices)
	lastSliceInBlock = numSlices - 1;

      //strcpy(fileName, workspace->outputPath);

      strcpy(fileName, workspace->path);
      strcat(fileName, "/recon/slice_");

      fileNameNumberPosition = fileName + strlen(fileName);
  
      for (int slice = firstSliceInBlock; slice <= lastSliceInBlock; slice++) {

	int sliceInBlock = slice - firstSliceInBlock;

	sprintf(fileNameNumberPosition, "%d.b", slice);
	
	file = fopen(fileName, "w");

	if (workspace->numerical == 1) {
	  //////////////////////////////
	  ////// value for 8 bits
	  ////// (linear interpolation)

	  int idx;
	  char num;
	  float a, b, minvalue, maxvalue, deltavalue, truevalue, bit8;

	  minvalue   = -workspace->threshold;
	  maxvalue   =  workspace->threshold;

	  deltavalue = maxvalue - minvalue;

	  a = 255.0/deltavalue;
	  b = - a * minvalue;

	  int startIdx = sliceInBlock * imageSize;
	  int endIdx = startIdx + imageSize;

	  for(idx = startIdx; idx < endIdx; idx++) {
	    truevalue = workspace->blockRecon[idx];
	    bit8 = a * truevalue + b;

	    num = (char)(((int)bit8) & 0xFF);
	    fwrite(&num, 1, 1, file);
	  }

	  

	} 
	else {
	  ///////////////////////////////////////////////
	  ////////// saving as floating point precision
	  
	  float* blockData = workspace->blockRecon + sliceInBlock * imageSize;

	  fwrite(blockData, sizeof(float), imageSize, file);
	}

	
	fclose(file);
      }
      
    }
}

