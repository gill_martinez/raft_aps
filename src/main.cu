/* -------------------------------------------------------------------
   RAFT - Tomographic reconstruction code


   Author(s): 	
                Eduardo X. Miqueles	email: eduardo.miqueles@lnls.br
		Gilberto Martinez Jr
		Janito Vaqueiro F.Filho
		Thiago Vallin Spina

   ######
   Remark:
   ######
  
   Sinograms are accessed by row-major ordering 
   and with mathematical order "angles x rays".
   Therefore, 
   
   sino[j * nrays + i] = sino( theta[j], ray[i] )
  
   with Nrays indicating the number of columns and Nangles
   the number of rows. A given pixel on the sinogram
   can be referenced using the macro FMATRIX 
   
   FMATRIX(array, j, i, CSTRIDE)  = array[j*CSTRIDE + i]
   
   -------------------------------------------------------------------*/


#include "raft.h"


/*===============================================================
 
  MAIN ()
 
  ===============================================================*/

int main (int argc,char *argv[], char **env)
{
  unsetenv("CUDA_VISIBLE_DEVICES");
  
  hid_t file,dataset, dataspace,datatype,acc_tpl1;
  herr_t status_hdf,ret;
  hid_t xfer_plist;
  hsize_t dims_in[3];
  //H5T_class_t class;
  H5T_order_t order;
  size_t size, dataSize, offset;
  int rank, status_n, Nangles, Nrays, Nslices, sizeImage;
  int numberofblocks, blockSize, numberOfstreams, nthreads;
  char path[100], path_tomo[100], outputPath[100];
  int sliceInit, sliceFinal, qtdSlices,numerical;
  int ringComposition, ringBlocks, hybrid, axis, zpad, numberOfIterations;
  float reg, threshold, offsetsino;
  bool printTiming, printTimingK, fromSinogram, check, BST, doNotCenter, doNotRings, doNotSave;
  gpu_ids_t *gpus;
  int mpi_namelen;
  char mpi_name[MPI_MAX_PROCESSOR_NAME];
  int processNumberBlock;

  raft_tomo workspace;

  /*MPI Stuff*/
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Info info = MPI_INFO_NULL;

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(comm,&workspace.processId);
  MPI_Comm_size(comm,&workspace.qtdProcess);
  MPI_Get_processor_name(mpi_name,&mpi_namelen);

  fprintf(stderr,"\n name %s\n",mpi_name);
  fprintf(stderr,"\n process %d\n",workspace.processId);
  
  
  ////////////////////////////////////////
  ////// Gengetopt 
  
  struct gengetopt_args_info arg;

  if(cmdline_parser(argc, argv, &arg)!=0)
    return 0;
  
  strcpy(path, arg.path_arg); 
  strcpy(path_tomo, arg.path_arg);
  strcat(path_tomo,"tomo.h5");
  
  configureOutputPath(&arg, outputPath);
  
  gpus = configureGpus(&arg);

  sliceInit  = arg.initial_arg;
  sliceFinal = arg.final_arg;  
  
  reg       = arg.reg_arg;
  blockSize = arg.block_arg;
  threshold = arg.max_arg;
  numerical = arg.numerical_arg;
  nthreads  = arg.threads_arg;
  hybrid    = arg.hybrid_arg;  
  offsetsino= arg.offset_arg;
  
  printTiming  = arg.timing_given;
  printTimingK = arg.ktiming_given;
  fromSinogram = arg.sinogram_given;  
  check        = arg.check_given;
  BST          = arg.bst_given;
  doNotCenter  = arg.doNotCenter_given;
  doNotRings   = arg.doNotRings_given;
  doNotSave    = arg.doNotSave_given;
  
  if(arg.iterations_given){
    numberOfIterations = arg.iterations_arg;
  }
  else{
    numberOfIterations = 0;
  }

  if(arg.axis_given)
  {
     axis = arg.axis_arg;
  }

  if(arg.zero_given)
    zpad = arg.zero_arg;
  else
    zpad = 0;		

  int devicesCount;
  cudaError_t CuStatus;
  CuStatus = cudaGetDeviceCount(&devicesCount);
 
  /*validate MPI process X Number of slices*/
  if(workspace.qtdProcess * blockSize > (sliceFinal - sliceInit))
    {
      if(workspace.processId==0)
	fprintf(stderr,"Decrease Number of process or blockSize!\n");
      
      MPI_Finalize();
 
      return 1;

    }
    
  if(CuStatus == cudaSuccess){     
  
    if(printTiming && workspace.processId == 0 ) {

      int* gpuIds = gpus->gpuIds;

      fprintf(stderr,"\nNumber of devices: %d\n",gpus->numGpus);

      for (int i = 0; i < gpus->numGpus; i++) 
	{
	  int dev = gpuIds[i];

	  cudaDeviceProp prop;
	  cudaGetDeviceProperties(&prop, dev);
	  
	  fprintf(stderr,"  Device Number: %d\n", dev);
	  fprintf(stderr,"  Device Bus ID: %d\n", prop.pciBusID);
	  fprintf(stderr,"  Device Device ID: %d\n", prop.pciDeviceID);
	  fprintf(stderr,"  Device name: %s\n", prop.name);
	  fprintf(stderr,"  Memory Clock Rate (KHz): %d\n",prop.memoryClockRate);
	  fprintf(stderr,"  Memory Bus Width (bits): %d\n",prop.memoryBusWidth);
	  fprintf(stderr,"  Peak Memory Bandwidth (GB/s): %f\n",2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
	  fprintf(stderr,"  Capability: %d.%d\n\n",prop.major,prop.minor);
	}
    }
  }
  else
     fprintf(stderr,"%s\n", cudaGetErrorString(CuStatus));


  //desable if numGpusPerNode > 1 
  gpus->numGpus = 1;


  numberOfstreams = NUMBEROFSTREAMS;

  ringComposition = arg.comp_arg;
  ringBlocks      = arg.div_arg;

  //////////////////////////////////////
  
  qtdSlices = sliceFinal - sliceInit;
  
  /* setup file access template with parallel IO access*/
  acc_tpl1 = H5Pcreate(H5P_FILE_ACCESS);
  
  /* set Parallel access with communicator */
  ret = H5Pset_fapl_mpio(acc_tpl1,comm,info);
  
  /* open the file collectively */
  file = H5Fopen(path_tomo,H5F_ACC_RDONLY,acc_tpl1);
  
  /*open dataset */
  dataset = H5Dopen(file,DATASET,H5P_DEFAULT);

  
  /* Release file-access template */
  ret=H5Pclose(acc_tpl1);

  
  /*
   * Get datatype and dataspace handles and then query
   * dataset class, order, size, rank and dimensions. */
  
  datatype = H5Dget_type(dataset);
  //class    = H5Tget_class(datatype);
  order	   = H5Tget_order(datatype);
  offset   = H5Tget_offset(datatype);
  size 	   = H5Tget_size(datatype);
  dataSize = H5Dget_storage_size(dataset);
  
  /*get dataspace*/
  dataspace = H5Dget_space(dataset);


  /* set up the collective transfer properties list 
  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  if(xfer_plist == FAIL)
    fprintf(stderr,"\nH5pcreate read Fail!\n");
  
  ret=H5Pset_dxpl_mpio(xfer_plist, H5FD_MPIO_INDEPENDENT);
  if(ret == FAIL)
    fprintf(stderr,"\nH5Pset_dxpl_mpio Fail!\n");

  */
  
  /*get Number of dimensions*/
  rank      = H5Sget_simple_extent_ndims(dataspace);
  status_n  = H5Sget_simple_extent_dims(dataspace,dims_in,NULL);
  
  if(DISP)
    { 
      if(order == H5T_ORDER_LE)
	fprintf(stderr,"Little endian order\n");
      
      fprintf(stderr,"H5 Offset: %lu\n",offset);
      fprintf(stderr,"H5 Data size is: %lu\n",size);
      fprintf(stderr,"H5 Number of points x Data size: %lu\n",dataSize);
            
      fprintf(stderr,"rank %d, dimensions %lu x %lu x %lu\n", 
	      rank,(unsigned long)(dims_in[0]), (unsigned long)(dims_in[1]), (unsigned long)(dims_in[2]));
  }
  
  Nangles   = dims_in[0]; //Nangles
  Nrays     = dims_in[2]; //Nrays
  Nslices   = dims_in[1]; //Nslices

  if(arg.size_given)
    sizeImage = arg.size_arg;
  else
    sizeImage = Nrays;
    
  /*calculate how many blocks are needed*/
  numberofblocks = number_of_blocks(qtdSlices,blockSize);

  /*calculate how many blocks each process will receive */
  if(numberofblocks > 1)
    {
      processNumberBlock = numberofblocks / workspace.qtdProcess;
      
      /*adjust how many blocks the last process will receive*/
      if(workspace.processId == workspace.qtdProcess-1)
	processNumberBlock += numberofblocks - (workspace.qtdProcess * processNumberBlock); 

    }
  else
    processNumberBlock = numberofblocks;

  //fprintf(stdout,"Process[%d] received %d blocks %d\n", workspace.processId,processNumberBlock,blockSize);


  /*Process 0 send a broadCast message telling how many blocks will process*/
  if(workspace.processId == 0)
      workspace.blocksLastProcess = processNumberBlock;

  MPI_Bcast(&workspace.blocksLastProcess,1,MPI_INT,0,MPI_COMM_WORLD);
  
  raft_create_workspace(&workspace,
			-1.0,  // tmin,
			1.0, // tmax,
			-1.0, // xymin,
			0.0, // theta0,
			PI,   // thetaf,
			Nrays,
			Nangles,
			Nslices,
			sizeImage,
			blockSize,
			sliceInit,
			sliceFinal,
			numberOfstreams,
			file,
			dataspace,
			dataset,
			-1,
			path,
			outputPath,
			reg,
			threshold,
			numerical,
			ringComposition,
			ringBlocks,
			hybrid,
			offsetsino,
			axis,
	                zpad,
			numberofblocks,
			processNumberBlock,
			gpus,
			devicesCount,
			numberOfIterations,
			printTiming,
			printTimingK,
			fromSinogram,
			doNotCenter,
			doNotRings,
			doNotSave);
  if(!fromSinogram)
    {
      //get flat image
      raft_read_flat(&workspace, path);
      
      //get dark image
      raft_read_dark(&workspace, path);
    }
  
  /////////////////////////////////////////////////////////////////////
  
  int blockInit  = (int) floor(sliceInit/blockSize);
  int blockFinal = (int) floor(sliceFinal/blockSize);

  struct timespec TimeStart, TimeEnd;
  clock_gettime(CLOCK, &TimeStart);
  
  if(check)
    {
      if(fromSinogram)
	{
	  if(BST)
	    check_fbp_bst_fromSinogram_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	  else
	    check_fbp_slantstack_fromSinogram_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	}
      else
	{
	  if(BST)
	    check_fbp_bst_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	  else
	    check_fbp_slantstack_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	}
    }
  else
    {
      if( numberOfIterations > 0 )
	{
	  /*
	  if(fromSinogram)
	    reconstruct_fromSinogram_em_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	  else
	    reconstruct_em_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	  */
	}
      else
	{
	  if(fromSinogram)
	    {
	      if(BST){
		reconstruct_fromSinogram_bst_cpu_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	      }
	      else
		reconstruct_fromSinogram_fbp_cpu_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	    }
	    
	  else
	    if(BST)
	      reconstruct_fbp_bst_cpu_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	    else
	      reconstruct_fbp_cpu_gpu(blockInit, blockSize, processNumberBlock, &workspace);
	      
	 }
     }

   clock_gettime(CLOCK, &TimeEnd);
   float elapsed = TIME(TimeEnd,TimeStart);
   fprintf(stderr,"\nProcess[%d],Total Elapsed time: %lf\n",workspace.processId,elapsed);  

   /////////////////////////////////////////
  //close dataset,dataspace,memspace,file
  //


  raft_destroy_workspace(workspace);

  //status_hdf = H5Pclose(xfer_plist);
  status_hdf = H5Dclose (dataset);
  status_hdf = H5Sclose (dataspace);
  status_hdf = H5Fclose (file);

  status_n++;
  status_hdf++;
  nthreads++;
 
  MPI_Finalize();
 
  return 0;
}
