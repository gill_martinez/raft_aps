#ifndef RAFT_PIPELINE_PROCESSINGFUNCTIONS_H
#define RAFT_PIPELINE_PROCESSINGFUNCTIONS_H

#include "../reconstruction/raft_rings.h"
#include "../common/raft_types.h"
#include "raft_pipeline.h"

#define CPUTHREADS 0

const int numStreamsPerGpu = 1;

typedef struct 
{
  int firstBlock;
  int currentBlock;
  int blockSize;
  raft_tomo* workspace;

}work_item_creation_parameters_t;

gpu_ids_t* configureGpus(const struct gengetopt_args_info* arg);

void configureOutputPath(const struct gengetopt_args_info* arg, char* outputPath);

void copyPathString(char* destination, const char* sourcePath);

void reconstruct_fbp_cpu_gpu(int blockInit, int blockSize, int nblock, raft_tomo* workspace);

void reconstruct_fbp_bst_cpu_gpu(int blockInit, int blockSize, int nblock, raft_tomo* workspace);

void reconstruct_fromSinogram_fbp_cpu_gpu(int blockInit, int blockSize, int numberofblocks, raft_tomo* workspace);
void reconstruct_fromSinogram_bst_cpu_gpu(int blockInit, int blockSize, int numberofblocks, raft_tomo* workspace);

void reconstruct_em_gpu(int blockInit, int blockSize, int numberofblocks, raft_tomo* workspace);

void reconstruct_fromSinogram_em_gpu(int blockInit, int blockSize, int numberofblocks, raft_tomo* workspace);

void check_fbp_slantstack_gpu(int blockInit, int blockSize, int numberofblocks, raft_tomo* workspace);

void check_fbp_slantstack_fromSinogram_gpu(int blockInit, int blockSize, int numberofblocks, raft_tomo* workspace);

void check_fbp_bst_fromSinogram_gpu(int blockInit, int blockSize, int numberofblocks, raft_tomo* workspace);

void check_fbp_bst_gpu(int blockInit, int blockSize, int numberofblocks, raft_tomo* workspace);

void createGpuWorkspaces(raft_tomo** gpuWorkspaces, raft_tomo* globalWorkspace);

void destroyGpuWorkspaces(raft_tomo** gpuWorkspaces);

void createWorkItems(queue_t* workItems, int firstBlock, int blockSize, raft_tomo** workspace, int numBlocks);

work_item_creation_parameters_t* createWorkItemParameters(int firstBlock, int currentBlock, int blockSize, raft_tomo* workspace);

void* createWorkItem(void* parametersData);

raft_tomo* createGpuThreadWorkItem(int currentBlock, int blockInit,int blockSize, raft_tomo* globalWorkspace);

raft_tomo* cloneWorkspace(raft_tomo* sourceWorkspace);

void* allocateWorkItemGpuMemory_normalization(void* wrappedWorkspace);

void* allocateWorkItemGpuMemory_lowpass(void* wrappedWorkspace);

void* allocateWorkItemGpuMemory_backprojection(void* wrappedWorkspace);

void* allocateWorkItemGpuMemory_backprojection_bst(void* wrappedWorkspace);

void* freeWorkItemGpuMemory_backprojection(void* wrappedWorkspace);

void* freeWorkItemGpuMemory_backprojection_bst(void* wrappedWorkspace);

void* freeWorkItemGpuMemory_normalization(void* wrappedWorkspace);

void* freeWorkItemGpuMemory_lowpass(void* wrappedWorkspace);

void* readTomoBlock(void* wrappedWorkspace);

void* saveReconstructedBlock(void* wrappedWorkspace);

void* destroyGpuThreadWorkItem(void* wrappedWorkspace);

void *backprojection_slantstack_gpu(void* wrappedWorkspace);

void *backprojection_bst_gpu(void* wrappedWorkspace);

void *sinogram_normalization_gpu(void* wrappedWorkspace);

void *sinogram_ringfiltering_cpu(void* wrappedWorkspace);

void *sinogram_lowpass_gpu(void* wrappedWorkspace);

void *sinogram_findcenter_cpu(void* wrappedWorkspace);

void *sinogram_recentering_gpu(void* wrappedWorkspace);

void *em_emission_gpu(void *wrappedWorkspace);

void *processBlockInGpu(void* wrappedWorkspace);

void* processBlockInGpu_bst(void* wrappedWorkspace) ;

void* processBlockInGpu_fromSinogram(void* wrappedWorkspace);

void* processBlockInGpu_bst_fromSinogram(void* wrappedWorkspace);


typedef struct
{  
  raft_ring plan;
  float *sinogram;
  int nrays, nviews;
  int nthread;   
  int ringBlock, ringComposition;

}param_ring_t;


typedef struct
{  
  raft_tomo *workspace;  
  float *sinogram;
  int nthread;

}param_centersino_t;


#endif // RAFT_PIPELINE_PROCESSINGFUNCTIONS_H


