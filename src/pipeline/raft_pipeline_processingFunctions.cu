#include <stdio.h>
#include <stdlib.h>
#include <hdf5.h>
#include <math.h>
#include <cuda.h>
#include <fftw3.h> 
#include <cufft.h>
#include <cuComplex.h>
#include <pthread.h>

extern "C" {
#include "../common/cmdline.h"
#include <time.h>
}

#include "raft_pipeline.h"
#include "raft_pipeline_processingFunctions.h"

#include "../reconstruction/raft_processblock.h"
#include "../reconstruction/raft_backprojection_bst.h"
#include "../reconstruction/raft_backprojection_slantstack.h"
#include "../reconstruction/raft_centersino.h"
#include "../reconstruction/raft_rings.h"
#include "../reconstruction/raft_sinogram.h"
#include "../reconstruction/raft_filters.h"

#include "../readh5/raft_h5.h"

#include "../common/raft_memory.h"
#include "../common/raft_workspace.h"
#include "../common/raft_types.h"
#include "../common/raft_save.h"


/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

void configureOutputPath(const struct gengetopt_args_info* arg,
			 char* outputPath) 
{
  if (arg->outpath_given)
    copyPathString(outputPath, arg->outpath_arg);
  else {
    copyPathString(outputPath, arg->path_arg);
    strcat(outputPath, "/recon");
  }
}

gpu_ids_t* configureGpus(const struct gengetopt_args_info* arg) 
{
  int ids[] = { 0 };
  
  if (arg->gpu_given > 0)
    return createGpuIds(arg->gpu_given, arg->gpu_arg);
  else
    return createGpuIds(1, ids);
}


void copyPathString(char* destination, 
		    const char* sourcePath) 
{
  int length = strlen(sourcePath);
  int pathEnd = length - 1;
  char lastChar = sourcePath[pathEnd];
  
  while (lastChar == '/') {
    --pathEnd;
    
    lastChar = sourcePath[pathEnd];
  }
  
  int pathLength = pathEnd + 1;

  strncpy(destination, sourcePath, pathLength);
  destination[pathLength] = '\0';
}

void reconstruct_fbp_cpu_gpu(int blockInit, 
			     int blockSize, 
			     int numberofblocks,
			     raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  int numCpuThreads = 1;
  int numGpuThreads;
  
  if(numGpus==1)
    numGpuThreads = numGpus * numStreamsPerGpu;
  else
    numGpuThreads = 2*numGpus * numStreamsPerGpu;

  pipeline_stage_params_t stages[] = {
    { "Create work item",               4, 4,
      createWorkItem },
    { "Read tomo block",                8, 1,
      readTomoBlock },
    { "Sinogram normalization",         8, numGpuThreads,
      sinogram_normalization_gpu },
    { "Sinogram Find Center",           8, numCpuThreads,
      sinogram_findcenter_cpu },
    { "Sinogram Ring/Filters",          8, numCpuThreads,
      sinogram_ringfiltering_cpu },
    { "Sinogram Low-pass",             16, numGpuThreads,
      sinogram_lowpass_gpu },
    { "Backprojection block",           8, numGpuThreads,
      backprojection_slantstack_gpu },
    { "Save result block",              32, 32,
      saveReconstructedBlock },
    { "Destroy work item",              1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo* workspaces[numGpus];
  queue_t workItems;
  
  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);

  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);
  
  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}


void reconstruct_fbp_bst_cpu_gpu(int blockInit, 
				 int blockSize, 
				 int numberofblocks,
				 raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  int numCpuThreads = 1;
  int numGpuThreads;
  
  if(numGpus==1)
    numGpuThreads = numGpus * numStreamsPerGpu;
  else
    numGpuThreads = 2*numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "CreateWork work item",           4, 4,
      createWorkItem },
    { "Read tomo block",                8, 1,
      readTomoBlock },
     { "Sinogram normalization",         8, numGpuThreads,
      sinogram_normalization_gpu },
    { "Sinogram Find Center",           8, numCpuThreads,
      sinogram_findcenter_cpu },
    { "Sinogram Ring/Filters",          8, numCpuThreads,
      sinogram_ringfiltering_cpu },
    { "Sinogram Low-pass",              16, numGpuThreads,
      sinogram_lowpass_gpu },
    { "Backprojection block",           8, numGpuThreads,
      backprojection_bst_gpu },
    { "Save result block",              32, 32,
      saveReconstructedBlock },
     { "Destroy work item",              1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo* workspaces[numGpus];
  queue_t workItems;
  
  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);

  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);
  
  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}


void reconstruct_fromSinogram_fbp_cpu_gpu(int blockInit, 
					  int blockSize, 
					  int numberofblocks,
					  raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  int numCpuThreads = 1;
  int numGpuThreads = 2 * numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "Create work item",               4, 4,
      createWorkItem },
    { "Read tomo block",                8, 1,
      readTomoBlock },
    { "Sinogram Find Center (CPU)",     8, numCpuThreads,
      sinogram_findcenter_cpu },
    { "Sinogram Ring/Filters (CPU)",    8, numCpuThreads,
      sinogram_ringfiltering_cpu },
    { "Sinogram Low-pass (GPU)",        16, numGpuThreads,
      sinogram_lowpass_gpu },
    { "Backprojection block (GPU)",     8, numGpuThreads,
      backprojection_slantstack_gpu },
    { "Save result block",              32, 32,
      saveReconstructedBlock },
    { "Destroy work item",              1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo* workspaces[numGpus];
  queue_t workItems;
  
  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);

  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);
  
  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}

void reconstruct_fromSinogram_bst_cpu_gpu(int blockInit, 
					  int blockSize, 
					  int numberofblocks,
					  raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  int numCpuThreads = 1;
  int numGpuThreads = 2 * numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "Create work item",               4, 4,
      createWorkItem },
    { "Read tomo block",                8, 1,
      readTomoBlock },
    { "Sinogram Find Center (CPU)",     8, numCpuThreads,
      sinogram_findcenter_cpu },
    { "Sinogram Ring/Filters (CPU)",    8, numCpuThreads,
      sinogram_ringfiltering_cpu },
    { "Sinogram Low-pass (GPU)",        16, numGpuThreads,
      sinogram_lowpass_gpu },
    { "Backprojection block (GPU)",     8, numGpuThreads,
      backprojection_bst_gpu },
    { "Save result block",              32, 32,
      saveReconstructedBlock },
    { "Destroy work item",              1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo* workspaces[numGpus];
  queue_t workItems;
  
  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);

  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);
  
  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}


/*
void reconstruct_em_gpu(int blockInit, 
			int blockSize, 
			int numberofblocks,
			raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  int numCpuThreads = 1;
  int numGpuThreads = 2 * numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "Create work item",                           4, 4,
      createWorkItem },
    { "Read tomo block",                            8, 1,
      readTomoBlock },
    { "Sinogram normalization (GPU)",               8, numGpuThreads,
      sinogram_normalization_gpu },
    { "Sinogram Find Center (CPU)",                 8, numCpuThreads,
      sinogram_findcenter_cpu },
    { "Sinogram Ring/Filters (CPU)",                8, numCpuThreads,
      sinogram_ringfiltering_cpu },
    { "Sinogram Re-center + Regularization (GPU)",  16, numGpuThreads,
      sinogram_recentering_gpu },
    { "EM-Emisison (GPU)",                          8, numGpuThreads,
      em_emission_gpu },
    { "Save result block",                          32, 32,
      saveReconstructedBlock },
    { "Destroy work item",                          1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo* workspaces[numGpus];
  queue_t workItems;

  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);
  
  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);

  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}

void reconstruct_fromSinogram_em_gpu(int blockInit, 
				     int blockSize, 
				     int numberofblocks,
				     raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  int numCpuThreads = 1;
  int numGpuThreads = 2 * numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "Create work item",                           4, 4,
      createWorkItem },
    { "Read tomo block",                            8, 1,
      readTomoBlock },
    { "Sinogram Find Center (CPU)",                 8, numCpuThreads,
      sinogram_findcenter_cpu },
    { "Sinogram Ring/Filters (CPU)",                8, numCpuThreads,
      sinogram_ringfiltering_cpu },
    { "Sinogram Re-center + Regularization (GPU)",  16, numGpuThreads,
      sinogram_recentering_gpu },
    { "EM-Emisison (GPU)",                          8, numGpuThreads,
      em_emission_gpu },
    { "Save result block",                          32, 32,
      saveReconstructedBlock },
    { "Destroy work item",                          1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo* workspaces[numGpus];
  queue_t workItems;

  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);
  
  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);

  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}
*/


void check_fbp_slantstack_gpu(int blockInit, 
			      int blockSize, 
			      int numberofblocks,
			      raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  int numGpuThreads = 2 * numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "Create work item",                           4, 4,
      createWorkItem },
    { "Read tomo block",                            8, 1,
      readTomoBlock },
    { "ProcessBlock (GPU)",                         8, numGpuThreads,
      processBlockInGpu },
    /* { "Save result block",                        32, 32,
      saveReconstructedBlock },
    */{ "Destroy work item",                          1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo *workspaces[numGpus];
  queue_t workItems;

  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);
  
  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);

  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}


void check_fbp_slantstack_fromSinogram_gpu(int blockInit, 
					   int blockSize, 
					   int numberofblocks,
					   raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  int numGpuThreads = 2 * numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "Create work item",                           4, 4,
      createWorkItem },
    { "Read tomo block",                            8, 1,
      readTomoBlock },
    { "ProcessBlock (GPU)",                         8, numGpuThreads,
      processBlockInGpu_fromSinogram },
    /*{ "Save result block",                        32, 32,
      saveReconstructedBlock },*/
    { "Destroy work item",                          1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo *workspaces[numGpus];
  queue_t workItems;

  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);
  
  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);

  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}


void check_fbp_bst_gpu(int blockInit, 
		       int blockSize, 
		       int numberofblocks,
		       raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  
  //int numGpuThreads = 1;
  int numGpuThreads = 2 * numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "Create work item",                           4, 4,
      createWorkItem },
    { "Read tomo block",                            8, 1,
      readTomoBlock },
    { "ProcessBlock (GPU)",                         8, numGpuThreads,
      processBlockInGpu_bst },
    /*{ "Save result block",                        32, 32,
      saveReconstructedBlock }, 
    */{ "Destroy work item",                          1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo *workspaces[numGpus];
  queue_t workItems;

  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);
  
  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);

  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}

void check_fbp_bst_fromSinogram_gpu(int blockInit, 
				    int blockSize, 
				    int numberofblocks,
				    raft_tomo* workspace) 
{
  int numGpus = workspace->gpus->numGpus;
  
  //int numGpuThreads = 1;
  int numGpuThreads = 2 * numGpus * numStreamsPerGpu;
  
  pipeline_stage_params_t stages[] = {
    { "Create work item",                           4, 4,
      createWorkItem },
    { "Read tomo block",                            8, 1,
      readTomoBlock },
    { "ProcessBlock (GPU)",                         8, numGpuThreads,
      processBlockInGpu_bst_fromSinogram },
    /*{ "Save result block",                        32, 32,
      saveReconstructedBlock }, */
    { "Destroy work item",                          1, 1,
      destroyGpuThreadWorkItem },
  };

  raft_tomo *workspaces[numGpus];
  queue_t workItems;

  int numStages = sizeof(stages) / sizeof(stages[0]);
  pipeline_t* pipeline = createPipeline(numStages, stages);
  
  createGpuWorkspaces(workspaces, workspace);
  createWorkItems(&workItems, blockInit, blockSize, workspaces, numberofblocks);
  
  runPipeline(pipeline, &workItems);

  destroyPipeline(pipeline);
  releaseQueue(&workItems);
  destroyGpuWorkspaces(workspaces);
}


void createGpuWorkspaces(raft_tomo **gpuWorkspaces,
			 raft_tomo *globalWorkspace) 
{
  gpu_ids_t* gpus = globalWorkspace->gpus;
  int numGpus = gpus->numGpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(globalWorkspace->processId % gpusPerNode)]);

  
  int Nslices = globalWorkspace->Nslices;
  int Nrays = globalWorkspace->Nrays;
  int gpuIndex = 0;
  
  //enable this for multiples Gpu Per Nodes
  for ( gpuIndex = 0; gpuIndex < numGpus; ++gpuIndex)
    {

    raft_tomo *gpuWorkspace = cloneWorkspace(globalWorkspace);

    int gpuId = gpuIds[gpuIndex];

    gpuWorkspace->gpuId = gpuId;

    cudaSetDevice( gpuIdProcess );

    //cudaSetDevice( gpuId );

    gpuWorkspace->data.channelDesc_tomo = cudaCreateChannelDesc(32, 0, 0, 0,
								cudaChannelFormatKindFloat);
    
    gpuWorkspace->data.channelDesc_flatAfter = cudaCreateChannelDesc(32, 0, 0, 0,
								     cudaChannelFormatKindFloat);

    gpuWorkspace->data.channelDesc_flatBefore = cudaCreateChannelDesc(32, 0, 0, 0,
								      cudaChannelFormatKindFloat);
    
    gpuWorkspace->data.channelDesc_dark = cudaCreateChannelDesc(32, 0, 0, 0,
								cudaChannelFormatKindFloat);
    
    cudaMallocArray(&gpuWorkspace->data.flatBeforeArray,
		    &gpuWorkspace->data.channelDesc_flatBefore, Nslices, Nrays);
    
    cudaMallocArray(&gpuWorkspace->data.flatAfterArray,
		    &gpuWorkspace->data.channelDesc_flatAfter, Nslices, Nrays);
    
    cudaMallocArray(&gpuWorkspace->data.darkArray,
		    &gpuWorkspace->data.channelDesc_dark, Nslices, Nrays);
    
    raft_load_shared_gpu_data(gpuWorkspace);
    
    gpuWorkspaces[gpuIndex] = gpuWorkspace;
  }
}


void destroyGpuWorkspaces(raft_tomo** gpuWorkspaces) 
{
  int numGpus = gpuWorkspaces[0]->gpus->numGpus;

  for (int gpuIndex = 0; gpuIndex < numGpus; ++gpuIndex) {
    raft_tomo* gpuWorkspace = gpuWorkspaces[gpuIndex];

    cudaFreeArray(gpuWorkspace->data.flatBeforeArray);
    cudaFreeArray(gpuWorkspace->data.flatAfterArray);
    cudaFreeArray(gpuWorkspace->data.darkArray);

    cudaDestroyTextureObject(gpuWorkspace->data.flatAfterTexture);
    cudaDestroyTextureObject(gpuWorkspace->data.flatBeforeTexture);

    cudaDestroyTextureObject(gpuWorkspace->data.darkTexture);
  }
}

void createWorkItems(queue_t* workItems, 
		     int firstBlock, 
		     int blockSize,
		     raft_tomo** workspaces, 
		     int numBlocks) 
{
  int numGpus = workspaces[0]->gpus->numGpus;

  initializeQueue(workItems);

  for (int currentBlock = 0; currentBlock < numBlocks; ++currentBlock) {
    int gpuIndex = currentBlock % numGpus;

    void* parameters = createWorkItemParameters(firstBlock, currentBlock,
						blockSize, workspaces[gpuIndex]);
    
    queuePush(workItems, parameters);
  }
}

work_item_creation_parameters_t* createWorkItemParameters(int firstBlock,
							  int currentBlock, 
							  int blockSize, 
							  raft_tomo* workspace) {
  work_item_creation_parameters_t* workItemParameters;
  
  workItemParameters = (work_item_creation_parameters_t*)
    malloc(sizeof(*workItemParameters));
  
  workItemParameters->firstBlock = firstBlock;
  workItemParameters->currentBlock = currentBlock;
  workItemParameters->blockSize = blockSize;
  workItemParameters->workspace = workspace;
  
  return workItemParameters;
}

void* createWorkItem(void* parametersData) 
{
  work_item_creation_parameters_t* parameters =
    (work_item_creation_parameters_t*)parametersData;
  
  raft_tomo* workItem = createGpuThreadWorkItem(parameters->currentBlock,
						parameters->firstBlock,
						parameters->blockSize,
						parameters->workspace);
  
  free(parameters);
  
  return workItem;
}


void* processBlockInGpu(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;

  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  
  cudaSetDevice( gpuIdProcess );

  //cudaSetDevice( workspace-> gpuId );

  
  allocateWorkItemGpuMemory_normalization(workspace);
  allocateWorkItemGpuMemory_backprojection(workspace);
  allocateWorkItemGpuMemory_lowpass(workspace);
  
  raft_process_block_gpu(workspace);
  
  freeWorkItemGpuMemory_lowpass(workspace);
  freeWorkItemGpuMemory_normalization(workspace);
  freeWorkItemGpuMemory_backprojection(workspace);
 
  return workspace;
}

void* processBlockInGpu_fromSinogram(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);
  
  cudaSetDevice( gpuIdProcess );

  //cudaSetDevice( workspace-> gpuId );
  
  allocateWorkItemGpuMemory_normalization(workspace);
  allocateWorkItemGpuMemory_backprojection(workspace);
  allocateWorkItemGpuMemory_lowpass(workspace);
  
  raft_process_block_gpu_fromSinogram(workspace);
  
  freeWorkItemGpuMemory_lowpass(workspace);
  freeWorkItemGpuMemory_normalization(workspace);
  freeWorkItemGpuMemory_backprojection(workspace);
 
  return workspace;
}

void* processBlockInGpu_bst(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  
  cudaSetDevice( gpuIdProcess );

  //cudaSetDevice( workspace-> gpuId );

  allocateWorkItemGpuMemory_normalization(workspace);
  allocateWorkItemGpuMemory_backprojection_bst(workspace);
  allocateWorkItemGpuMemory_lowpass(workspace);
 
  raft_process_block_gpu_bst(workspace);

  freeWorkItemGpuMemory_lowpass(workspace);
  freeWorkItemGpuMemory_normalization(workspace);
  freeWorkItemGpuMemory_backprojection_bst(workspace);
 
  return workspace;
}


void* processBlockInGpu_bst_fromSinogram(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  
  cudaSetDevice( gpuIdProcess );
  
  //cudaSetDevice( workspace-> gpuId );
  
  allocateWorkItemGpuMemory_normalization(workspace);
  allocateWorkItemGpuMemory_backprojection_bst(workspace);
  allocateWorkItemGpuMemory_lowpass(workspace);
 
  raft_process_block_gpu_bst_fromSinogram(workspace);

  freeWorkItemGpuMemory_lowpass(workspace);
  freeWorkItemGpuMemory_normalization(workspace);
  freeWorkItemGpuMemory_backprojection_bst(workspace);
 
  return workspace;
}


void* sinogram_normalization_gpu(void* wrappedWorkspace)
{
  raft_tomo *workspace = (raft_tomo*) wrappedWorkspace;

  struct timespec TimeStart, TimeEnd;
  
  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  
  cudaSetDevice( gpuIdProcess );
  
  //cudaSetDevice( workspace-> gpuId );
  
  //////////////////////////
  // flat/dark normalization
  
  clock_gettime(CLOCK, &TimeStart);
  
  allocateWorkItemGpuMemory_normalization(workspace);
  
  if(workspace->hybrid==1)
    {
      raft_build_sinogram_block_360_gpu(workspace->blockSino,
					workspace->tomo,
					workspace);
    }
  else
    {    
      raft_build_sinogram_block_gpu_zpad(workspace->blockSino,
					 workspace->tomo,
					 workspace);
    }

  /*
    {
    int Nrays=workspace->Nrays;
    int Nangles=workspace->Nangles;
    
    int m,n,z=0;
    
    FILE *fp=fopen("debuga.dat","w");
    for(m=0;m<Nangles;m++)
    {
    for(n=0;n<Nrays;n++)
    fprintf(fp,"%lf ",workspace->blockSino[z*Nrays*Nangles+m*Nrays+n]);
    fprintf(fp,"\n");
    }
    fclose(fp);
    }
  */
  
  freeWorkItemGpuMemory_normalization(workspace); 
  
  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  if(workspace->printTiming)	
    fprintf(stderr,"\n[%d]\t%lf\tSinogram Normalization (GPU)",workspace->nblock,elapsed);  
  
  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;
  
  return workspace; 
}

void *kernel_processOneRingFiltering(void *t)
{
  param_ring_t *param;

  param = (param_ring_t *)t;

  int ringBlocks = param->ringBlock;
  int ringComposition = param->ringComposition;
  int r;

  if ( ringComposition != 0 )
    {
      if (ringBlocks > 1)
	{
	  for(r = 0 ; r < ringComposition; r++)
	    {
	      raft_rings_blocks(param->plan, param->sinogram, param->nrays,  param->nviews, ringBlocks);		
	    }
	}
      else
	{
	  if ( ringComposition > 1)
	    {
	      for(r = 0 ; r < ringComposition; r++)
		{
		  raft_rings(param->plan, param->sinogram, param->nrays,  param->nviews);
		}	      
	    }
	  else
	    raft_rings(param->plan, param->sinogram, param->nrays,  param->nviews);
	    
	}
    }
  
  //raft_rings_gpu(&param->plan, param->sinogram, param->nrays,  param->nviews);

  pthread_exit(NULL);
}


void *sinogram_ringfiltering_cpu(void* wrappedWorkspace)
{
  raft_tomo *workspace = (raft_tomo*) wrappedWorkspace;

  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  
  cudaSetDevice( gpuIdProcess );
  
  //cudaSetDevice( workspace-> gpuId );
  
  struct timespec TimeStart, TimeEnd;

  clock_gettime(CLOCK, &TimeStart);
  
  if(!workspace->doNotRings)
    {
      if(workspace->ringComposition > 0)
	{
	  ////////////////////////////////////
	  /////// CPU THREADS ONLY FOR RINGS 
	  ///////
	  int nrays     = workspace->Nrays;
	  int nviews    = workspace->Nangles;
	  int blockSize = workspace->blockSize;

	  pthread_attr_t attr;
	  pthread_t *thread;
	  int n, rc;    
	  param_ring_t *params; 
  
	  thread = (pthread_t *)malloc(sizeof(pthread_t) * blockSize);
	  params = (param_ring_t *)malloc(sizeof(param_ring_t) * blockSize);
  
	  pthread_attr_init(&attr);
	  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	  for(n = 0; n < blockSize; n++) 
	    {
	      params[n].plan = raft_rings_plan_create(nrays, nviews, 0);
	      params[n].nrays = nrays;
	      params[n].nviews = nviews;
	      params[n].nthread = n; 
	      params[n].ringBlock = workspace->ringBlocks;
	      params[n].ringComposition = workspace->ringComposition;      
	      params[n].nthread = n;
	      params[n].sinogram = (float *)malloc(sizeof(float)*nrays*nviews);
      
	      memcpy( params[n].sinogram, &workspace->blockSino[n*nrays*nviews], sizeof(float)*nrays*nviews );
      
	      pthread_create(&thread[n], 
			     NULL, 
			     kernel_processOneRingFiltering, 
			     (void *)&params[n]);
	    }
  
	  pthread_attr_destroy(&attr);
	  for(n = 0; n < blockSize; n++) 
	    {
	      rc = pthread_join(thread[n], NULL);

	      memcpy( &workspace->blockSino[n*nrays*nviews], params[n].sinogram, sizeof(float)*nrays*nviews );
      
	      free(params[n].sinogram);
	      raft_rings_plan_destroy(&params[n].plan);
	    }  

	  rc++;
	  free(thread);
	  free(params);  
	}
    }
  
  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  if(workspace->printTiming)	
    fprintf(stderr,"\n[%d]\t%lf\tRing Filtering (CPU)",workspace->nblock,elapsed);  

  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;
 
  return workspace;
}


void *kernel_processOneCentering(void *t)
{
  param_centersino_t *param;

  param = (param_centersino_t *)t;
    
  /////////////////////////////////
  //////// Find optimal offset

  int b = param->nthread;
  float beta;
  
  beta = raft_find_center(param->workspace, param->sinogram);
  
  param->workspace->centerblock[b] = beta;
  
  /**/
  int firstSlice = param->workspace->sliceInit;
  int currentBlock = param->workspace->nblock;
  int slicesPerBlock = param->workspace->blockSize;
  int firstSliceInBlock = firstSlice + slicesPerBlock * currentBlock;
  //fprintf(stdout,"%d %f\n",firstSliceInBlock + b, beta);
  /**/

  pthread_exit(NULL);
}


void *sinogram_findcenter_cpu(void* wrappedWorkspace)
{
  raft_tomo *workspace = (raft_tomo*) wrappedWorkspace;
  
  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  
  cudaSetDevice( gpuIdProcess );
  
  //cudaSetDevice( workspace-> gpuId );
  
  struct timespec TimeStart, TimeEnd;
  clock_gettime(CLOCK, &TimeStart);

  if(!workspace->doNotCenter)
    {      
      /////////////////////////////////////////////////
      /////// CPU THREADS ONLY FOR CENTERING
      ///////
      int nrays     = workspace->Nrays;
      int nviews    = workspace->Nangles;
      int blockSize = workspace->blockSize;

      pthread_attr_t attr;
      pthread_t *thread;
      int n, rc;    
      param_centersino_t *params; 
  
      thread = (pthread_t *)malloc(sizeof(pthread_t) * blockSize);
      params = (param_centersino_t *)malloc(sizeof(param_centersino_t) * blockSize);
  
      pthread_attr_init(&attr);
      pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

      for(n = 0; n < blockSize; n++) 
	{
	  params[n].workspace = (raft_tomo *)malloc(sizeof(raft_tomo));
	  params[n].workspace->dt = workspace->dt;
	  params[n].workspace->dxy = workspace->dxy;
	  params[n].workspace->tmin = workspace->tmin;
	  params[n].workspace->tmax = workspace->tmax;
	  params[n].workspace->xymin = workspace->xymin;
	  
	  params[n].workspace->offset = workspace->offset;
	  params[n].workspace->searchForCenter = SEARCH4CENTER;
     
	  params[n].workspace->centerblock = (float *) malloc(sizeof(float) * params[n].workspace->searchForCenter);
	  params[n].workspace->A = (float *) malloc (nrays*sizeof(float));
	  params[n].workspace->TA = (float *) malloc (nrays*sizeof(float));
	  params[n].workspace->B = (float *) malloc (nrays*sizeof(float));
	  params[n].workspace->TB = (float *) malloc (nrays*sizeof(float));
	  params[n].workspace->shift = (float *) malloc (nrays*sizeof(float));
	  params[n].workspace->fourier.in     = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * nrays );
	  params[n].workspace->fourier.out  = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * nrays );
	  params[n].workspace->fourier.fkernel = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * nrays );
	  params[n].workspace->fourier.in_   = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * nrays );
	  params[n].workspace->fourier.out_  = (fftw_complex *) fftw_alloc_complex( sizeof(fftw_complex) * nrays );
      
	  params[n].workspace->fourier.plan_forw = fftw_plan_dft_1d( nrays, 
								     params[n].workspace->fourier.in, 
								     params[n].workspace->fourier.out, 
								     FFTW_FORWARD,
								     FFTW_ESTIMATE);

	  params[n].workspace->fourier.plan_back = fftw_plan_dft_1d( nrays, 
								     params[n].workspace->fourier.in, 
								     params[n].workspace->fourier.out, 
								     FFTW_BACKWARD, 
								     FFTW_ESTIMATE);
      
	  params[n].workspace->fourier.plan_forw_ = fftw_plan_dft_1d( nrays, 
								      params[n].workspace->fourier.in_, 
								      params[n].workspace->fourier.out_, 
								      FFTW_FORWARD,
								      FFTW_ESTIMATE);
      
	  params[n].workspace->fourier.plan_back_ = fftw_plan_dft_1d( nrays, 
								      params[n].workspace->fourier.in_, 
								      params[n].workspace->fourier.out_, 
								      FFTW_BACKWARD, 
								      FFTW_ESTIMATE);
      
	  params[n].workspace->Nrays = nrays;
	  params[n].workspace->Nangles = nviews;
	  params[n].nthread = n; 
      
	  params[n].sinogram = (float *)malloc(sizeof(float)*nrays*nviews);
      
	  memcpy( params[n].sinogram, &workspace->blockSino[n*nrays*nviews], sizeof(float)*nrays*nviews );
      
	  pthread_create(&thread[n], 
			 NULL, 
			 kernel_processOneCentering, 
			 (void *)&params[n]);
	}
  
      pthread_attr_destroy(&attr);
      for(n = 0; n < blockSize; n++) 
	{
	  rc = pthread_join(thread[n], NULL);

	  memcpy( &workspace->blockSino[n*nrays*nviews], params[n].sinogram, sizeof(float)*nrays*nviews );

	  workspace->centerblock[n] = params[n].workspace->centerblock[n];

	  free(params[n].sinogram);

	  free(params[n].workspace->A);
	  free(params[n].workspace->B);
	  free(params[n].workspace->TA);
	  free(params[n].workspace->TB);  
	  free(params[n].workspace->shift);
	  free(params[n].workspace->centerblock);
      
	  fftw_destroy_plan( params[n].workspace->fourier.plan_forw );
	  fftw_destroy_plan( params[n].workspace->fourier.plan_back );
      
	  fftw_destroy_plan( params[n].workspace->fourier.plan_forw_ );
	  fftw_destroy_plan( params[n].workspace->fourier.plan_back_ );      
	  fftw_free( params[n].workspace->fourier.in );
	  fftw_free( params[n].workspace->fourier.out );
	  fftw_free( params[n].workspace->fourier.fkernel );      
	  fftw_free( params[n].workspace->fourier.in_ );
	  fftw_free( params[n].workspace->fourier.out_ );
      
	  free( params[n].workspace );
	}  

      rc++;
      free(thread);
      free(params);
    }
  else
    {
      for(int n = 0; n < workspace->blockSize; n++) 
	workspace->centerblock[n] = 0.0;
      
    }

  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  if(workspace->printTiming)	
    fprintf(stderr,"\n[%d]\t%lf\tSinogram Find Center (CPU)",workspace->nblock,elapsed);  
  
  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;

  return workspace;
}


void *sinogram_lowpass_gpu(void* wrappedWorkspace)
{
  raft_tomo *workspace = (raft_tomo*) wrappedWorkspace;

  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  
  cudaSetDevice( gpuIdProcess );
  
  //cudaSetDevice( workspace-> gpuId );

  struct timespec TimeStart, TimeEnd;

  clock_gettime(CLOCK, &TimeStart);
  
  allocateWorkItemGpuMemory_lowpass(workspace);
  
  raft_lowpassino_gpu(workspace->blockSino, workspace);
  
  freeWorkItemGpuMemory_lowpass(workspace);
  
  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  if(workspace->printTiming)	
    fprintf(stderr,"\n[%d]\t%lf\tLow-pass filtering (GPU)",workspace->nblock,elapsed);
  
  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;
  
  return workspace;
}

void *sinogram_recentering_gpu(void* wrappedWorkspace)
{
  raft_tomo *workspace = (raft_tomo*) wrappedWorkspace;

  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  cudaSetDevice( gpuIdProcess );

  // cudaSetDevice( workspace-> gpuId );

  struct timespec TimeStart, TimeEnd;
  clock_gettime(CLOCK, &TimeStart);

  allocateWorkItemGpuMemory_lowpass(workspace);
  
  raft_recentersino_withregularization_gpu(workspace->blockSino, workspace);
  
  freeWorkItemGpuMemory_lowpass(workspace);

  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  if(workspace->printTiming)	
    fprintf(stderr,"\n[%d]\t%lf\tSinogram Recentering block (GPU)",workspace->nblock,elapsed);  

  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;
  
  return workspace;
}


raft_tomo* createGpuThreadWorkItem(int currentBlock, 
				   int blockInit,
				   int blockSize, 
				   raft_tomo* globalWorkspace) 
{
  int Nrays = globalWorkspace->measured_nrays;
  int Nangles = globalWorkspace->measured_nangles;
  int NdataOriginal  = blockSize * Nrays * Nangles, NdataNew, Nvoxels;

  if(globalWorkspace->hybrid==1)
    {
      Nrays   = 2*globalWorkspace->axis;
      Nangles = (int) globalWorkspace->measured_nangles/2;	
      NdataNew = blockSize * Nrays * Nangles;
      Nvoxels  = blockSize * Nrays * Nrays;
    }
  else
    {
      Nrays   = globalWorkspace->measured_nrays + 2*globalWorkspace->zpad;
      Nangles = globalWorkspace->measured_nangles;
      NdataNew = blockSize * Nrays * Nangles;
      Nvoxels  = blockSize * Nrays * Nrays;
    }
  
  raft_tomo* workspace = cloneWorkspace(globalWorkspace);
  
  workspace->nblock = currentBlock; 
  if(workspace->processId!=0){
    workspace->li =  (workspace->blocksLastProcess * workspace->processId * blockSize) + (currentBlock * blockSize);
    workspace->ls =  (workspace->blocksLastProcess * workspace->processId * blockSize) + ((currentBlock + 1) * blockSize); 
  }
  else{

    workspace->li = (blockSize*currentBlock )+(blockInit*blockSize);
    workspace->ls = (blockSize*(currentBlock+1))+(blockInit*blockSize);

    }

  
  workspace->tomo = (float*)malloc(NdataOriginal * sizeof(float));
  workspace->blockRecon = (float*)malloc(Nvoxels * sizeof(float));  
  
  /*
  checkCudaErrors( cudaMallocHost((void **)&workspace->tomo, NdataOriginal * sizeof(float)) );
  checkCudaErrors( cudaMallocHost((void **)&workspace->blockRecon, Nvoxels * sizeof(float)) );
  */
  checkCudaErrors( cudaMallocHost((void **)&workspace->blockSino, NdataNew*sizeof(float)) );
  
  return workspace;
}

raft_tomo* cloneWorkspace(raft_tomo* sourceWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)malloc(sizeof(*workspace));
  
  memcpy(workspace, sourceWorkspace, sizeof(*workspace));

  return workspace;
}

void* allocateWorkItemGpuMemory_normalization(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
 
  raft_allocate_sinogram_normalization_gpu_memory(workspace);
  
  return workspace;
}

void* allocateWorkItemGpuMemory_lowpass(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
 
  raft_allocate_sinogram_lowpass_gpu_memory(workspace);
  
  return workspace;
}



void* allocateWorkItemGpuMemory_backprojection(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  raft_allocate_backprojection_gpu_memory(workspace);
  
  return workspace;
}

void* freeWorkItemGpuMemory_backprojection(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  raft_deallocate_backprojection_gpu_memory(workspace);
  
  return workspace;
}


void* allocateWorkItemGpuMemory_backprojection_bst(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  raft_allocate_backprojection_bst_gpu_memory(workspace);
  
  return workspace;
}

void* freeWorkItemGpuMemory_backprojection_bst(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  raft_deallocate_backprojection_bst_gpu_memory(workspace);
  
  return workspace;
}



void* freeWorkItemGpuMemory_normalization(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  raft_deallocate_sinogram_normalization_gpu_memory(workspace);
  
  return workspace;
}


void* freeWorkItemGpuMemory_lowpass(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  raft_deallocate_sinogram_lowpass_gpu_memory(workspace);
  
  return workspace;
}

void* readTomoBlock(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  struct timespec TimeStart, TimeEnd;
  clock_gettime(CLOCK, &TimeStart);
  
  raft_read_tomo_block(workspace);
      
  if(workspace->fromSinogram)
    {
      int Nvoxels = (workspace->Nrays)*(workspace->Nangles)*(workspace->blockSize);
      
      //cudaMemcpy( workspace->blockSino, workspace->tomo, Nvoxels*sizeof(float),cudaMemcpyHostToHost);
      memcpy( workspace->blockSino, workspace->tomo, Nvoxels*sizeof(float));      
    }
  
  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  
  if(workspace->printTiming)	
    fprintf(stderr,"\n[%d]\t%lf\tRead H5 (CPU)",workspace->nblock,elapsed);  

  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;

  return workspace;
}

void* backprojection_slantstack_gpu(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;

  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  
  cudaSetDevice( gpuIdProcess );
  
  //cudaSetDevice( workspace-> gpuId );
  

  allocateWorkItemGpuMemory_backprojection(workspace);

  struct timespec TimeStart, TimeEnd;
  clock_gettime(CLOCK, &TimeStart);

  raft_backprojection_slantstack_gpu(workspace->blockRecon, 
				     workspace->blockSino,
				     workspace);
  
  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  if(workspace->printTiming)	
    fprintf(stderr,"\n[%d]\t%lf\tBackprojection (GPU)",workspace->nblock,elapsed);  
  
  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;
      

  /*
    {
    int Nrays=workspace->Nrays;
    int Nangles=workspace->Nangles;
    
    int m,n,z=0;
    
    FILE *fp=fopen("debuga1.dat","w");
    for(m=0;m<workspace->sizeImage;m++)
    {
    for(n=0;n<workspace->sizeImage;n++)
    fprintf(fp,"%lf ",workspace->blockRecon[z*workspace->sizeImage*workspace->sizeImage+m*workspace->sizeImage+n]);
	  
    fprintf(fp,"\n");
    }
    fclose(fp);


    int m,n,z=0;
    
    FILE *fp=fopen("debuga.dat","w");
    for(m=0;m<Nangles;m++)
    {
    for(n=0;n<Nrays;n++)
    fprintf(fp,"%lf ",workspace->blockSino[z*Nrays*Nangles+m*Nrays+n]);
    fprintf(fp,"\n");
    }
    fclose(fp);
    }
  */

    
  /////////////////////////////////
  //// check sinogram function
  /*
    int Nrays=workspace->Nrays;
    int Nangles=workspace->Nangles;
  
    {
    int m,n,z=0;
    FILE *fp=fopen("dado.dat","w");
    
    for(m=0;m<Nangles;m++)
    {
    for(n=0;n<Nrays;n++)
    fprintf(fp,"%lf ",workspace->tomo[z*Nrays*Nangles+m*Nrays+n]);
    fprintf(fp,"\n");
    }
    fclose(fp);
    }

    raft_radon_slantstack_gpu(workspace->blockSino, 
    workspace->blockRecon,
    workspace);

    {
    int m,n,z=0;
    FILE *fp=fopen("dadogpu.dat","w");
   
    for(m=0;m<Nangles;m++)
    {
    for(n=0;n<Nrays;n++)
    fprintf(fp,"%lf ",workspace->blockSino[z*Nrays*Nangles+m*Nrays+n]);
    fprintf(fp,"\n");
    }
    fclose(fp);
    }
  */
  
  //////////////
  
  freeWorkItemGpuMemory_backprojection(workspace);
  
  return workspace;
}


void* backprojection_bst_gpu(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;

  gpu_ids_t* gpus = workspace->gpus;
  int* gpuIds = gpus->gpuIds;
  int gpusPerNode = gpus->maxGpuPerNode;
  
  int gpuIdProcess = (gpuIds[(workspace->processId % gpusPerNode)]);

  cudaSetDevice( gpuIdProcess );
  
  //cudaSetDevice( workspace-> gpuId );
  
  allocateWorkItemGpuMemory_backprojection_bst(workspace);

  struct timespec TimeStart, TimeEnd;
  clock_gettime(CLOCK, &TimeStart);

  raft_backprojection_bst_gpu(workspace->blockRecon, 
			      workspace->blockSino,
			      workspace);

  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  //if(workspace->printTiming)	
  //  fprintf(stderr,"\n[%d]\t%lf\tBackprojection (GPU)",workspace->nblock,elapsed);  
    
  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;
  
  freeWorkItemGpuMemory_backprojection_bst(workspace);
  
  return workspace;
}

/*
void* em_emission_gpu(void *wrappedWorkspace)
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;
  
  cudaSetDevice( workspace->gpuId );
    
  //raft_allocate_em_emission_memory(workspace);
  
  raft_em_emission_gpu(workspace->blockRecon,
		       workspace->blockSino,
		       workspace);
  
  //raft_allocate_em_emission_memory(workspace);
  
  return workspace;
}
*/

void* saveReconstructedBlock(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;

  struct timespec TimeStart, TimeEnd;
  clock_gettime(CLOCK, &TimeStart);

  raft_save_backprojection_block(workspace);
    
  clock_gettime(CLOCK, &TimeEnd);
  float elapsed = TIME(TimeEnd,TimeStart);
  if(workspace->printTiming)	
    fprintf(stderr,"\n[%d]\t%lf\tSave reconstructed block (CPU)",workspace->nblock,elapsed);  

  ////////////////////
  //total elapsed time
  workspace->elapsedTimes[0] += elapsed;
  
  return workspace;
}

void* destroyGpuThreadWorkItem(void* wrappedWorkspace) 
{
  raft_tomo* workspace = (raft_tomo*)wrappedWorkspace;

  
  free(workspace->tomo);  
  free(workspace->blockRecon);    
  cudaFreeHost(workspace->blockSino);
  /*
  cudaFreeHost(workspace->blockRecon);
  cudaFreeHost(workspace->tomo);
  */
  
  free(workspace);
    
  return NULL;
}

