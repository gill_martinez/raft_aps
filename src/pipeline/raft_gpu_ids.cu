#include <stdlib.h>

#include "raft_gpu_ids.h"

gpu_ids_t* createGpuIds(int numGpus, int* ids) 
{
  int arraySize = sizeof(int) * numGpus;
  int size = sizeof(gpu_ids_t) + arraySize;
  gpu_ids_t* gpuIds = (gpu_ids_t*)calloc(size, 1);

  gpuIds->numGpus = numGpus;

  if (numGpus > 0) {
    if (ids != NULL)
      memcpy(gpuIds->gpuIds, ids, arraySize);
  }

  return gpuIds;
}

void destroyGpuIds(gpu_ids_t* gpuIds) 
{
  free(gpuIds);
}
