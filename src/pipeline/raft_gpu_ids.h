#ifndef RAFT_GPU_IDS_H
#define RAFT_GPU_IDS_H

typedef struct {
  int maxGpuPerNode;
  int numGpus;
  int gpuIds[0];
} gpu_ids_t;

gpu_ids_t* createGpuIds(int numGpus, int* ids);
void destroyGpuIds(gpu_ids_t* gpuIds);

#endif
