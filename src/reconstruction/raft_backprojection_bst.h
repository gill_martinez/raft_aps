#ifndef RAFT_BACKPROJECTION_BST_H
#define RAFT_BACKPROJECTION_BST_H

#include "../common/raft_types.h"

__host__ void raft_allocate_backprojection_bst_gpu_memory(raft_tomo* workspace);

__host__ void raft_deallocate_backprojection_bst_gpu_memory(raft_tomo* workspace);

__host__ void raft_backprojection_bst_gpu(float *blockRecon, float *blockSino, raft_tomo *workspace);

__host__ void raft_backprojection_bst_gpu_dev2host(float *blockRecon, float *blockSinoDev, raft_tomo *workspace);

#endif //  RAFT_BACKPROJECTION_BST_H
