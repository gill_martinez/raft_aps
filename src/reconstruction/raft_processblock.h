#ifndef RAFT_PROCESSBLOCK_H
#define RAFT_PROCESSBLOCK_H

#include "../common/raft_types.h"

__host__ void raft_process_block_gpu(raft_tomo *workspace);

__host__ void raft_process_block_gpu_fromSinogram(raft_tomo *workspace);

__host__ void raft_process_block_gpu_bst(raft_tomo *workspace);

__host__ void raft_process_block_gpu_bst_fromSinogram(raft_tomo *workspace);

#endif // RAFT_PROCESSBLOCK_H
