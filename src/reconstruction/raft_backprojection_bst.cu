#include <stdlib.h>
#include <math.h>
#include <hdf5.h>

#include "../common/raft_types.h"
#include "../reconstruction/raft_processblock.h"
#include "../common/raft_memory.h"
#include "raft_backprojection_bst.h"

extern "C" {
#include <time.h>
}

#define BETA 1.8f


/**********sinogram--2--polar kernel************/

__global__ void sino2p(cufftComplex *d_result,
		       cudaTextureObject_t texture,
		       int nrays,
		       int nangles,
		       int blockSize)
{ 
  int tx = blockIdx.x*blockDim.x + threadIdx.x; //nrays
  int ty = blockIdx.y*blockDim.y + threadIdx.y; //nangles
  int tz = blockIdx.z*blockDim.z + threadIdx.z; //blockSize
  
  int new_nangles = nangles*2;
  int new_nrays   = nrays/2;

  
  if((tx < new_nrays) && (ty < new_nangles) && (tz < blockSize))
    {
      if(ty < nangles)
	{
	  d_result[tz*new_nangles*new_nrays+ty*new_nrays+tx].x = tex2D<float>(texture, ((tx+(nrays/2)) + 0.5f), ((ty+0.5f))+(tz*nangles)); 
	  
	  d_result[tz*new_nangles*new_nrays+ty*new_nrays+tx].y = 0.0;	
	}
      else
	{
	  d_result[tz*new_nangles*new_nrays+ty*new_nrays+tx].x = tex2D<float>(texture, ((nrays/2)-1-tx)+0.5f, ((ty-(new_nangles/2))+0.5f)+(tz*nangles));
	  
	  d_result[tz*new_nangles*new_nrays+ty*new_nrays+tx].y =0.0;
	}
    }
}


__global__ void zero_padding_bst(cufftComplex *padded_block,cufftComplex *sino_polar,int nraysZpad,int nrays,int nangles,int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x; //nrays
  int ty = blockIdx.y * blockDim.y + threadIdx.y; //nangles
  int tz = blockIdx.z * blockDim.z + threadIdx.z; /*blockSize*/
  
  int Nangles 	= nangles * 2;
  
  if(( tx < nrays/2) && (ty < Nangles) && (tz < blockSize))
  {
    int offset = tz*Nangles*nraysZpad+ty*nraysZpad+tx;
    int off    = tz*Nangles*nrays/2+ty*nrays/2+tx;
    
    padded_block[offset] = sino_polar[off];    
  }
}

/*
__global__ void kaiser_bessel_window(cufftComplex *data,float *window,int nraysZpad,int nangles,int blockSize)
{
  int tx = blockIdx.x*blockDim.x + threadIdx.x; //nrays
  int ty = blockIdx.y*blockDim.y + threadIdx.y; //nangles
  int tz = blockIdx.z*blockDim.z + threadIdx.z; ///*blockSize
  
  int Nangles = nangles*2;
  int nrays   = nraysZpad;
  //float b1,b2,arg_bessel,w_bessel;
  
  if(( tx < nrays) && (ty < Nangles) && (tz < blockSize))
  { 
    int off = tz*Nangles*nrays+ty*nrays+tx; 
    data[off].x = data[off].x * window[tx];
  }
}
*/

__global__ void kaiser_bessel_window(cufftComplex *data,int nraysZpad,int nangles,int blockSize)
{
  int tx = blockIdx.x*blockDim.x + threadIdx.x; //nrays
  int ty = blockIdx.y*blockDim.y + threadIdx.y; //nangles
  int tz = blockIdx.z*blockDim.z + threadIdx.z; ///*blockSize*/
  
  int Nangles = nangles*2;
  int nrays   = nraysZpad;
  float b1,b2,arg_bessel,w_bessel;
  
  if(( tx < nrays) && (ty < Nangles) && (tz < blockSize))
  { 
    /* apply Kaiser-Bessel Window for origin treatment */
    arg_bessel = BETA*sqrtf(1 - (((2*tx - nrays + 1)/(nrays - 1))*((2*tx - nrays + 1)/(nrays - 1))));
    b1 = cyl_bessel_i0f(arg_bessel);
    b2 = cyl_bessel_i0f(BETA);
    w_bessel =(fabsf(b1)/fabsf(b2));
    
    int off = tz*Nangles*nrays+ty*nrays+tx; 
    data[off].x = data[off].x * w_bessel;
    data[off].y = data[off].y * w_bessel;
    
  }
}

/**********Convolution BST kernel************/
__global__ void convBST(cufftComplex *block,float *sigma,int nraysZpad,int Nangles,int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x; //rays
  int ty = blockIdx.y * blockDim.y + threadIdx.y; //views
  int tz = blockIdx.z * blockDim.z + threadIdx.z;
  
  int nangles   = Nangles*2;
  int nrays     = nraysZpad; 
  
  if ( (tx < nrays) && (ty < nangles) && (tz < blockSize) )
  {

    // double factor = (1.0 / ( nrays * sigma[tx]));
    double factor = (12.0 / ( nrays * sigma[tx]));
    int offset = tz*nangles*nrays + ty*nrays + tx;   //accessing 3D matrix as 1D array
    
    /*get real and complex values*/
    double voxel_real 	  = block[offset].x;
    double voxel_complex   = block[offset].y; 
  
    block[offset].x = (voxel_real * factor);
    block[offset].y = (voxel_complex * factor);
    
  }
}

/**********polar--2--cartesian kernel   TEXTURE ************/

__global__ void polar2cartesian_fourier(cufftComplex *cartesian_block, 
					cudaTextureObject_t texture,
					int nraysZpad,
					int nrays,
					int nangles,
					int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x; //nrays
  int ty = blockIdx.y * blockDim.y + threadIdx.y; //nangles
  int tz = blockIdx.z * blockDim.z + threadIdx.z; //blockSize
  
  /*input values*/
  int nangles_in    = nangles * 2;
  int nrays_in	    = nraysZpad;
  int sizeImage     = nrays;
  double delta_theta = ((2*PI) / (nangles_in - 1)); 
 
  double ds 	  = 2.0/ (nrays - 1);
  double sigMax   = nrays_in * ds;
  
  double dsigma   = (sigMax / (double)(nrays - 1));
  
  double dx       = ((2 * sigMax) / (sizeImage - 1));
  double dy       = ((2 * sigMax) / (sizeImage - 1));
  double x0       = -sigMax;
  double y0       = -sigMax;
  
  cufftComplex p1,zero;
  zero.x 	 = 0.0;
  zero.y	 = 0.0;
  
  if( (tx < sizeImage) && (ty < sizeImage) && (tz < blockSize) )
  {
    double w1 = x0 + (tx * dx);
    double w2 = y0 + (ty * dy);
  
    double theta = atan2f(w2,w1) + PI; // atan2f returns value in the range [-pi,pi]; need + pi    
    double s =  hypotf(w1,w2);
    
    int i = (int) floor((theta )/delta_theta);
    int j = (int) floor((s)/dsigma);
  
    if( (i< nangles_in ) && (j< nrays_in ) )
    {      
      /*access polarBlock's real and complex voxel*/
      p1 = tex2D<cufftComplex>(texture,(j+0.5f), (i+0.5f) + (tz*nangles_in));
      
      cartesian_block[tz*sizeImage*sizeImage+(ty*sizeImage)+tx].x = p1.x/(float)nrays;
      cartesian_block[tz*sizeImage*sizeImage+(ty*sizeImage)+tx].y = p1.y/(float)nrays;
      
    }
    else
      cartesian_block[tz*sizeImage*sizeImage+(ty*sizeImage)+tx] = zero;
    
  }

}


__global__ void fft_shift_2d_bst (cufftComplex *data,int nrays,int nangles,int blockSize)
{ 
  
  /********************** shift 3D code based on:  ***********************************************************
   *													      *
   * https://github.com/marwan-abdellah/cufftShift/blob/master/Src/CUDA/Kernels/in-place/cufftShift_3D_IP.cu  *
   *													      *
   * 													      *	
   ************************************************************************************************************/
  
  // 3D Volume & 2D Slice & 1D Line
  int sLine = nangles;
  int sSlice = nrays*nangles;
  int sVolume = sSlice*blockSize;

  // Transformations Equations
  int sEq1 = (sVolume + sSlice + sLine) / 2;
  int sEq2 = (sVolume + sSlice - sLine) / 2;
  int sEq3 = (sVolume - sSlice + sLine) / 2;
  int sEq4 = (sVolume - sSlice - sLine) / 2;

  // Thread
  int xThreadIdx = threadIdx.x;
  int yThreadIdx = threadIdx.y;
  int zThreadIdx = threadIdx.z;

  // Block Width & Height & depth
  int blockWidth  = blockDim.x;
  int blockHeight = blockDim.y;
  int blockDepth  = blockDim.z;
  
  // Thread Index 2D
  int xIndex = blockIdx.x * blockWidth  + xThreadIdx;
  int yIndex = blockIdx.y * blockHeight + yThreadIdx;
  int zIndex = blockIdx.z * blockDepth  + zThreadIdx;

  // Thread Index Converted into 1D Index
  int index = (zIndex * sSlice) + (yIndex * sLine) + xIndex;

  cufftComplex regTemp;
  
  double a = 1-2*(xIndex&1);
  double b = 1-2*(yIndex&1);
  
  if (zIndex < blockSize / 2)
  {
      if (xIndex < nrays / 2)
      {
	  if (yIndex < nangles / 2)
	  {
	     regTemp = data[index];

	      // First Quad
	      data[index] = data[index + sEq1];

	      // Fourth Quad
	      data[index + sEq1].x = regTemp.x*(a*b);
	  }
	  else
	  {
	      regTemp = data[index];

	      // Third Quad
	      data[index] = data[index + sEq3];

	      // Second Quad
	      data[index + sEq3].x = regTemp.x*(a*b);
	  }
      }
      else
      {
	  if (yIndex < nangles / 2)
	  {
	      regTemp = data[index];

	      // Second Quad
	      data[index] = data[index + sEq2];

	      // Third Quad
	      data[index + sEq2].x = regTemp.x*(a*b);
	  }
	  else
	  {
	      regTemp = data[index];

	      // Fourth Quad
	      data[index] = data[index + sEq4];

	      // First Quad
	      data[index + sEq4].x = regTemp.x*(a*b);
	  }
      }
  }
  
}

__global__ void fft_shift_x(cufftComplex *data,
			    int Nrays,
			    int Nslices,
			    int blockSize)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  int ty = threadIdx.y + blockIdx.y*blockDim.y;
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
 
  float tmpx, tmpy;
 
  double a = 1-2*(tx&1);
  double b = 1-2*(ty&1);
  
  if( (tx < Nrays/2) && (ty<Nslices) && (tz < blockSize) )
    {
      tmpx = data[ tz*Nrays*Nslices + ty*Nrays + tx].x;
      tmpy = data[ tz*Nrays*Nslices + ty*Nrays + tx].y;
     
      data[ tz*Nrays*Nslices + ty*Nrays + tx].x =  data[ tz*Nrays*Nslices + ty*Nrays + (tx + Nrays/2)].x*(a*b);
      data[ tz*Nrays*Nslices + ty*Nrays + tx].y =  data[ tz*Nrays*Nslices + ty*Nrays + (tx + Nrays/2)].y;
     
      data[ tz*Nrays*Nslices + ty*Nrays + (tx + Nrays/2)].x =  tmpx*(a*b);
      data[ tz*Nrays*Nslices + ty*Nrays + (tx + Nrays/2)].y =  tmpy;
    }
}

__global__ void fft_shift_y(cufftComplex *data,
			    int Nrays,
			    int Nslices,
			    int blockSize)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x;
  int ty = threadIdx.y + blockIdx.y*blockDim.y;
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
 
  float tmpx, tmpy;
  
  if( (tx < Nrays) && (ty<Nslices/2) && (tz < blockSize) )
    {
      tmpx = data[ tz*Nrays*Nslices + ty*Nrays + tx].x;
      tmpy = data[ tz*Nrays*Nslices + ty*Nrays + tx].y;
     
      data[ tz*Nrays*Nslices + ty*Nrays + tx].x =  data[ tz*Nrays*Nslices + (ty + Nslices/2)*Nrays + tx].x;
      data[ tz*Nrays*Nslices + ty*Nrays + tx].y =  data[ tz*Nrays*Nslices + (ty + Nslices/2)*Nrays + tx].y;
     
      data[ tz*Nrays*Nslices + (ty + Nslices/2)*Nrays + tx].x =  tmpx;
      data[ tz*Nrays*Nslices + (ty + Nslices/2)*Nrays + tx].y =  tmpy;
    }
}

__global__ void bl_interpolation(float *data,cudaTextureObject_t texture,int nrays,int nraysZpad,int sizeImage,int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x;
  int ty = blockIdx.y * blockDim.y + threadIdx.y;
  int tz = blockIdx.z * blockDim.z + threadIdx.z;
  
  int old_Nx = nraysZpad;
  int old_Ny = nraysZpad;
  int Nx = sizeImage;
  int Ny = sizeImage;
  
  double alpha = 2.0;

  double x0 = -alpha; 
  double y0 = -alpha;
  double x1 =  alpha; 
  double y1 =  alpha; 
  
  double xx0 = -1.0; 
  double yy0 = -1.0;
  double xx1 =  1.0; 
  double yy1 =  1.0; 

  double old_dx = (x1-x0)/old_Nx;
  double old_dy = (y1-y0)/old_Ny;
  
  
  double dx = (xx1 - xx0)/Nx;
  double dy = (yy1 - yy0)/Ny;

  double p1;
  
  if( (tx < Nx) && (ty < Ny) && (tz < blockSize) )
    {    
      double x = (xx0) + (tx * dx); 
      double y = (yy0) + (ty * dy);
      
      int i = (int) floor((x - x0) / old_dx);
      int j = (int) floor((y - y0) / old_dy);
      
      if( (i < nraysZpad & i > -1 ) && (j < nraysZpad & j > -1))
	{   

	  p1 = tex2D<cufftComplex>(texture + tz*nraysZpad*nraysZpad,j+0.5f,i+0.5f).x;
	 
	  data[tz*sizeImage*sizeImage+ty*sizeImage+tx] = p1;
	}
      else
	data[tz*sizeImage*sizeImage+ty*sizeImage+tx] = 0.0;
    } 
}


__global__ void div_kernel(float *data,int nrays, int sizeImage,int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x;
  int ty = blockIdx.y * blockDim.y + threadIdx.y;
  int tz = blockIdx.z * blockDim.z + threadIdx.z;
  
  if( (tx < sizeImage) && (ty < sizeImage) && (tz < blockSize) )
  {
    int offset   = tz*sizeImage*sizeImage+ty*sizeImage+tx;
    data[offset] /= nrays;  
  }
}

__global__ void zoom_kernel(cufftComplex *data_out,cufftComplex *data_in,int sizeImage,int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x;
  int ty = blockIdx.y * blockDim.y + threadIdx.y;
  int tz = blockIdx.z * blockDim.z + threadIdx.z;
 
//   float ddx = (0.5*(2.0)) / sizeImage;
//   float ddy = (0.5*(2.0)) / sizeImage;
//   
//   float dx = 2.0 / sizeImage;
//   float dy = 2.0 / sizeImage;
//   
  int old_Nx   = sizeImage - 1;
  int old_Ny   = sizeImage - 1;
  int Nx       = sizeImage - 1;
  int Ny       = sizeImage - 1;
  
  double x0 = -1.0; 
  double y0 = -1.0;
  double x1 =  1.0; 
  double y1 =  1.0; 
  double old_dx = (x1-x0)/old_Nx;
  double old_dy = (y1-y0)/old_Ny;
  double dx = (x1-x0)/Nx;
  double dy = (y1-y0)/Ny;
  cufftComplex p1,p2,p3,p4;
  cufftComplex zero;
  zero.x = 0.0;
  zero.y = 0.0;
  
  if( (tx < Nx) && (ty < Ny) && (tz < blockSize) )
  {
    float x = ((x0)) + (tx * old_dx); 
    float y = ((y0)) + (ty * old_dy);
    
    int i = (int) floor((x - x0) / dx);
    int j = (int) floor((y - y0) / dy);
    
    if( (i< Nx) && (j< Ny))
    {	
      int offset = tz*sizeImage*sizeImage + j*sizeImage + i;    
      p1 = data_in[offset];
      p2 = data_in[tz*sizeImage*sizeImage + j*sizeImage + i+1];
      p3 = data_in[tz*sizeImage*sizeImage + (j+1)*sizeImage + i];
      p4 = data_in[tz*sizeImage*sizeImage + (j+1)*sizeImage + i+1];
      data_out[tz*sizeImage*sizeImage+ty*sizeImage+tx].x = (p1.x+p2.x+p3.x+p4.x)/4.0;
    }
    else
      data_out[tz*sizeImage*sizeImage+ty*sizeImage+tx] = zero;
  }
  
}


//Align a to nearest higher multiple of b
__host__ int iAlignUp_bst(int a, int b)
{
    return (a % b != 0) ? (a - a % b + b) : a;
}

__host__ int snapTransformSize_bst(int dataSize)
{
    int hiBit;
    unsigned int lowPOT, hiPOT;

    dataSize = iAlignUp_bst(dataSize, 16);

    for (hiBit = 31; hiBit >= 0; hiBit--)
        if (dataSize & (1U << hiBit))
        {
            break;
        }

    lowPOT = 1U << hiBit;

    if (lowPOT == (unsigned int)dataSize)
    {
        return dataSize;
    }

    hiPOT = 1U << (hiBit + 1);

    if (hiPOT <= 1024)
    {
        return hiPOT;
    }
    else
    {
        return iAlignUp_bst(dataSize, 512);
    }
}

void debuga(cufftComplex *data,int nrays,int nangles,int blockSize)
{
    
  cufftComplex *temp = (cufftComplex *) malloc ((nrays)*(nangles)*blockSize*sizeof(cufftComplex));
  cudaMemcpy(temp,data,(nrays)*(nangles)*blockSize*sizeof(cufftComplex),cudaMemcpyDeviceToHost);
  
  int slice  = 0,i,j;
  int offset = (nrays)*(nangles); 
  
  for(i=0;i<nrays;i++)
  {
    for(j=0;j<nangles;j++)
    {
   	fprintf(stdout,"%f ",temp[(slice*offset)+j*nrays+i].x);
    }
    fprintf(stdout,"\n");
  }
  
  free(temp);
  
}

void debugaReal(float *data,int nrays,int nangles,int blockSize)
{
  float *temp = (float *) malloc ((nrays)*(nangles)*blockSize*sizeof(float));
  cudaMemcpy(temp,data,(nrays)*(nangles)*blockSize*sizeof(float),cudaMemcpyDeviceToHost);
  
  int slice  = 0,i,j;
  int offset = (nrays)*(nangles); 
  
  for(i=0;i<nrays;i++)
  {
    for(j=0;j<nangles;j++)
    {
   	fprintf(stdout,"%f ",temp[(slice*offset+j*nrays)+i]);
    }
    fprintf(stdout,"\n");
  }
  
  free(temp);
  
}


__host__ void raft_allocate_backprojection_bst_gpu_memory(raft_tomo* workspace) 
{
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;

  int Nrays, Nangles;
  if(workspace->hybrid==1)
    {
      Nrays   = 2*workspace->axis;
      Nangles = (int) workspace->measured_nangles/2;	
    }
  else
    {
      Nrays   = workspace->measured_nrays + 2*workspace->zpad;
      Nangles = workspace->measured_nangles;
    }

  workspace->bst = (bst *)malloc (sizeof(bst));

  /*get arguments*/
  workspace->bst->nrays    = Nrays;
  workspace->bst->nangles  = Nangles; 
  
  //filling struct parameters
  workspace->bst->paddingCoeff  = 4;
  workspace->bst->zpad          = snapTransformSize_bst(workspace->bst->paddingCoeff * int(workspace->bst->nrays/2)-1);

  workspace->bst->blockSize   = delta;
  workspace->bst->pi 	      = PI;
  workspace->bst->sizeImage   = workspace->sizeImage;

  workspace->bst->sigma	      = (float *) malloc (workspace->bst->zpad*sizeof(float));
  
  /*alloc device memory*/
  checkCudaErrors(cudaMalloc((void **) &workspace->bst->d_block_zpad,(workspace->bst->zpad)*workspace->bst->nangles*2*workspace->bst->blockSize*sizeof(cufftComplex)));
  checkCudaErrors(cudaMalloc((void **) &workspace->bst->d_cartesian_block,workspace->bst->sizeImage*workspace->bst->sizeImage*workspace->bst->blockSize*sizeof(cufftComplex)));
  checkCudaErrors(cudaMalloc((void **) &workspace->bst->d_reconstruct_block,workspace->bst->sizeImage*workspace->bst->sizeImage*workspace->bst->blockSize*sizeof(float)));
  checkCudaErrors(cudaMalloc((void **) &workspace->bst->d_sigma,workspace->bst->zpad*sizeof(float)));
  checkCudaErrors(cudaMalloc((void **) &workspace->bst->d_sino_polar,(workspace->bst->nrays/2)*(workspace->bst->nangles*2)*workspace->bst->blockSize*sizeof(cufftComplex)));
  
  workspace->bst->channelDesc[0] = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
  workspace->bst->channelDesc[1] = cudaCreateChannelDesc(32,32,0,0,cudaChannelFormatKindFloat);
  
  checkCudaErrors(cudaMallocArray(&workspace->bst->dst[0], &workspace->bst->channelDesc[0],
				  Nrays,Nangles*delta,cudaArrayDefault));
   
  checkCudaErrors(cudaMallocArray(&workspace->bst->dst[1], &workspace->bst->channelDesc[1],
  				  workspace->bst->zpad,Nangles*2*delta,cudaArrayDefault));
  
  raft_create_texture_object_for_array(&workspace->bst->textureObject[0], workspace->bst->dst[0]);
  raft_create_texture_object_for_array(&workspace->bst->textureObject[1], workspace->bst->dst[1]);
  
  /*##############################   Free memory requirement #########################################
    #												     #
    #  The first program call to any cuFFT function causes the initialization of the cuFFT kernels.  #
    #  This can fail if there is not enough free memory on the GPU. It is advisable to initialize    #
    #  cufft first (e.g. by creating a plan) and then allocating memory.			     #
    #  Read more at: http://docs.nvidia.com/cuda/cufft/index.html#ixzz4PcxVsE9o			     #
    #												     #
    ##################################################################################################
										
    /*Real_2_Complex Plan*/
  
  int rank    = 1;					//--- 1D FFTS
  int n[]     = { workspace->bst->zpad};	        //--- Size of the Fourier transform
  int istride = 1;
  int ostride = 1;					//--- Distance between two successive input/output elements
  int idist   =  workspace->bst->zpad;
  int odist   =  workspace->bst->zpad;			// Distance between Batches
  //int inembed[] = {0};				// Input size with pitch (ignored for 1D transforms)
  //int onembed[] = {0};				// Output size with pitch (ignored for 1D transforms)
  int batch   = workspace->bst->nangles*2*workspace->bst->blockSize; //Number of batched executions
  
  if( cufftPlanMany(&workspace->bst->plan_R2C,rank,n,
		    NULL,istride,idist,
		    NULL,ostride,odist,CUFFT_C2C,batch)!= CUFFT_SUCCESS)
    {
      fprintf(stderr,"CUFFT error: Plan R2C creation failed\n");     
    } 
  
  /*Complex_2_Real Plan*/
  int rank2 = 2;
  int f[]   = {workspace->bst->sizeImage,workspace->bst->sizeImage};	//{(bst->nrays/2 + bst->zpad)*bst->nangles*2};
  istride   = 1; 
  ostride   = 1;
  idist     = workspace->bst->sizeImage*workspace->bst->sizeImage;	//bst->nrays*bst->nrays;
  odist     = workspace->bst->sizeImage*workspace->bst->sizeImage;	//bst->nrays*bst->nrays;
  batch     = workspace->bst->blockSize;
  
  if (cufftPlanMany(&workspace->bst->plan_C2R,rank2,f,
		    NULL,istride,idist,
		    NULL,ostride,odist,CUFFT_C2C,batch)!=CUFFT_SUCCESS)
    {
      fprintf(stderr,"CUFFT error: Plan C2R creation failed\n");
    }
}


__host__ void raft_deallocate_backprojection_bst_gpu_memory(raft_tomo* workspace) 
{
  cudaFreeArray(workspace->bst->dst[0]);
  cudaFreeArray(workspace->bst->dst[1]);

  cufftDestroy(workspace->bst->plan_C2R);
  cufftDestroy(workspace->bst->plan_R2C);
  
  cudaFree(workspace->bst->d_cartesian_block);
  cudaFree(workspace->bst->d_block_zpad);
  cudaFree(workspace->bst->d_reconstruct_block);
  cudaFree(workspace->bst->d_sigma);
  cudaFree(workspace->bst->d_sino_polar);
  
  free(workspace->bst->sigma);
  free(workspace->bst);
}


__global__ void kernel_set_imaginary_to_zero(cufftComplex *block, 
					     float *array, 
					     int Nangles, 
					     int Nrays,
					     int blockSize)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  int voxel;
  
  if( (tx< Nrays) && (ty<Nangles) && (tz < blockSize) )
    {
      voxel = tz*Nrays*Nangles+(ty*Nrays)+tx;
      
      block[ voxel ].x = array[ voxel ];
      block[ voxel ].y = 0;
    }
}


__global__ void cpy_kernel(float *real_data,
			   cufftComplex *complex_data,
			   int sizeImage,
			   int blockSize)
{

  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  int voxel;

  if( (tx< sizeImage) && (ty<sizeImage) && (tz < blockSize) )
    {
      voxel = tz*sizeImage*sizeImage+(ty*sizeImage)+tx;
      real_data[ voxel ] = complex_data[ voxel ].x;
    }
}


void raft_backprojection_bst_gpu(float *blockRecon,
				 float *blockSino,
				 raft_tomo *workspace)
{
  int i;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;

  float time;
  float totalTime = 0.0;
  float memoryTime;
  float totalMemoryTime = 0.0;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  dim3 ThreadsPerBlock(TPBX,TPBY,TPBZ);
  
  //define sigma vector
  workspace->bst->dsigma = 2.0/workspace->bst->nrays;
  workspace->bst->sigma[0] = workspace->bst->zpad;

#pragma omp simd 
  for(i=1;i<workspace->bst->zpad;i++)
    workspace->bst->sigma[i] = workspace->bst->dsigma * i;

  cudaEventRecord(start);
  
  //copying data host to device
  cudaMemcpy(workspace->bst->d_sigma, workspace->bst->sigma, workspace->bst->zpad*sizeof(float), cudaMemcpyHostToDevice);

  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;

  
  /*############### 1 Step --> sino2polar ###################################*/
  cudaEventRecord(start);
  
  cudaMemcpyToArray(workspace->bst->dst[0],
		    0,
		    0,
		    blockSino,
		    workspace->bst->nrays*workspace->bst->nangles*delta*sizeof(float),
		    cudaMemcpyHostToDevice);

  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;

  
  dim3 gridBlock(ceil((workspace->bst->nrays/2)/ThreadsPerBlock.x)+1,
		 ceil(workspace->bst->nangles*2/ThreadsPerBlock.y)+1,
		 ceil(delta/ThreadsPerBlock.z)+1);
  
  cudaEventRecord(start);
  
  sino2p<<<gridBlock,ThreadsPerBlock>>>(workspace->bst->d_sino_polar,
					workspace->bst->textureObject[0],
					workspace->bst->nrays,
					workspace->bst->nangles,
					delta);

  
  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: sino2p",time/1000,workspace->nblock);  
  
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError()); 
  
  //debuga(workspace->bst->d_sino_polar,workspace->bst->nrays/2,workspace->bst->nangles*2,workspace->bst->blockSize);
  
  /*############### 2 Step --> Add Zero Padding ###################################*/  
  
  cudaMemset(workspace->bst->d_block_zpad, 0, workspace->bst->zpad*workspace->bst->nangles*2*delta*sizeof(cufftComplex));
  
  dim3 gridPadded(ceil((workspace->bst->zpad)/ThreadsPerBlock.x)+1,
		  ceil(workspace->bst->nangles*2/ThreadsPerBlock.y)+1,
		  ceil(delta/ThreadsPerBlock.z)+1);
  
  cudaEventRecord(start);
  
  zero_padding_bst<<<gridPadded,ThreadsPerBlock>>>(workspace->bst->d_block_zpad,
						   workspace->bst->d_sino_polar,
						   workspace->bst->zpad,
						   workspace->bst->nrays,
						   workspace->bst->nangles,
						   delta);
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: zeroPadding",time/1000,workspace->nblock);  
  
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError()); 
  
  
  /*############### 4 Step --> Apply Kaiser Bessel Window ###################################*/

  cudaEventRecord(start);
  
  kaiser_bessel_window<<<gridPadded,ThreadsPerBlock>>>(workspace->bst->d_block_zpad,
						       workspace->bst->zpad,
						       workspace->bst->nangles,
						       delta);
  
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
 
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: Kaiser Bessel Window",time/1000,workspace->nblock);  

  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError()); 
  
  /*############### 5 Step --> Apply 1D fourier ###################################*/
  
  cudaEventRecord(start);
  
  /*--------- fourier transform C2C FORWARD --------------------*/
  
  if( cufftExecC2C(workspace->bst->plan_R2C,
		   workspace->bst->d_block_zpad,
		   workspace->bst->d_block_zpad,
		   CUFFT_FORWARD) != CUFFT_SUCCESS)
  {
    fprintf(stderr,"CUFFT error: execR2C forward failed\n ");    
  }
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: 1D Fourier",time/1000,workspace->nblock);  
  
  /*############### 6 Step --> Convolution ###################################*/
  
  cudaEventRecord(start);

  /************call Convolution kernel***************/
  
  convBST<<<gridPadded,ThreadsPerBlock>>>(workspace->bst->d_block_zpad,
					  workspace->bst->d_sigma,
					  workspace->bst->zpad,
					  workspace->bst->nangles,
					  delta);
  
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: Convolution",time/1000,workspace->nblock);  
  
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError()); 

  /*############### 7 Step --> polar2cartesian in frequency Domain ###################################*/
  
  /**********perform polar --> Cartesian  ***************/  

  cudaEventRecord(start);
  
  cudaMemcpyToArray(workspace->bst->dst[1],
		    0,
		    0,
		    workspace->bst->d_block_zpad,
		    workspace->bst->zpad*workspace->bst->nangles*2*delta*sizeof(cufftComplex),
		    cudaMemcpyDeviceToDevice);

  
  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;

  
  dim3 gridBlockComplex(ceil(workspace->bst->sizeImage/ThreadsPerBlock.x),
			ceil(workspace->bst->sizeImage/ThreadsPerBlock.y),
			ceil(delta/ThreadsPerBlock.z)+1);
  
  /**************kernel polar --- > cartesian (in frequency domain) **********/  
  
  cudaEventRecord(start);

  polar2cartesian_fourier<<<gridBlockComplex,ThreadsPerBlock>>>(workspace->bst->d_cartesian_block,
								workspace->bst->textureObject[1],
								workspace->bst->zpad,
								workspace->bst->nrays,
								workspace->bst->nangles,
								delta);
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
 
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: polar2cartesian",time/1000,workspace->nblock);  

  cudaDeviceSynchronize();    
  checkCudaErrors(cudaPeekAtLastError());

 
/*############### 8 Step --> Apply 2D Fourier ###################################*/
  
  cudaEventRecord(start);
  
  if (cufftExecC2C(workspace->bst->plan_C2R,
		   workspace->bst->d_cartesian_block,workspace->bst->d_cartesian_block,
		   CUFFT_INVERSE)!= CUFFT_SUCCESS)
  {
    fprintf(stderr,"CUFFT error: execC2R forward failed\n"); 
  }
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: 2D Fourier",time/1000,workspace->nblock);  

  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError());

/*############### 9 Step --> Apply x and y shift directions ###################################*/
    
  cudaEventRecord(start);
  
  fft_shift_x<<<gridBlockComplex,ThreadsPerBlock>>>(workspace->bst->d_cartesian_block,
						    workspace->bst->sizeImage,
						    workspace->bst->sizeImage,
						    delta);
  
  fft_shift_y<<<gridBlockComplex,ThreadsPerBlock>>>(workspace->bst->d_cartesian_block,
						    workspace->bst->sizeImage,
						    workspace->bst->sizeImage,
						    delta);
    
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  totalTime += time;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: Shift x,y",time/1000,workspace->nblock);  
 
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError());
  
  /*############### 10 Step --> copy complex vector --> real vector ###################################*/
  
  cudaEventRecord(start);
  
  cpy_kernel<<<gridBlockComplex,ThreadsPerBlock>>>(workspace->bst->d_reconstruct_block,
						   workspace->bst->d_cartesian_block,
						   workspace->bst->sizeImage,
						   workspace->bst->blockSize);  
  cudaEventRecord(stop);
 
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: get real part",time/1000,workspace->nblock);  

  
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError());
 
  cudaEventRecord(start);
    
  //copying data device to host
  cudaMemcpy(workspace->blockRecon,workspace->bst->d_reconstruct_block,workspace->bst->sizeImage*workspace->bst->sizeImage*delta*sizeof(float),cudaMemcpyDeviceToHost);


  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;

  if(workspace->printTiming){	
    //fprintf(stderr,"\n[%d]\t%lf\tExecute BST Kernel (GPU)",workspace->nblock,totalTime/1000);  
    //fprintf(stderr,"\n[%d]\t%lf\tMemory Time BST Kernel (GPU)",workspace->nblock,totalMemoryTime/1000);  
    fprintf(stderr,"\n[%d]\t%lf\t -->Total BST time (GPU) -- process[%d]",workspace->nblock,((totalTime + totalMemoryTime) / 1000),workspace->processId );  

  }
  
  
  cudaEventDestroy(start);
  cudaEventDestroy(stop);
  
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@                        @@@@@@@@@@@@@@@@@@@@@ 
  @@@@@@@@@@@@@@@@@@@@@@ from device to host    @@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@                        @@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

__host__ void raft_backprojection_bst_gpu_dev2host(float *blockRecon,
						   float *blockSinoDev,
						   raft_tomo *workspace)
{
  int i;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;

  float time;
  float totalTime = 0.0;
  float memoryTime;
  float totalMemoryTime = 0.0;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  dim3 ThreadsPerBlock(TPBX,TPBY,TPBZ);
  
  //define sigma vector
  workspace->bst->dsigma = 2.0/workspace->bst->nrays;
  workspace->bst->sigma[0] = workspace->bst->zpad;
  for(i=1;i<workspace->bst->zpad;i++)
    workspace->bst->sigma[i] = workspace->bst->dsigma * i;

  
  cudaEventRecord(start);
  
  //copying data host to device
  cudaMemcpy(workspace->bst->d_sigma, workspace->bst->sigma, workspace->bst->zpad*sizeof(float), cudaMemcpyHostToDevice);
  
  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;

  
  /*############### 1 Step --> sino2polar ###################################*/

  cudaEventRecord(start);

  cudaMemcpyToArray(workspace->bst->dst[0],
		    0,
		    0,
		    blockSinoDev,
		    workspace->bst->nrays*workspace->bst->nangles*delta*sizeof(float),
		    cudaMemcpyDeviceToDevice);


  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;

  
  dim3 gridBlock(ceil((workspace->bst->nrays/2)/ThreadsPerBlock.x)+1,
		 ceil(workspace->bst->nangles*2/ThreadsPerBlock.y)+1,
		 ceil(delta/ThreadsPerBlock.z)+1);
  
  cudaEventRecord(start);
  
  sino2p<<<gridBlock,ThreadsPerBlock>>>(workspace->bst->d_sino_polar,
					workspace->bst->textureObject[0],
					workspace->bst->nrays,
					workspace->bst->nangles,
					delta);

  
  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: sino2p",time/1000,workspace->nblock);  
  
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError()); 
  
  //debuga(workspace->bst->d_sino_polar,workspace->bst->nrays/2,workspace->bst->nangles*2,workspace->bst->blockSize);
  
  /*############### 2 Step --> Add Zero Padding ###################################*/  
  
  cudaMemset(workspace->bst->d_block_zpad, 0, workspace->bst->zpad*workspace->bst->nangles*2*delta*sizeof(cufftComplex));
  
  dim3 gridPadded(ceil((workspace->bst->zpad)/ThreadsPerBlock.x)+1,
		  ceil(workspace->bst->nangles*2/ThreadsPerBlock.y)+1,
		  ceil(delta/ThreadsPerBlock.z)+1);
  
  cudaEventRecord(start);
  
  zero_padding_bst<<<gridPadded,ThreadsPerBlock>>>(workspace->bst->d_block_zpad,
						   workspace->bst->d_sino_polar,
						   workspace->bst->zpad,
						   workspace->bst->nrays,
						   workspace->bst->nangles,
						   delta);
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: zeroPadding",time/1000,workspace->nblock);  
  
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError()); 
  
  
  /*############### 4 Step --> Apply Kaiser Bessel Window ###################################*/

  cudaEventRecord(start);
  
  kaiser_bessel_window<<<gridPadded,ThreadsPerBlock>>>(workspace->bst->d_block_zpad,
						       workspace->bst->zpad,
						       workspace->bst->nangles,
						       delta);
  
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
 
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: Kaiser Bessel Window",time/1000,workspace->nblock);  

  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError()); 
  
  /*############### 5 Step --> Apply 1D fourier ###################################*/
  
  cudaEventRecord(start);
  
  /*--------- fourier transform C2C FORWARD --------------------*/
  
  if( cufftExecC2C(workspace->bst->plan_R2C,
		   workspace->bst->d_block_zpad,
		   workspace->bst->d_block_zpad,
		   CUFFT_FORWARD) != CUFFT_SUCCESS)
  {
    fprintf(stderr,"CUFFT error: execR2C forward failed\n ");    
  }
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: 1D Fourier",time/1000,workspace->nblock);  
  
  /*############### 6 Step --> Convolution ###################################*/
  
  cudaEventRecord(start);

  /************call Convolution kernel***************/
  
  convBST<<<gridPadded,ThreadsPerBlock>>>(workspace->bst->d_block_zpad,
					  workspace->bst->d_sigma,
					  workspace->bst->zpad,
					  workspace->bst->nangles,
					  delta);
  
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: Convolution",time/1000,workspace->nblock);  
  
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError()); 

  /*############### 7 Step --> polar2cartesian in frequency Domain ###################################*/
  
  /**********perform polar --> Cartesian  ***************/  

  cudaEventRecord(start);
  
  cudaMemcpyToArray(workspace->bst->dst[1],
		    0,
		    0,
		    workspace->bst->d_block_zpad,
		    workspace->bst->zpad*workspace->bst->nangles*2*delta*sizeof(cufftComplex),
		    cudaMemcpyDeviceToDevice);

  
  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;

  
  dim3 gridBlockComplex(ceil(workspace->bst->sizeImage/ThreadsPerBlock.x),
			ceil(workspace->bst->sizeImage/ThreadsPerBlock.y),
			ceil(delta/ThreadsPerBlock.z)+1);
  
  /**************kernel polar --- > cartesian (in frequency domain) **********/  
  
  cudaEventRecord(start);

  polar2cartesian_fourier<<<gridBlockComplex,ThreadsPerBlock>>>(workspace->bst->d_cartesian_block,
								workspace->bst->textureObject[1],
								workspace->bst->zpad,
								workspace->bst->nrays,
								workspace->bst->nangles,
								delta);
  cudaEventRecord(stop);
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
 
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: polar2cartesian",time/1000,workspace->nblock);  

  cudaDeviceSynchronize();    
  checkCudaErrors(cudaPeekAtLastError());

 
/*############### 8 Step --> Apply 2D Fourier ###################################*/
  
  cudaEventRecord(start);
  
  if (cufftExecC2C(workspace->bst->plan_C2R,
		   workspace->bst->d_cartesian_block,workspace->bst->d_cartesian_block,
		   CUFFT_INVERSE)!= CUFFT_SUCCESS)
  {
    fprintf(stderr,"CUFFT error: execC2R forward failed\n"); 
  }
  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: 2D Fourier",time/1000,workspace->nblock);  

  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError());

/*############### 9 Step --> Apply x and y shift directions ###################################*/
    
  cudaEventRecord(start);
  
  fft_shift_x<<<gridBlockComplex,ThreadsPerBlock>>>(workspace->bst->d_cartesian_block,
						    workspace->bst->sizeImage,
						    workspace->bst->sizeImage,
						    delta);
  
  fft_shift_y<<<gridBlockComplex,ThreadsPerBlock>>>(workspace->bst->d_cartesian_block,
						    workspace->bst->sizeImage,
						    workspace->bst->sizeImage,
						    delta);
  
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  totalTime += time;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: Shift x,y",time/1000,workspace->nblock);  
 
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError());
  
  /*############### 10 Step --> copy complex vector --> real vector ###################################*/
  
  cudaEventRecord(start);
  
  cpy_kernel<<<gridBlockComplex,ThreadsPerBlock>>>(workspace->bst->d_reconstruct_block,
						   workspace->bst->d_cartesian_block,
						   workspace->bst->sizeImage,
						   workspace->bst->blockSize);  
  cudaEventRecord(stop);
 
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  totalTime += time;
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->BST Kernel: get real part",time/1000,workspace->nblock);  

  
  
  cudaDeviceSynchronize();
  checkCudaErrors(cudaPeekAtLastError());
 
  cudaEventRecord(start);
  
  //copying data device to host
  cudaMemcpy(workspace->blockRecon,workspace->bst->d_reconstruct_block,workspace->bst->sizeImage*workspace->bst->sizeImage*delta*sizeof(float),cudaMemcpyDeviceToHost);


  cudaEventRecord(stop);  
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;

  if(workspace->printTiming){	
    //fprintf(stderr,"\n[%d]\t%lf\tExecute BST Kernel (GPU)",workspace->nblock,totalTime/1000);  
    //fprintf(stderr,"\n[%d]\t%lf\tMemory Time BST Kernel (GPU)",workspace->nblock,totalMemoryTime/1000);  
    fprintf(stderr,"\n[%d]\t%lf\t -->Total BST time (GPU) -- process[%d]",workspace->nblock,((totalTime + totalMemoryTime) / 1000),workspace->processId );
  }


  cudaEventDestroy(start);
  cudaEventDestroy(stop);  
}
