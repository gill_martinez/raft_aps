#ifndef RAFT_CENTERSINO_H
#define RAFT_CENTERSINO_H

#include "../common/raft_types.h"


__host__ void raft_translate_sino(raft_tomo *workspace,
				  float *sino,
				  float c);

__host__ float raft_find_center(raft_tomo *workspace, 
				float *sino);


__host__ void translateSignal(raft_tomo *workspace,
			      float *output,
			      float *signal,
			      float c);

__host__ void translateSino(raft_tomo *workspace,
			    float *sino,
			    float c);


__host__ void raft_recentersino_withregularization_gpu(float *blockSino,
						       raft_tomo *workspace);




#endif // RAFT_CENTERSINO_H

