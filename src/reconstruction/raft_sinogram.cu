#include <stdlib.h>
#include <math.h>
#include <hdf5.h>

#include "../common/raft_types.h"
#include "../common/raft_memory.h"
#include "raft_sinogram.h"


extern "C" {
#include <time.h>
}

__constant__ float TOL = 0.0001f;
__constant__ float TOL2 = 0.0000001f;


/*------------------------------------------------------
  Sinogram normalization:
  
  sino = -log ( (counts-dark) / (flat-dark) )

  Remark: flat, dark and counts are given as a texture
  ------------------------------------------------------*/

__global__ void kernel_sinogram_normalization(float *sino,
					      cudaTextureObject_t texFlatBefore,
					      cudaTextureObject_t texFlatAfter,
					      cudaTextureObject_t texDark,
					      cudaTextureObject_t texTomo,
					      int delta,
					      int Nrays,
					      int Nangles,
					      int offset,
					      int sliceInit)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  float pflat,pdark,ptomo,psino;
  float corr_flat,corr_dark,buff_img;
  
  if( ((tx)<Nrays) && ((ty)<Nangles) && ((tz)<delta) )
    {	             
      
      pflat  = tex2D<float>(texFlatBefore, tx + 0.5f, sliceInit + offset + tz + 0.5f);
      pdark  = tex2D<float>(texDark, tx + 0.5f, sliceInit + offset + tz + 0.5f);
      ptomo  = tex3D<float>(texTomo, tx + 0.5f, ty + 0.5f, tz + 0.5f);
	
      corr_flat = pflat - pdark;
      corr_dark = ptomo - pdark;
      buff_img  = (corr_dark/corr_flat);
      
      if(corr_flat<TOL)
	psino = 1.0f;
      
      if(buff_img<TOL)
	psino = 1.0f;
      
      psino  =  ((float)(ptomo-pdark)/(float)(pflat-pdark));
      
      sino[tz*Nrays*Nangles+(ty*Nrays)+tx] = -logf(psino);
    } 
}


/*------------------------------------------------------
  Sinogram normalization for 360 degrees measurements:
  
  sino = -log ( (counts-dark) / (flat-dark) )

  Remark: flat, dark and counts are given as a texture
  ------------------------------------------------------*/

__global__ void kernel_sinogram_normalization_360(float *sino,
						  cudaTextureObject_t texFlatBefore,
						  cudaTextureObject_t texFlatAfter,
						  cudaTextureObject_t texDark,
						  cudaTextureObject_t texTomo,
						  int delta,
						  int Nrays,
						  int Nangles,
						  int axis,
						  int offset,
						  int sliceInit)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;

  float pflat,pdark,ptomo,psino;
  float corr_flat,corr_dark,buff_img;
  
  int _Nrays_  = 2*axis;
  int _Nangles_= Nangles/2;

  if( ((tx)<_Nrays_) && ((ty)<_Nangles_) && ((tz)<delta) )
    {	 
      
      if( tx < axis )
	{
          pflat = tex2D<float>(texFlatBefore,tx+0.5f,sliceInit+offset+tz+0.5f);
          pdark = tex2D<float>(texDark,tx+0.5f,sliceInit+offset+tz+0.5f);
	  ptomo = tex3D<float>(texTomo, tx + 0.5f, ty + 0.5f, tz + 0.5f);
	  
	  corr_flat = pflat - pdark;
	  corr_dark = ptomo - pdark;
	  buff_img  = (corr_dark/corr_flat);
	  
	  if(corr_flat<TOL)
	    psino = 1.0f;
	  
	  if(buff_img<TOL)
	    psino = 1.0f;
	  
	  psino  =  ((float)(ptomo-pdark)/(float)(pflat-pdark));
	  
	  sino[tz*(_Nrays_)*(_Nangles_)+(ty*(_Nrays_))+tx] = -logf(psino);
	}
      else
	{
	  pflat = tex2D<float>(texFlatBefore,2*axis-tx + 0.5f,sliceInit+offset+tz+0.5f);
	  pdark = tex2D<float>(texDark,2*axis-tx + 0.5f,sliceInit+offset+tz+0.5f);
	  ptomo = tex3D<float>(texTomo,2*axis - tx + 0.5f, ty + _Nangles_ + 0.5f, tz + 0.5f);
	  
	  corr_flat = pflat - pdark;
	  corr_dark = ptomo - pdark;
	  buff_img  = (corr_dark/corr_flat);
	  
	  if(corr_flat<TOL)
	    psino = 1.0f;
	  
	  if(buff_img<TOL)
	    psino = 1.0f;
	  
	  psino  =  ((float)(ptomo-pdark)/(float)(pflat-pdark));
	  
	  sino[tz*(_Nrays_)*(_Nangles_)+(ty*(_Nrays_))+tx] = -logf(psino);
	}
    } 
}


/*------------------------------------------------------
  Sinogram normalization for 180 degrees measurements
  with zero-padding
  
  sino = -log ( (counts-dark) / (flat-dark) )

  Remark: flat, dark and counts are given as a texture
  ------------------------------------------------------*/

__global__ void kernel_sinogram_normalization_zpad(float *sino,
						   cudaTextureObject_t texFlatBefore,
						   cudaTextureObject_t texFlatAfter,
						   cudaTextureObject_t texDark,
						   cudaTextureObject_t texTomo,
						   int delta,
						   int Nrays,
						   int Nangles,
						   int zpad,
						   int offset,
						   int sliceInit)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  float pflatA, pflatB, pflatI, pdark,ptomo,psino;
  float corr_flat,corr_dark,buff_img;

  int _Nrays_ = 2*zpad + Nrays;

  if( ((tx)<_Nrays_) && ((ty)<Nangles) && ((tz)<delta) )
    {	             
      if( (tx > zpad) && (tx < Nrays + zpad) )
	{
	  /* within the original sinogram */
	  
	  pflatA = tex2D<float>(texFlatAfter,tx-zpad+0.5f,sliceInit+offset+tz+0.5f);
	  pflatB = tex2D<float>(texFlatBefore,tx-zpad+0.5f,sliceInit+offset+tz+0.5f);

	  pflatI = pflatB + (pflatB-pflatA)/((float)(Nangles-1)) * ty;

	  pdark  = tex2D<float>(texDark,tx-zpad+0.5f,sliceInit+offset+tz+0.5f);
	  ptomo  = tex3D<float>(texTomo,tx-zpad+0.5f,ty+0.5f,tz+0.5f);
	  
	  corr_flat = pflatI - pdark;
	  corr_dark = ptomo - pdark;
	  buff_img  = (corr_dark/corr_flat);
	  
	  if(corr_flat<TOL)
	    psino = 1.0f;
	  
	  if(buff_img<TOL)
	    psino = 1.0f;
	  
	  psino  =  ((float)(ptomo-pdark)/(float)(pflatI-pdark));
	  
	  sino[tz*_Nrays_*Nangles+(ty*_Nrays_)+tx] = -logf(psino);
	} 
      else
	{
	  /* outside sinogram domain: extrapolation decaying to zero */
	  
	  sino[tz*_Nrays_*Nangles+(ty*_Nrays_)+tx] = 0.0;
	}
    }
  
}


/*---------------------------------------------------------
 This function execute the following process:

 sino = -log((tomo[i][j]-dark K.[i])/flat K.[i]-dark K.[i])
 
 known as sinogram normalization (also implemented in GPU)
 --------------------------------------------------------*/

__host__ float *raft_build_sinogram_block(raft_tomo workspace)
{
  int Nangles = workspace.Nangles;
  int Nrays   = workspace.Nrays;
  int i,j,k;
  int nblock = workspace.nblock;
  int blockSize = workspace.blockSize;
  int sliceInit = workspace.sliceInit;
  int sliceFinal= workspace.sliceFinal;
  int delta = MIN(sliceFinal,workspace.ls) - workspace.li; 

  //delta is the size block - 	
  //it changes in the last loop iteration
  
  int offset = nblock*blockSize*Nrays;

  float *sinogram = (float*)malloc(delta*Nangles*Nrays*sizeof(float));

  for(k = 0; k < delta;k++)
    {
      for(i = 0; i < Nrays;i++)
	{
	  for(j = 0; j < Nangles; j++)
	    {	
	      sinogram[k*Nrays*Nangles+(j*Nrays)+i] = (float) -logf( (float) ( (float) workspace.tomo[k*Nrays*Nangles+(j*Nrays)+i]-workspace.dark[sliceInit+offset+k*Nrays+i]) / (float) (workspace.flatBefore[sliceInit+offset+k*Nrays+i]-workspace.dark[sliceInit+offset+k*Nrays+i]));
	    }
	}
    }
  
  return sinogram;
}

/*------------------------------------------------------------
  This function execute the following process:
   
  sino = -log((tomo[i][j]-dark K.[i])/flat K.[i]-dark K.[i])
  
  known as sinogram normalization implemented in GPU
  
  ---------------------------------------------------------*/

__host__ void raft_build_sinogram_block_gpu(float *blockSino,
					    float *blockTomo,
					    raft_tomo *workspace)
{
  int nblock    = workspace->nblock;
  int blockSize = workspace->blockSize;
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int Nslices   = workspace->Nslices; 
  //int i,j;
  int sliceInit = workspace->sliceInit;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
   
  //cudaStream_t stream;
  //cudaStreamCreate(&stream);
  
  ////////////////////////////////////
  ////  nblock is the block number
  int offset   	= nblock*blockSize;
  
  float time;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  
  cudaEventRecord(start);

  cudaMemcpyToArray(workspace->data.flatBeforeArray,
		    0,
		    0,
		    workspace->flatBefore,
		    Nslices*Nrays*sizeof(float),
		    cudaMemcpyHostToDevice);
  

  cudaEventRecord(stop);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Flat: Host to Device",time/1000,workspace->nblock);  

  cudaEventRecord(start);
  
  cudaMemcpyToArray(workspace->data.darkArray,
		    0,
		    0,
		    workspace->dark,
		    Nslices*Nrays*sizeof(float),
		    cudaMemcpyHostToDevice);

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
 
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Dark: Host to Device",time/1000,workspace->nblock);  

  
  cudaMemcpy3DParms myparms = {0};
  myparms.srcPtr = make_cudaPitchedPtr(blockTomo,Nrays*sizeof(float),Nrays,Nangles);
  myparms.dstArray = workspace->data.tomoArray;
  myparms.extent = make_cudaExtent(Nrays,Nangles,delta);
  myparms.kind = cudaMemcpyHostToDevice;
  cudaMemcpy3D(&myparms);

  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil(Nrays/threadsPerBlock.x)+1,(int)ceil(Nangles/threadsPerBlock.y)+1,(int)ceil(delta/threadsPerBlock.z)+1);
 
  //launch kernel
  float *blockSinoDev;
  int voxels = Nrays * Nangles * delta;
  checkCudaErrors( cudaMalloc((void**)&blockSinoDev, voxels*sizeof(float)) );
  
  cudaEventRecord(start);
  
  kernel_sinogram_normalization<<<gridBlock,threadsPerBlock>>>(blockSinoDev,
							       workspace->data.flatBeforeTexture,
							       workspace->data.flatAfterTexture,
							       workspace->data.darkTexture,
							       workspace->data.tomoTexture,
							       delta,
							       Nrays,
							       Nangles,
							       offset,
							       sliceInit);

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  checkCudaErrors(cudaPeekAtLastError());
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Execute kernel sinogram normalization",time/1000,workspace->nblock);  

  //copying data Device to Host
  
  cudaEventRecord(start);

  cudaMemcpy(blockSino, blockSinoDev, 
	     delta*Nrays*Nangles*sizeof(float),
	     cudaMemcpyDeviceToHost);

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Sinograms: Device To Host",time/1000,workspace->nblock);  
  
  //cudaStreamSynchronize(stream);
  //cudaStreamDestroy(stream); 

  cudaFree(blockSinoDev);
}

/*------------------------------------------------------------
  This function execute the following process:
   
  sino = -log((tomo[i][j]-dark K.[i])/flat K.[i]-dark K.[i])
  
  known as sinogram normalization implemented in GPU
  
  ---------------------------------------------------------*/

__host__ void raft_build_sinogram_block_gpu_zpad(float *blockSino,
						 float *blockTomo,
						 raft_tomo *workspace)
{
  int nblock    = workspace->nblock;
  int blockSize = workspace->blockSize;
  int Nangles   = workspace->measured_nangles; 
  int Nrays     = workspace->measured_nrays;
  int Nslices   = workspace->Nslices; 
  //int i,j;
  int sliceInit = workspace->sliceInit;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
  
  float time;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  
  ////////////////////////////////////
  ////  nblock is the block number

  int offset  = nblock*blockSize;
  int _Nrays_ = Nrays + 2*workspace->zpad;

  cudaEventRecord(start);
  
  cudaMemcpyToArray(workspace->data.flatBeforeArray,
		    0,
		    0,
		    workspace->flatBefore,
		    Nslices*Nrays*sizeof(float),
		    cudaMemcpyHostToDevice);
  
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Flat: Host to Device",time/1000,workspace->nblock);  
 
  
  cudaMemcpyToArray(workspace->data.flatAfterArray,
		    0,
		    0,
		    workspace->flatAfter,
		    Nslices*Nrays*sizeof(float),
		    cudaMemcpyHostToDevice);
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Flat: Host to Device",time/1000,workspace->nblock);  


  cudaMemcpyToArray(workspace->data.darkArray,
		    0,
		    0,
		    workspace->dark,
		    Nslices*Nrays*sizeof(float),
		    cudaMemcpyHostToDevice);

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Dark: Host to Device",time/1000,workspace->nblock);  

  cudaMemcpy3DParms myparms = {0};
  myparms.srcPtr = make_cudaPitchedPtr(blockTomo,Nrays*sizeof(float),Nrays,Nangles);
  myparms.dstArray = workspace->data.tomoArray;
  myparms.extent = make_cudaExtent(Nrays,Nangles,delta);
  myparms.kind = cudaMemcpyHostToDevice;
  cudaMemcpy3D(&myparms);
 
  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil(_Nrays_/threadsPerBlock.x)+1,(int)ceil(Nangles/threadsPerBlock.y)+1,(int)ceil(delta/threadsPerBlock.z)+1);
 
  //launch kernel


  float *blockSinoDev;

  int voxels = _Nrays_ * Nangles * delta;
  checkCudaErrors( cudaMalloc((void**)&blockSinoDev, voxels*sizeof(float)) );

  cudaEventRecord(start);
  
  kernel_sinogram_normalization_zpad<<<gridBlock,threadsPerBlock>>>(blockSinoDev,
								    workspace->data.flatBeforeTexture,
								    workspace->data.flatAfterTexture,
								    workspace->data.darkTexture,
								    workspace->data.tomoTexture,
								    delta,
								    Nrays,
								    Nangles,
								    workspace->zpad,
								    offset,
								    sliceInit);
  
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Execute kernel",time/1000,workspace->nblock);  

  checkCudaErrors(cudaPeekAtLastError());

  //copying data Device to Host
  
  cudaEventRecord(start);

  cudaMemcpy(blockSino, 
	     blockSinoDev, 
	     delta*(_Nrays_)*Nangles*sizeof(float),
	     cudaMemcpyDeviceToHost);


  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  if(workspace->printTimingK)	
     fprintf(stderr,"\n\t%lf\t|%d-->Copy Sinograms: Device To Host",time/1000,workspace->nblock);  
  
  cudaFree(blockSinoDev);

  /////////////////////////////////////////////////
  /////  Update Nrays within workspace
  /////
  workspace->Nrays   = _Nrays_; 
}

/*------------------------------------------------------------
  This function execute the following process:
   
  sino = -log((tomo[i][j]-dark K.[i])/flat K.[i]-dark K.[i])
  
  known as sinogram normalization implemented in GPU, 
  for 360 degrees tomographic measurements.
  
  ---------------------------------------------------------*/

__host__ void raft_build_sinogram_block_360_gpu(float *blockSino,
						float *blockTomo,
						raft_tomo *workspace)
{
  int nblock    = workspace->nblock;
  int blockSize = workspace->blockSize;
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int Nslices   = workspace->Nslices; 
  //int i,j;
  int sliceInit = workspace->sliceInit;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;

  int _Nrays_   = 2*workspace->axis; 
  int _Nangles_ = Nangles/2;
   
  struct timespec TimeStart, TimeEnd;
  
  int offset   	= nblock*blockSize;
  
  clock_gettime(CLOCK, &TimeStart);
  
  cudaMemcpyToArray(workspace->data.flatBeforeArray,
		    0,
		    0,
		    workspace->flatBefore,
		    Nslices*Nrays*sizeof(float),
		    cudaMemcpyHostToDevice);
  
  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Flat: Host to Device",TIME(TimeEnd,TimeStart),workspace->nblock);  
 	 
  clock_gettime(CLOCK, &TimeStart);

  cudaMemcpyToArray(workspace->data.darkArray,
		    0,
		    0,
		    workspace->dark,
		    Nslices*Nrays*sizeof(float),
		    cudaMemcpyHostToDevice);

  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
     fprintf(stderr,"\n\t%lf\t|%d-->Copy Dark: Host to Device",TIME(TimeEnd,TimeStart),workspace->nblock);  

  
  cudaMemcpy3DParms myparms = {0};
  myparms.srcPtr = make_cudaPitchedPtr(blockTomo,Nrays*sizeof(float),Nrays,Nangles);
  myparms.dstArray = workspace->data.tomoArray;
  myparms.extent = make_cudaExtent(Nrays,Nangles,delta);
  myparms.kind = cudaMemcpyHostToDevice;
  cudaMemcpy3D(&myparms);

  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil((_Nrays_)/threadsPerBlock.x)+1,
		 (int)ceil((_Nangles_)/threadsPerBlock.y)+1,
		 (int)ceil(delta/threadsPerBlock.z)+1);
 
  //launch kernel
  clock_gettime(CLOCK, &TimeStart);

  float *blockSinoDev;
  int voxels = Nrays * Nangles * delta;
  checkCudaErrors( cudaMalloc((void**)&blockSinoDev, voxels*sizeof(float)) );

  kernel_sinogram_normalization_360<<<gridBlock,threadsPerBlock>>>(blockSinoDev,
								   workspace->data.flatBeforeTexture,
								   workspace->data.flatAfterTexture,
								   workspace->data.darkTexture,
								   workspace->data.tomoTexture,
								   delta,
								   Nrays,
								   Nangles,
								   workspace->axis,	
								   offset,
								   sliceInit);
  
  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
     fprintf(stderr,"\n\t%lf\t|%d-->Execute kernel",TIME(TimeEnd,TimeStart),workspace->nblock);  

  //copying data Device to Host
  
  clock_gettime(CLOCK, &TimeStart);

  cudaMemcpy(blockSino, 
	     blockSinoDev, 
	     delta*(_Nrays_)*(_Nangles_)*sizeof(float),
	     cudaMemcpyDeviceToHost);

  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
     fprintf(stderr,"\n\t%lf\t|%d-->Copy BlockSinogram: Device To Host",TIME(TimeEnd,TimeStart),workspace->nblock);  
  
  cudaFree( blockSinoDev );

  /////////////////////////////////////////////////
  /////  Update Nrays and Nangles within workspace
  /////
  
  workspace->Nrays   = _Nrays_;
  workspace->Nangles = _Nangles_; 
}



__host__ void raft_allocate_sinogram_normalization_gpu_memory(raft_tomo* workspace) 
{
  int Nrays = workspace->Nrays;
  int Nangles = workspace->Nangles;
  
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;

  if(!workspace->fromSinogram)
    checkCudaErrors(cudaMalloc((void**)&workspace->data.radon, 
			       Nrays*Nangles*delta*sizeof(float)));

  checkCudaErrors(cudaMalloc3DArray(&workspace->data.tomoArray, 
				    &workspace->data.channelDesc_tomo,
				    make_cudaExtent(Nrays, Nangles, delta)));
  
  raft_create_texture_object_for_array(&workspace->data.tomoTexture, workspace->data.tomoArray);
}



__host__ void raft_deallocate_sinogram_normalization_gpu_memory(raft_tomo* workspace) 
{
  if(!workspace->fromSinogram)
    cudaFree(workspace->data.radon);
  cudaFreeArray(workspace->data.tomoArray);
  cudaDestroyTextureObject(workspace->data.tomoTexture);  
}


/////////////////////////////////////////////////////////////////
//
// Radon transform 
//

__global__ void kernel_radon_ss_block(float *output,
				      cudaTextureObject_t texImg,
				      int sizeImage,
				      int Nrays,
				      int Nangles,
				      float dxy,
				      float dt,
				      float dth,
				      float tmin,
				      float tmax,
				      float xymin,
				      float delta)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x;
  int ty = blockIdx.y * blockDim.y + threadIdx.y;
  int tz = blockIdx.z * blockDim.z + threadIdx.z;

  float theta, cos, sin, tan, cond, t, x, y, sum;
  int Y, X;

  if ( (tx < Nrays) && (ty < Nangles) && (tz < delta) )
    {
      cond = sqrtf(2.0)/2.0;
      
      theta = ty*dth;

      __sincosf(theta, &sin, &cos);
            
      if( sin < cond )
	{
	  tan = sin/cos;
	  sum = 0;
	  
	  for(Y = 0; Y < sizeImage; Y++)
	    {
	      y = xymin + Y * dxy;
	      t = tmin + tx * dt;
	      
	      x = (t/cos - y*tan); 
	      X = (float)((x - xymin)/dxy);

	      //sum += tex3D<float>(texImg, X + 0.5f, Y + 0.5f, tz + 0.5f);
	      sum += tex2D<float>(texImg, X + 0.5f, Y + 0.5f + (tz*Nangles));
	    }
	  
	  output[ tz*Nrays*Nangles + ty*Nrays + tx] = dxy*sum/fabsf(cos);
	}
      else
	{
	  tan = sin/cos;
	  sum = 0;

	  for(X = 0; X < sizeImage; X++)
	    {
	      x = xymin + X * dxy;
	      
	      t = tmin + tx * dt;
	      y = (t/sin - x/tan);
	      Y = (float)((y - xymin)/dxy);
	      
	      //sum += tex3D<float>(texImg, X + 0.5f, Y + 0.5f, tz + 0.5f);
	      sum += tex2D<float>(texImg, X + 0.5f, Y + 0.5f + (tz*Nangles));
	    }

	  output[ tz*Nrays*Nangles + ty*Nrays + tx] = dxy*sum/fabsf(sin);
	}
    }
}				      


void raft_radon_slantstack_gpu(float *blockSino,
			       float *blockImg,
			       raft_tomo *workspace)
{
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int sizeImage = workspace->sizeImage;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
   
  struct timespec TimeStart, TimeEnd;

  //cudaStream_t stream;
  //cudaStreamCreate(&stream);

  // cudaMemcpy3DParms myparms = {0};
  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);

  ///////////////
  /// temp
  
  workspace->backp.channelDesc = cudaCreateChannelDesc(32,0,0,0,cudaChannelFormatKindFloat);
  
  /*
  checkCudaErrors(cudaMalloc3DArray(&workspace->backp.dst, 
				    &workspace->backp.channelDesc,
				    make_cudaExtent(sizeImage, sizeImage, delta)));
  */

  checkCudaErrors(cudaMallocArray(&workspace->backp.dst, &workspace->backp.channelDesc,
				  sizeImage,sizeImage*delta,cudaArrayDefault));
  
  

  raft_create_texture_object_for_array(&workspace->backp.TextureObject, workspace->backp.dst);
  
  /////////////////////////////////////
  /////////////////////////////////////
  //////// execute backprojection

  float dt, dth, dxy, tmin, tmax, xymin;

  sizeImage = workspace->sizeImage;
  
  dt   = workspace->dt;
  dth  = workspace->dth;
  dxy  = workspace->dxy;
  tmin = workspace->tmin;
  tmax = workspace->tmax;
  xymin= workspace->xymin;

  /*
  myparms.srcPtr = make_cudaPitchedPtr(blockImg, sizeImage*sizeof(float), sizeImage, sizeImage);
  myparms.dstArray = workspace->backp.dst;
  myparms.extent = make_cudaExtent(sizeImage,sizeImage,delta);
  myparms.kind = cudaMemcpyHostToDevice;
  cudaMemcpy3D(&myparms); //, stream);
  */
 
  cudaMemcpyToArray(workspace->backp.dst,
		    0,
		    0,
		    blockImg,
		    sizeImage*sizeImage*delta*sizeof(float),
		    cudaMemcpyHostToDevice);

  dim3 gridBlock((int)ceil(Nrays/threadsPerBlock.x)+1,
		 (int)ceil(Nangles/threadsPerBlock.y)+1,
		 (int)ceil(delta/threadsPerBlock.z)+1);
 
  //launch kernel
  clock_gettime(CLOCK, &TimeStart);
  
  kernel_radon_ss_block<<<gridBlock, threadsPerBlock>>>(workspace->data.radon,
							workspace->backp.TextureObject,
							sizeImage,
							Nrays,
							Nangles,
							dxy,
							dt,
							dth,
							tmin,
							tmax,
							xymin,
							delta);
  
  checkCudaErrors(cudaPeekAtLastError());
  
  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTiming)	
    fprintf(stderr,"\n\t%lf\tExecute Radon kernel", TIME(TimeEnd,TimeStart));  

  //copying data Device to Host
  clock_gettime(CLOCK, &TimeStart);

  cudaMemcpy(blockSino, 
	     workspace->data.radon, 
	     delta*Nrays*Nangles*sizeof(float),
	     cudaMemcpyDeviceToHost); //, stream);

  
  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTiming)	
     fprintf(stderr,"\n\t%lf\t|%d-->Copy Radon block: Device to host", TIME(TimeEnd,TimeStart),workspace->nblock);  

  //cudaStreamSynchronize(stream);
  //cudaStreamDestroy(stream);

  cudaFreeArray(workspace->backp.dst);
  cudaDestroyTextureObject(workspace->backp.TextureObject);
}

