#include <stdlib.h>
#include <math.h>
#include <hdf5.h>

#include "../common/raft_types.h"
#include "raft_processblock.h"
#include "raft_backprojection_bst.h"
#include "raft_backprojection_slantstack.h"
#include "raft_sinogram.h"
#include "raft_filters.h"

extern "C" {
#include <time.h>
}

__constant__ float TOL = 0.0001f;
__constant__ float TOL2 = 0.0000001f;


__host__ void raft_process_block_gpu(raft_tomo *workspace)
{
  int nblock    = workspace->nblock;
  int blockSize = workspace->blockSize;
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int sliceInit = workspace->sliceInit;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
   
  struct timespec TimeStart, TimeEnd;

  cudaStream_t stream;
  cudaStreamCreate(&stream);

  ////////////////////////////////////
  ////  nblock is the block number

  int offset   	= nblock*blockSize;

  clock_gettime(CLOCK, &TimeStart);
  
  cudaMemcpy3DParms myparms = {0};
  
  myparms.srcPtr = make_cudaPitchedPtr(workspace->tomo,Nrays*sizeof(float),Nrays,Nangles);
  myparms.dstArray = workspace->data.tomoArray;
  myparms.extent = make_cudaExtent(Nrays,Nangles,delta);
  myparms.kind = cudaMemcpyHostToDevice;
  cudaMemcpy3DAsync(&myparms,stream);
  
  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil(Nrays/threadsPerBlock.x)+1,
	         (int)ceil(Nangles/threadsPerBlock.y)+1,
		 (int)ceil(delta/threadsPerBlock.z)+1);
 
  //launch kernel
  clock_gettime(CLOCK, &TimeStart);

  kernel_sinogram_normalization<<<gridBlock, threadsPerBlock, 0, stream>>>(workspace->data.radon,
									   workspace->data.flatBeforeTexture,
									   workspace->data.flatAfterTexture,
									   workspace->data.darkTexture,
									   workspace->data.tomoTexture,
									   delta,
									   Nrays,
									   Nangles,
									   offset,
									   sliceInit);


  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Execute kernel: sinogram normalization",TIME(TimeEnd,TimeStart),workspace->nblock);  

  ///////////////////////////////////
  //////// execute lowpass filtering
  
  raft_lowpassino_gpu_dev2dev(workspace->data.radon, workspace);

  //////////////////////////////////////////////////
  //////// execute backprojection (with SlantStack)

  raft_backprojection_slantstack_gpu_dev2host(workspace->blockRecon,
					      workspace->data.radon,
					      workspace);
}


__host__ void raft_process_block_gpu_fromSinogram(raft_tomo *workspace)
{
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
  
  float *dsinogram;
  
  checkCudaErrors( cudaMalloc((void**)&dsinogram, Nrays*Nangles*delta*sizeof(float)) );

  cudaMemcpy(dsinogram, workspace->tomo, Nrays*Nangles*delta*sizeof(float), cudaMemcpyHostToDevice);

  ///////////////////////////////////
  //////// execute lowpass filtering
  
  raft_lowpassino_gpu_dev2dev(dsinogram, workspace);

  //////////////////////////////////////////////////
  //////// execute backprojection (with SlantStack)

  raft_backprojection_slantstack_gpu_dev2host(workspace->blockRecon, dsinogram, workspace);

  cudaFree(dsinogram);
}

////////////////////////////////////////////////////

__host__ void raft_process_block_gpu_bst(raft_tomo *workspace)
{
  int nblock    = workspace->nblock;
  int blockSize = workspace->blockSize;
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int sliceInit = workspace->sliceInit;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
   
  struct timespec TimeStart, TimeEnd;

  cudaStream_t stream;
  cudaStreamCreate(&stream);

  ////////////////////////////////////
  ////  nblock is the block number

  int offset   	= nblock*blockSize;

  clock_gettime(CLOCK, &TimeStart);
  
  cudaMemcpy3DParms myparms = {0};
  
  myparms.srcPtr = make_cudaPitchedPtr(workspace->tomo,Nrays*sizeof(float),Nrays,Nangles);
  myparms.dstArray = workspace->data.tomoArray;
  myparms.extent = make_cudaExtent(Nrays,Nangles,delta);
  myparms.kind = cudaMemcpyHostToDevice;
  cudaMemcpy3DAsync(&myparms,stream);
  
  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil(Nrays/threadsPerBlock.x)+1,
	         (int)ceil(Nangles/threadsPerBlock.y)+1,
		 (int)ceil(delta/threadsPerBlock.z)+1);
 
  //launch kernel
  clock_gettime(CLOCK, &TimeStart);

  kernel_sinogram_normalization<<<gridBlock, threadsPerBlock, 0, stream>>>(workspace->data.radon,
									   workspace->data.flatBeforeTexture,
									   workspace->data.flatAfterTexture,
									   workspace->data.darkTexture,
									   workspace->data.tomoTexture,
									   delta,
									   Nrays,
									   Nangles,
									   offset,
									   sliceInit);


  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Execute kernel: sinogram normalization",TIME(TimeEnd,TimeStart),workspace->nblock);  

  ///////////////////////////////////
  //////// execute lowpass filtering
  
  raft_lowpassino_gpu_dev2dev(workspace->data.radon, workspace);

  //////////////////////////////////////////
  //////// execute backprojection (with BST)
  
  raft_backprojection_bst_gpu_dev2host(workspace->blockRecon, workspace->data.radon, workspace);
}


__host__ void raft_process_block_gpu_bst_fromSinogram(raft_tomo *workspace)
{
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
  
  float *dsinogram;
  
  checkCudaErrors( cudaMalloc((void**)&dsinogram, Nrays*Nangles*delta*sizeof(float)) );

  cudaMemcpy(dsinogram, workspace->tomo, Nrays*Nangles*delta*sizeof(float), cudaMemcpyHostToDevice);

  ///////////////////////////////////
  //////// execute lowpass filtering
  
  raft_lowpassino_gpu_dev2dev(dsinogram, workspace);

  /////////////////////////////////////////////
  //////// execute backprojection (with BST)

  raft_backprojection_bst_gpu_dev2host(workspace->blockRecon, dsinogram, workspace);

  cudaFree(dsinogram);
}

