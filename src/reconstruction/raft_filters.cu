#include <stdlib.h>
#include <math.h>
#include <hdf5.h>

#include "../common/raft_types.h"
#include "raft_filters.h"

extern "C" {
#include <time.h>
}

__constant__ float TOL = 0.0001f;
__constant__ float TOL2 = 0.0000001f;


__global__ void kernel_set_fourier_sino(cufftComplex *block, 
					float *array, 
					int Nangles, 
					int Nrays,
					int blockSize)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  int voxel;
  
  if( (tx< Nrays) && (ty<Nangles) && (tz < blockSize) )
    {
      voxel = tz*Nrays*Nangles+(ty*Nrays)+tx;
      
      block[ voxel ].x = array[ voxel ];
      block[ voxel ].y = 0;
    }
}


__global__ void kernel_get_real(float *array,
				cufftComplex *block, 
				int Nangles, 
				int Nrays,
				int blockSize)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  int voxel;

  if( (tx< Nrays) && (ty<Nangles) && (tz < blockSize) )
    {
      voxel = tz*Nrays*Nangles+(ty*Nrays)+tx;

      array[ voxel ] = block[voxel].x;
    }
}

/*------------------------------------------------------
  Kernel: Low-pass filter for sinograms
  ------------------------------------------------------*/

__global__ void kernel_lowpassino_gpu(cufftComplex *block, 
				      float *dcenterblock,
				      float regularization,				  
				      int Nrays,
				      int Nangles,
				      int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x;
  int ty = blockIdx.y * blockDim.y + threadIdx.y;
  int tz = blockIdx.z * blockDim.z + threadIdx.z;
  int TX;  //fourier shift

  float dt = 2.0/((float) Nrays); 
  float sigmaMax = 1/(2*dt);
  float delta_sigma = 2*sigmaMax/((float) Nrays); 
  float RegFBP;

  if ( (tx < Nrays) && (ty < Nangles) && (tz < blockSize) )
  {
    TX = ((tx + (Nrays/2)) % Nrays);
    float sigma = -sigmaMax + tx * delta_sigma;
    int offset = tz*Nangles*Nrays + ty*Nrays + TX;
        
    /*get real and complex values*/
    float a = block[offset].x;
    float b = block[offset].y; 
    float c, d, cos, sin, beta;
    
    beta = dcenterblock[tz];
    
    __sincosf( 2*PI*beta*sigma, &sin, &cos );
    
    RegFBP = 1.0 / (1 + (4*(regularization)*SQUARE(PI*sigma)) );

    c = fabsf(sigma) * RegFBP * cos;
    d = fabsf(sigma) * RegFBP * sin;
        
    /*set real and complex data */
    block[offset].x = ( a*c - b*d ) / ((float) Nrays);
    block[offset].y = ( a*d + b*c ) / ((float) Nrays);    

    // Division by Nrays: Fourier defition from CUFFT
    //                    without 1/N
  }
}

/*------------------------------------------------------------
  This function execute the following process:
   
  sino = convolution( sino, filter )

  with fourier(filter) = abs(sigma)

  Low-pass filtering for sinogram data.  
  ---------------------------------------------------------*/

__host__ void raft_lowpassino_gpu(float *blockSino,
				  raft_tomo *workspace)
{
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
  float *centerblockDev;
 
  struct timespec TimeStart, TimeEnd;

  int Nrays, Nangles;
  if(workspace->hybrid==1)
    {
      Nrays   = 2*workspace->axis;
      Nangles = (int) workspace->measured_nangles/2;	
    }
  else
    {
      Nrays   = workspace->measured_nrays + 2*workspace->zpad;
      Nangles = workspace->measured_nangles;
  }

  float *blockSinoDev;
  int voxels = Nrays * Nangles * delta;
  checkCudaErrors( cudaMalloc((void**)&blockSinoDev, voxels*sizeof(float)) );
  checkCudaErrors( cudaMalloc((void **)&centerblockDev, sizeof(float)*delta) );
  
  //cudaStream_t stream;
  //cudaStreamCreate(&stream);


  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil((Nrays)/threadsPerBlock.x)+1,
		 (int)ceil((Nangles)/threadsPerBlock.y)+1,
		 (int)ceil(delta/threadsPerBlock.z)+1);
  
  clock_gettime(CLOCK, &TimeStart);

  //--- Set imaginary part to zero: C2C FFT
  
  cudaMemcpy(blockSinoDev, blockSino, delta*(Nrays)*(Nangles)*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(centerblockDev, workspace->centerblock, delta*sizeof(float), cudaMemcpyHostToDevice);
  
  kernel_set_fourier_sino<<<gridBlock, threadsPerBlock>>>(workspace->fourier.SinoCU, 
							  blockSinoDev, 
							  Nangles, 
							  Nrays,
							  delta);
  
  checkCudaErrors(cudaPeekAtLastError());
  
  //--- FFT: block of sinograms
  
  if( cufftExecC2C(workspace->fourier.SinoCUplan, 
		   workspace->fourier.SinoCU, 
		   workspace->fourier.SinoCU, CUFFT_FORWARD) != CUFFT_SUCCESS)
    {
      fprintf(stderr,"RafT Error: CUFFT error (Low-pass). ExecR2C forward failed\n ");
      return;
    }
  
  //--- Multiplication in the frequency domain +
  //--- shift from centersino + regularization factor
  
  kernel_lowpassino_gpu<<<gridBlock, threadsPerBlock>>>(workspace->fourier.SinoCU, 
							centerblockDev,
							workspace->reg,
							Nrays,Nangles,delta);

  checkCudaErrors(cudaPeekAtLastError());

  //--- IFFT: block of sinograms

  if( cufftExecC2C(workspace->fourier.SinoCUplan, 
		   workspace->fourier.SinoCU, 
		   workspace->fourier.SinoCU, CUFFT_INVERSE ) != CUFFT_SUCCESS)
  {
    fprintf(stderr,"RafT Error: CUFFT error (Low-pass). ExecR2C forward failed\n ");
    return;
  }
 
  kernel_get_real<<<gridBlock, threadsPerBlock>>>(blockSinoDev,
						  workspace->fourier.SinoCU,
						  Nangles, 
						  Nrays,
						  delta);

  checkCudaErrors(cudaPeekAtLastError());
  
  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Execute Low-pass kernel",TIME(TimeEnd,TimeStart),workspace->nblock);  
  
  //copying data Device to Host
  
  clock_gettime(CLOCK, &TimeStart);
    
  cudaMemcpy(blockSino, 
	     blockSinoDev, 
	     delta*(Nrays)*(Nangles)*sizeof(float),
	     cudaMemcpyDeviceToHost);
  
  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Filtered block: Device to host",TIME(TimeEnd,TimeStart),workspace->nblock);  
  
  cudaFree(blockSinoDev);
  cudaFree(centerblockDev);
  
  //cudaStreamSynchronize(stream);
  //cudaStreamDestroy(stream);
}


__host__ void raft_allocate_sinogram_lowpass_gpu_memory(raft_tomo* workspace) 
{
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
  
  int Nrays, Nangles, voxels;
  
  if(workspace->hybrid==1)
    {
      Nrays   = 2*workspace->axis;
      Nangles = (int) workspace->measured_nangles/2;	
    }
  else
    {
      Nrays   = workspace->measured_nrays + 2*workspace->zpad;
      Nangles = workspace->measured_nangles;
  }
  
  voxels = delta * Nrays * Nangles;

  checkCudaErrors(cudaMalloc((void**)&workspace->fourier.SinoCU, sizeof(cufftComplex)*voxels ));
  
  /*Complex_2_Complex Plan*/
  int rank = 1;				//--- 1D FFTS
  int n[] = { Nrays };		        //--- Size of the Fourier transform
  int istride = 1;
  int ostride = 1;			//--- Distance between two successive input/output elements
  int idist = Nrays;
  int odist = Nrays;			//--- Distance between Batches
  //int inembed[] = {0};		//--- Input size with pitch (ignored for 1D transforms)
  //int onembed[] = {0};		//--- Output size with pitch (ignored for 1D transforms)
  int batch = (Nangles)*delta;
  
  if( cufftPlanMany(&workspace->fourier.SinoCUplan, rank, n,
		    NULL, istride, idist, 
		    NULL, ostride, odist, CUFFT_C2C, batch) != CUFFT_SUCCESS )
    {
      fprintf(stderr,"RafT: CUFFT error. Plan creation failed\n"); 
      return;
    }
}


__host__ void raft_deallocate_sinogram_lowpass_gpu_memory(raft_tomo* workspace) 
{
  cudaFree( workspace->fourier.SinoCU );
  cufftDestroy( workspace->fourier.SinoCUplan );
}


/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@                        @@@@@@@@@@@@@@@@@@@@@ 
  @@@@@@@@@@@@@@@@@@@@@@ from device to device  @@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@                        @@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

__host__ void raft_lowpassino_gpu_dev2dev(float *blockSinoDev,
					  raft_tomo *workspace)
{
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
  float *centerblockDev;
 
  struct timespec TimeStart, TimeEnd;

  int Nrays, Nangles;
  if(workspace->hybrid==1)
    {
      Nrays   = 2*workspace->axis;
      Nangles = (int) workspace->measured_nangles/2;	
    }
  else
    {
      Nrays   = workspace->measured_nrays + 2*workspace->zpad;
      Nangles = workspace->measured_nangles;
  }

  checkCudaErrors( cudaMalloc((void **)&centerblockDev, sizeof(float)*delta) );
  
  //cudaStream_t stream;
  //cudaStreamCreate(&stream);

  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil((Nrays)/threadsPerBlock.x)+1,
		 (int)ceil((Nangles)/threadsPerBlock.y)+1,
		 (int)ceil(delta/threadsPerBlock.z)+1);
  
  clock_gettime(CLOCK, &TimeStart);

  //--- Set imaginary part to zero: C2C FFT
  
  cudaMemcpy(centerblockDev, workspace->centerblock, delta*sizeof(float), cudaMemcpyHostToDevice);
  
  kernel_set_fourier_sino<<<gridBlock, threadsPerBlock>>>(workspace->fourier.SinoCU, 
							  blockSinoDev, 
							  Nangles, 
							  Nrays,
							  delta);
  
  checkCudaErrors(cudaPeekAtLastError());
  
  //--- FFT: block of sinograms
  
  if( cufftExecC2C(workspace->fourier.SinoCUplan, 
		   workspace->fourier.SinoCU, 
		   workspace->fourier.SinoCU, CUFFT_FORWARD) != CUFFT_SUCCESS)
  {
    fprintf(stderr,"RafT Error: CUFFT error (Low-pass). ExecR2C forward failed\n ");
    return;
  }
  
  //--- Multiplication in the frequency domain +
  //--- shift from centersino + regularization factor
  
  kernel_lowpassino_gpu<<<gridBlock, threadsPerBlock>>>(workspace->fourier.SinoCU, 
							centerblockDev,
							workspace->reg,
							Nrays,Nangles,delta);

  checkCudaErrors(cudaPeekAtLastError());

  //--- IFFT: block of sinograms

  if( cufftExecC2C(workspace->fourier.SinoCUplan, 
		   workspace->fourier.SinoCU, 
		   workspace->fourier.SinoCU, CUFFT_INVERSE ) != CUFFT_SUCCESS)
  {
    fprintf(stderr,"RafT Error: CUFFT error (Low-pass). ExecR2C forward failed\n ");
    return;
  }
 
  kernel_get_real<<<gridBlock, threadsPerBlock>>>(blockSinoDev,
						  workspace->fourier.SinoCU,
						  Nangles, 
						  Nrays,
						  delta);

  checkCudaErrors(cudaPeekAtLastError());
  
  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Execute Low-pass kernel",TIME(TimeEnd,TimeStart),workspace->nblock);  
  
  cudaFree(centerblockDev);
  
  //cudaStreamSynchronize(stream);
  //cudaStreamDestroy(stream);
}
