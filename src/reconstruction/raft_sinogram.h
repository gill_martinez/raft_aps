#ifndef RAFT_SINOGRAM_H
#define RAFT_SINOGRAM_H

#include "../common/raft_types.h"


__global__ void kernel_sinogram_normalization(float *sino,
					      cudaTextureObject_t texFlatBefore,
					      cudaTextureObject_t texFlatAfter,
					      cudaTextureObject_t texDark,
					      cudaTextureObject_t texTomo,
					      int delta,
					      int Nrays,
					      int Nangles,
					      int offset,
					      int sliceInit);

__host__ void raft_radon_slantstack_gpu(float *blockSino, 
					float *blockImg,
					raft_tomo *workspace);


__host__ float *raft_build_sinogram_block(raft_tomo workspace);

__host__ void raft_build_sinogram_block_gpu(float *blockSino,
					    float *blockTomo,
					    raft_tomo *workspace);


__host__ void raft_build_sinogram_block_360_gpu(float *blockSino,
						float *blockTomo,
						raft_tomo *workspace);


__host__ void raft_build_sinogram_block_gpu_zpad(float *blockSino,
						 float *blockTomo,
						 raft_tomo *workspace);


__host__ void raft_allocate_sinogram_normalization_gpu_memory(raft_tomo* workspace);
__host__ void raft_deallocate_sinogram_normalization_gpu_memory(raft_tomo* workspace);


#endif // RAFT_SINOGRAM_H
