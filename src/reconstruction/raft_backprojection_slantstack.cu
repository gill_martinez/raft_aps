#include <stdlib.h>
#include <math.h>
#include <hdf5.h>

#include "../common/raft_types.h"
#include "../common/raft_memory.h"

#include "raft_processblock.h"


extern "C" {
#include <time.h>
}

__constant__ float TOL = 0.0001f;
__constant__ float TOL2 = 0.0000001f;

/*-----------------------------------
  Kernel for Backprojection operator
  
  Slantstack (SS) method.
  ----------------------------------*/

__global__ void kernel_bp_ss_block(float *image,
				   cudaTextureObject_t texBlock,
				   int wdI,
				   int nrays,
				   int nangles,
				   float dxy,
				   float dt,
				   float dth,
				   float tmin,
				   float tmax,
				   float xymin,
				   int blockSize)
{
  int i, j, k, T, z;
  float t, cs, x, y, cosk, sink;
  
  i = (blockDim.x * blockIdx.x + threadIdx.x);
  j = (blockDim.y * blockIdx.y + threadIdx.y);
  z = (blockDim.z * blockIdx.z + threadIdx.z);
  
  if ( ((i)<wdI) && ((j) < wdI) && ((z)<blockSize)  ){
    
    cs = 0;
    
    x = xymin + i * dxy;
    y = xymin + j * dxy;
    
    for(k=0; k < (nangles); k++)
      {
	__sincosf(k * dth, &sink, &cosk);
	
	t = x * cosk + y * sink;
	
	if (t < tmax && t > tmin)
	  {
	    T = (float)((t - tmin)/dt);	     
	    
	    cs = cs + tex2D<float>(texBlock, T + 0.5f, k + 0.5f + (z*nangles));
	  }
      }
    
    image[z*wdI*wdI+(j)*wdI + i]  = (cs*dth); 
  }
}


__host__ void raft_allocate_backprojection_gpu_memory(raft_tomo* workspace) 
{
  int imageSize = workspace->sizeImage;
  int blockSize = workspace->blockSize;
  int voxelsPerBlock = blockSize * imageSize * imageSize;
  
  int Nrays = workspace->Nrays;
  int Nangles = workspace->Nangles;
  
  checkCudaErrors( cudaMalloc((void**)&workspace->backp.backp,
			      voxelsPerBlock * sizeof(float)) );
  
  checkCudaErrors(cudaMallocArray(&workspace->data.dst, &workspace->data.channelDesc,
				  Nrays,Nangles*blockSize,cudaArrayDefault));
  
  raft_create_texture_object_for_array(&workspace->data.textureObject,
				       workspace->data.dst);
}


__host__ void raft_deallocate_backprojection_gpu_memory(raft_tomo* workspace) 
{
  cudaFree(workspace->backp.backp);
  cudaFreeArray(workspace->data.dst);
  cudaDestroyTextureObject(workspace->data.textureObject);
}



/*--------------------------------------------
  Apply slant-stack backprojection 
  -------------------------------------------*/

__host__ void raft_backprojection_slantstack_gpu(float *blockBack,
						 float *blockSino,
						 raft_tomo *workspace)
{
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;

  float time;
  float memoryTime;
  float totalMemoryTime = 0.0;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  
  //cudaStream_t stream;
  //cudaStreamCreate(&stream);
  
  //cudaMemcpy3DParms myparms = {0};
  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  
  /////////////////////////////////////
  /////////////////////////////////////
  //////// execute backprojection
  
  float dt, dth, dxy, tmin, tmax, xymin;
  int sizeImage;
  
  sizeImage = workspace->sizeImage;
  
  dt   = workspace->dt;
  dth  = workspace->dth;
  dxy  = workspace->dxy;
  tmin = workspace->tmin;
  tmax = workspace->tmax;
  xymin= workspace->xymin;
  
  /*
  myparms.srcPtr = make_cudaPitchedPtr(blockSino, Nrays*sizeof(float), Nrays, Nangles);
  myparms.dstArray = workspace->data.dst;
  myparms.extent = make_cudaExtent(Nrays,Nangles,delta);
  myparms.kind = cudaMemcpyHostToDevice;
  cudaMemcpy3D(&myparms); //, stream);
  */

  cudaEventRecord(start);
  
  cudaMemcpyToArray(workspace->data.dst,
		    0,
		    0,
		    blockSino,
		    Nrays*Nangles*delta*sizeof(float),
		    cudaMemcpyHostToDevice);

  checkCudaErrors(cudaPeekAtLastError());

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;
  
  
  dim3 gridBlockBack((int)ceil(sizeImage/threadsPerBlock.x)+1,
		     (int)ceil(sizeImage/threadsPerBlock.y)+1,
		     (int)ceil(delta/threadsPerBlock.z)+1);

  //launch kernel
  
 

  cudaEventRecord(start);

  kernel_bp_ss_block<<<gridBlockBack, threadsPerBlock>>>(workspace->backp.backp,
							 workspace->data.textureObject,
							 sizeImage,
							 Nrays,
							 Nangles,
							 dxy,
							 dt,
							 dth,
							 tmin,
							 tmax,
							 xymin,
							 delta);
  
  checkCudaErrors(cudaPeekAtLastError());

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  if(workspace->printTimingK){
    fprintf(stderr,"\n\t%lf\t|%d-->Execute Backprojection kernel",time/1000,workspace->nblock);  
  }
  
  //copying data Device to Host
  cudaEventRecord(start);
 
  cudaMemcpy(blockBack, 
	     workspace->backp.backp, 
	     delta*sizeImage*sizeImage*sizeof(float),
	     cudaMemcpyDeviceToHost); //, stream);


  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;

  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Backprojections: Device to host",time/1000,workspace->nblock);  

  if(workspace->printTiming){	
    //fprintf(stderr,"\n\t%lf\t|%d-->SlantStack Kernel Time",time/1000,workspace->nblock);
    //fprintf(stderr,"\n\t%lf\t|%d-->SlantStack Memory Time",totalMemoryTime/1000,workspace->nblock);
    fprintf(stderr,"\n\t%lf\t|%d-->SlantStack Total Time -- process[%d]",((time + totalMemoryTime) / 1000), workspace->nblock,workspace->processId);
    
  }
  
  cudaEventDestroy(start);
  cudaEventDestroy(stop);
  
  //cudaStreamSynchronize(stream);
  //cudaStreamDestroy(stream);
}


/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@                        @@@@@@@@@@@@@@@@@@@@@ 
  @@@@@@@@@@@@@@@@@@@@@@ from device to host    @@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@                        @@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

__host__ void raft_backprojection_slantstack_gpu_dev2host(float *blockBack,
							  float *blockSinoDev,
							  raft_tomo *workspace)
{
  int Nangles   = workspace->Nangles; 
  int Nrays     = workspace->Nrays;
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;

  float time;
  float memoryTime;
  float totalMemoryTime = 0.0;
  cudaEvent_t start,stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  //cudaStream_t stream;
  //cudaStreamCreate(&stream);
  
  //cudaMemcpy3DParms myparms = {0};
  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  
  /////////////////////////////////////
  /////////////////////////////////////
  //////// execute backprojection
  
  float dt, dth, dxy, tmin, tmax, xymin;
  int sizeImage;
  
  sizeImage = workspace->sizeImage;
  
  dt   = workspace->dt;
  dth  = workspace->dth;
  dxy  = workspace->dxy;
  tmin = workspace->tmin;
  tmax = workspace->tmax;
  xymin= workspace->xymin;
  
  /*
  myparms.srcPtr = make_cudaPitchedPtr(blockSino, Nrays*sizeof(float), Nrays, Nangles);
  myparms.dstArray = workspace->data.dst;
  myparms.extent = make_cudaExtent(Nrays,Nangles,delta);
  myparms.kind = cudaMemcpyHostToDevice;
  cudaMemcpy3D(&myparms); //, stream);
  */

  cudaEventRecord(start);
  
  cudaMemcpyToArray(workspace->data.dst,
		    0,
		    0,
		    blockSinoDev,
		    Nrays*Nangles*delta*sizeof(float),
		    cudaMemcpyDeviceToDevice);

  checkCudaErrors(cudaPeekAtLastError());

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;
  
  dim3 gridBlockBack((int)ceil(sizeImage/threadsPerBlock.x)+1,
		     (int)ceil(sizeImage/threadsPerBlock.y)+1,
		     (int)ceil(delta/threadsPerBlock.z)+1);

  //launch kernel
  
  cudaEventRecord(start);

  kernel_bp_ss_block<<<gridBlockBack, threadsPerBlock>>>(workspace->backp.backp,
							 workspace->data.textureObject,
							 sizeImage,
							 Nrays,
							 Nangles,
							 dxy,
							 dt,
							 dth,
							 tmin,
							 tmax,
							 xymin,
							 delta);
  
  checkCudaErrors(cudaPeekAtLastError());

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&time,start,stop);
  
  if(workspace->printTimingK){
    fprintf(stderr,"\n\t%lf\t|%d-->Execute Backprojection kernel",time/1000,workspace->nblock);  
  }
  
  //copying data Device to Host
  cudaEventRecord(start);
 
  cudaMemcpy(blockBack, 
	     workspace->backp.backp, 
	     delta*sizeImage*sizeImage*sizeof(float),
	     cudaMemcpyDeviceToHost); //, stream);
  
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&memoryTime,start,stop);
  totalMemoryTime += memoryTime;
  
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Backprojections: Device to host",time/1000,workspace->nblock);  


  if(workspace->printTiming){	
    //fprintf(stderr,"\n\t%lf\t|%d-->SlantStack Kernel Time",time/1000,workspace->nblock);
    //fprintf(stderr,"\n\t%lf\t|%d-->SlantStack Memory Time",totalMemoryTime/1000,workspace->nblock);
    fprintf(stderr,"\n\t%lf\t|%d-->SlantStack Total Time -- process[%d]",((time + totalMemoryTime) / 1000), workspace->nblock,workspace->processId);
  }

  cudaEventDestroy(start);
  cudaEventDestroy(stop);
  
  //cudaStreamSynchronize(stream);
  //cudaStreamDestroy(stream);
}


