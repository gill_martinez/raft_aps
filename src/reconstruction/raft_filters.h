#ifndef RAFT_FILTERS_H
#define RAFT_FILTERS_H

#include "../common/raft_types.h"


__host__ void raft_lowpassino_gpu(float *blockSino, raft_tomo *workspace);

__host__ void raft_allocate_sinogram_lowpass_gpu_memory(raft_tomo* workspace);

__host__ void raft_deallocate_sinogram_lowpass_gpu_memory(raft_tomo* workspace);
  
__host__ void raft_lowpassino_gpu_dev2dev(float *blockSinoDev, raft_tomo *workspace);

#endif // RAFT_FILTERS_H
