#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "raft_rings.h"
#include "../common/raft_types.h"
#include <cublas_v2.h>

void ringAtKernel(raft_ring *plan, 
		  float *newsino, 
		  float *sino, 
		  int nrays, 
		  int nviews);

void ringAtKernel_getKernel(raft_tomo *workspace,
			    raft_ring *plan, 
			    int b, 
			    float *sino, 
			    int nrays, 
			    int nviews);

void convolve_rings(float *output,
		    float *signal, int signal_len,
		    float *kernel, int kernel_len);

void removeNan(float *sino, 
	       int length);
  
void maxminSumLines(raft_ring *plan, 
		    float *maxv, 
		    float *minv, 
		    float *array, 
		    int nrays, 
		    int nviews);

void meanByLines(float *mean, 
		 float *img, 
		 int lines, 
		 int columns);

void matXvec(float *output, 
	     float *signal, 
	     raft_ring *plan, 
	     int nrays, 
	     int nviews);

float dot(float *array1, 
	  float *array2, 
	  int length);

float norm(float *array, 
	   int length);


float norm_of_projection(float *sino, 
			 int nrays,
			 int nviews,
			 int column);


void max_of_projection(int *index,
		       float *max,
		       float *sino, 
		       int nrays,
		       int nviews,
		       int column);

void CGM(raft_ring *plan, 
	 float *f, 
	 float alpha, 
	 int nrays, 
	 int nviews);

__host__ void maxminSumLines_at_gpu(raft_ring *plan, 
				    float *d_array,
				    float *maxv, 
				    float *minv, 
				    float *array, 
				    int nrays, 
        			    int nviews);

__host__ void maxminSumLines_at_gpu_block(raft_ring *plan, 
				          float *d_blockSino,
					  int nrays, 
					  int nviews,
					  int blockSize);

__host__ void meanByLines_at_gpu_block(float *d_mean, 
				       float *d_blockSino, 
				       int lines, 
				       int columns,
	 			       int blockSize);

__host__ void meanByLines_at_gpu(float *mean, 
				 float *d_array, 
				 int lines, 
				 int columns);

__host__ void  matXvec_at_gpu_block(float *d_output,
				    float *d_signal,
				    raft_ring *plan,
				    int nrays,
				    int nviews,
				    int blockSize);

__host__ void  matXvec_at_gpu(float *output,
			      float *signal,
			      raft_ring *plan,
			      int nrays,
			      int nviews);

__host__ void CGM_at_gpu_block(raft_ring *plan,
			       float *f,		
			       int nrays,
			       int nviews,
			       int blockSize,
			       int Idx);

__host__ void CGM_at_gpu(raft_ring *plan,
			 float *f,		
			 float alpha,
			 int nrays,
			 int nviews);

__host__  void ringAtKernel_gpu(raft_ring *plan, 
				float *newsino, 
				float *sino, 
				int nrays, 
				int nviews);

__host__  void ringAtKernel_gpu_block(raft_ring *plan, 
				      float *dblockSino, 
				      int nrays, 
				      int nviews, 
				      int blockSize,
				      int Idx);


/*--------------------------------------
  Ring filtering in the sinogram domain
  using the Conjugate gradient Method
  via Titarenko's algorithm with a 
  fixed finite difference kernel.

  CPU-version.
  
  Output:

  sino = Rings-Method ( sino )
  -------------------------------------*/

 void raft_rings(raft_ring plan, 
	         float *sino, 
		 int nrays, 
		 int nviews)
{
  int i;
  float alpha, min;

  /*-------------
    m = 0 & n = 0
    -------------*/
  
  raft_rings_plan_set(&plan, 0, 0);
  
  ringAtKernel(&plan, plan.sinoTemp, sino, nrays, nviews);

  /*--------------
    m = 1 & n = 0 
    -------------*/
  
  raft_rings_plan_set(&plan, 1, 0);
  
  ringAtKernel(&plan, plan.sinoTemp2, sino, nrays, nviews);

  /*---------------
    Geometric mean
   ---------------*/
  
  alpha = 1.5;

  min = plan.sinoTemp2[0] * plan.sinoTemp[0];
  
  for( i = 0; i < nrays*nviews; i++ ){
    plan.sinoTemp[i] = plan.sinoTemp2[i] * plan.sinoTemp[i];
    if (plan.sinoTemp[i] < min)
      min = plan.sinoTemp[i];
  }
  
  for( i = 0; i < nrays*nviews; i++ ){
    sino[i] = sqrt(plan.sinoTemp[i] + alpha * fabs(min));
  }    
}

/*--------------------------------------
  Ring filtering in the sinogram domain
  using the Conjugate gradient Method
  via Titarenko's algorithm with a 
  fixed finite difference kernel.

  CPU-version.
  
  Output:

  kernel vector k(t) (Nrays dimension)
  such that
  
  sino(theta,t) = sino(theta,t) + k(t)

  is the new sinogram without rings.
  -------------------------------------*/

 void raft_rings_kernel(raft_tomo *workspace,
			int b,
			float *sino)
{
  int Nrays, Nangles;

  Nrays   = workspace->Nrays;
  Nangles = workspace->Nangles; 

  raft_rings_plan_set(&workspace->planRings, 1, 0);
  
  ringAtKernel_getKernel(workspace, &workspace->planRings, b, sino, Nrays, Nangles);
  
  /*
  for(int k=0; k < Nrays; k++)
    workspace->kernel[b][k] = workspace->planRings.cgm_x[k];
  
  raft_rings_plan_set(&workspace->planRings, 0, 0);
  
  ringAtKernel_getKernel(workspace, &workspace->planRings, b, sino, Nrays, Nangles);

  for(int k=0; k < Nrays; k++)
    workspace->kernel[b][k] += workspace->planRings.cgm_x[k];
  */
}

/*----------------------------------
  Ring filtering by blocks of angles
  using the same strategy of function
  raft_rings(-). This is an useful
  function for datasets presenting
  a strong ring artefact.

  Output:

  sino = Ring-Method-Blocks(sino)

  -----------------------------*/

 void raft_rings_blocks(raft_ring plan, 
			float *sino, 
			int nrays, 
			int nviews, 
			int nblocks)
{
  int k,i,j,jj,size,first,last;
  
  size = nviews/nblocks;
  
  /*//Example: block division

    fprintf(stderr,"size:=%d\n",size);

    for(k=0; k < nblocks-1; k++)
       fprintf(stderr,"block[%d]:  %d <= angle index < %d \n",k, k*size, (k+1)*size);
    
    fprintf(stderr,"block[%d]:  %d <= angle index < %d \n",(nblocks-1), (nblocks-1)*size, MAX(nblocks*size,nviews));
  */

  /* --------------------------
     Process <nblocks-1> blocks 
     ---------------------------*/
  
  for (k = 0; k < nblocks - 1; k++)
    {
      for(i=0; i < nrays; i++){
	jj=0;
	for(j = k*size; j < (k+1)*size; j++)
	  {
            FMATRIX(plan.sinoTemp, jj, i, nrays) = FMATRIX(sino, j, i, nrays);
	    jj++;
	  }
      }
      
      /* filtering k-th block */
      raft_rings(plan, plan.sinoTemp, nrays, size);

      /* update k-th block */
      for(i=0; i < nrays; i++){
	jj=0;
	for(j = k*size; j < (k+1)*size; j++)
	  {
	    FMATRIX(sino, j, i, nrays) = FMATRIX(plan.sinoTemp, jj, i, nrays);
	    jj++;
	  }
      }
    }
  
  /* ---------------------
     Processing last block
     ---------------------*/
  
  first = (nblocks-1)*size;
  last  = MAX(nblocks*size, nviews);

  for(i = 0; i < nrays; i++){
    jj=0;
    for(j = first; j < last; j++)
      {
        FMATRIX(plan.sinoTemp, jj, i, nrays) = FMATRIX(sino, j, i, nrays);
	jj++;
      }
  }
      
  /* filtering block */
  raft_rings(plan, plan.sinoTemp, nrays, size);

  /* update block */
  for(i=0; i < nrays; i++){
    jj=0;
    for(j = first; j < last; j++)
      {
	FMATRIX(sino, j, i, nrays) = FMATRIX(plan.sinoTemp, jj, i, nrays);
	jj++;
      }
  }
}

/* --------------------------------------
   Create a plan for ring filtering of 
   a tomographic sinogram.
   ---------------------------------------*/

raft_ring raft_rings_plan_create(int nrays, 
				 int nviews,
				 int blocksize)
{
  raft_ring plan;

  int MLK = 4; // maximum length of kernel (finite differences)
   
  plan.conv  = (float *)malloc((nrays+MLK) * sizeof(float));
  plan.conv_ = (float *)malloc((nrays+MLK) * sizeof(float));
  plan.mean  = (float *)malloc(nrays * sizeof(float));

  plan.cgm_x = (float *)malloc(nrays * sizeof(float));
  plan.cgm_z = (float *)malloc(nrays * sizeof(float));
  plan.cgm_r = (float *)malloc(nrays * sizeof(float));
  plan.cgm_w = (float *)malloc(nrays * sizeof(float));
  plan.zeros = (float *)calloc(nrays, sizeof(float));
  plan.sum   = (float *)malloc(nviews * sizeof(float));

  plan.kernel      = (float *)calloc(MLK, sizeof(float));
  plan.flip_kernel = (float *)calloc(MLK, sizeof(float));

  plan.sinoTemp    = (float *)malloc((nrays*nviews)*sizeof(float));
  plan.sinoTemp2   = (float *)malloc((nrays*nviews)*sizeof(float));

  // block stuff
  
  plan.withBlock = 0;

  if(blocksize > 1)
    {
      plan.withBlock = 1;

      checkCudaErrors( cudaMalloc((void **)&plan.block.conv, (nrays+MLK)*blocksize*sizeof(float) ) );
      checkCudaErrors( cudaMalloc((void **)&plan.block.conv_, (nrays+MLK)*blocksize*sizeof(float) ) );
      checkCudaErrors( cudaMalloc((void **)&plan.block.mean, (nrays)*blocksize*sizeof(float) ) );
      
      checkCudaErrors( cudaMalloc((void **)&plan.block.cgm_x[0], (nrays)*blocksize*sizeof(float) ) );
      checkCudaErrors( cudaMalloc((void **)&plan.block.cgm_x[1], (nrays)*blocksize*sizeof(float) ) );
      
      checkCudaErrors( cudaMalloc((void **)&plan.block.cgm_z, (nrays)*blocksize*sizeof(float) ) );
      
      checkCudaErrors( cudaMalloc((void **)&plan.block.cgm_r, (nrays)*blocksize*sizeof(float) ) );
      checkCudaErrors( cudaMalloc((void **)&plan.block.cgm_w, (nrays)*blocksize*sizeof(float) ) );
      checkCudaErrors( cudaMalloc((void **)&plan.block.zeros, (nrays)*blocksize*sizeof(float) ) );
      cudaMemset( plan.block.zeros, 0, nrays*blocksize*sizeof(float) );
      
      checkCudaErrors( cudaMalloc((void **)&plan.block.sum, (nviews)*blocksize*sizeof(float) ) );
      
      checkCudaErrors( cudaMalloc((void **)&plan.block.kernel, (MLK)*sizeof(float) ) );
      checkCudaErrors( cudaMalloc((void **)&plan.block.flip_kernel, (MLK)*sizeof(float) ) );
      
      checkCudaErrors( cudaMalloc((void **)&plan.block.alpha, blocksize*sizeof(float)) );
    }

  return plan;
}

/*----------------------------------------
  Destroy plan for ring filtering
  ---------------------------------------*/

 void raft_rings_plan_destroy(raft_ring *plan)
{
  free(plan->conv);
  free(plan->conv_);
  free(plan->mean);
  free(plan->cgm_x);
  free(plan->cgm_r);
  free(plan->cgm_z);
  free(plan->cgm_w);
  free(plan->zeros);
  free(plan->sum);
  free(plan->kernel);
  free(plan->flip_kernel);
  free(plan->sinoTemp);
  free(plan->sinoTemp2);

  if(plan->withBlock)
    {
      cudaFree(plan->block.conv);
      cudaFree(plan->block.conv_);
      cudaFree(plan->block.mean);
      
      cudaFree(plan->block.cgm_x[0]);
      cudaFree(plan->block.cgm_x[1]);
      
      cudaFree(plan->block.cgm_r);
      cudaFree(plan->block.cgm_z);
      cudaFree(plan->block.cgm_w);
      cudaFree(plan->block.zeros);
      cudaFree(plan->block.sum);
      cudaFree(plan->block.kernel);
      cudaFree(plan->block.flip_kernel);
      cudaFree(plan->block.alpha);
    }

}

/*------------------------------------------
  Set plan for ring filtering according to
  a previously defined dictionary of finite
  difference kernels, of order (m,n).
  ------------------------------------------*/

 void raft_rings_plan_set(raft_ring *plan, int m, int n)
{
  plan->niter = 1000;
  plan->eps   = 0.00001;

  switch(m)
    {
    case 2:
      
      plan->kernel[0] = -1.0;
      plan->kernel[1] =  3.0;
      plan->kernel[2] = -3.0;
      plan->kernel[3] =  1.0;

      plan->length_kernel = 4;

      plan->flip_kernel[0] =  1.0;
      plan->flip_kernel[1] = -3.0;
      plan->flip_kernel[2] =  3.0;
      plan->flip_kernel[3] = -1.0; 

      break;

    case 1:
      
      if (n == 0)
	{
	  plan->kernel[0] = -1.0;
	  plan->kernel[1] =  2.0;
	  plan->kernel[2] = -1.0;

	  plan->length_kernel = 3;

	  plan->flip_kernel[0] = -1.0;
	  plan->flip_kernel[1] =  2.0;
	  plan->flip_kernel[2] = -1.0;
	}
      else
	{
	  plan->kernel[0] =  2.0;
	  plan->kernel[1] = -5.0;
	  plan->kernel[2] =  4.0;
	  plan->kernel[3] = -1.0;

	  plan->length_kernel = 4;
	  
	  plan->flip_kernel[0] = -1.0;
	  plan->flip_kernel[1] =  4.0;
	  plan->flip_kernel[2] = -5.0;
	  plan->flip_kernel[3] =  2.0;
	}
      
      break;

    default:
      
      if (n == 0)
	{
	  plan->kernel[0] =  1.0;
	  plan->kernel[1] = -1.0;

	  plan->length_kernel = 2;

	  plan->flip_kernel[0] = -1.0;
	  plan->flip_kernel[1] =  1.0;
	}
      else
	{
	  if (n == 1)
	    {
	      plan->kernel[0] = -3.0/2.0;
	      plan->kernel[1] =  2.0;
	      plan->kernel[2] = -1.0/2.0;
	      
	      plan->length_kernel = 3;

	      plan->flip_kernel[0] = -1.0/2.0;
	      plan->flip_kernel[1] =  2.0;
	      plan->flip_kernel[2] = -3.0/2.0;
	    }
	  else
	    {
	      plan->kernel[0] = -11.0/6.0;
	      plan->kernel[1] =  3.0;
	      plan->kernel[2] = -3.0/2.0;
	      plan->kernel[3] =  1.0/3.0;

	      plan->length_kernel = 4;
	      
	      plan->flip_kernel[0] =  1.0/3.0;
	      plan->flip_kernel[1] = -3.0/2.0;
	      plan->flip_kernel[2] =  3.0;
	      plan->flip_kernel[3] = -11./6.0;
	    }
	}
    }

  if( plan->withBlock )
    {
      cudaMemcpy(plan->block.flip_kernel, plan->flip_kernel, plan->length_kernel*sizeof(float),cudaMemcpyHostToDevice);
      cudaMemcpy(plan->block.kernel,      plan->kernel,      plan->length_kernel*sizeof(float),cudaMemcpyHostToDevice);
    }
}

/*----------------------------------------
  Compute the ring filtering for a given
  plan, over a input sinogram.
  ---------------------------------------*/

 void ringAtKernel(raft_ring *plan, 
		   float *newsino, 
		   float *sino, 
		   int nrays, 
		   int nviews)
{
  int i, j;
  float alpha, sinomax, sinomin;

  //optional?
  //removeNan(sino, nrays*nviews);
  
  maxminSumLines(plan, &sinomax, &sinomin, sino, nrays, nviews);
  
  alpha = 1.0 / (2.0 * (sinomax - sinomin));
  
  meanByLines(plan->mean, sino, nrays, nviews);

  // conv = convolution(w, h)
  matXvec(plan->conv, plan->mean, plan, nrays, nviews); 

  for(i=0; i<nrays; i++)
    plan->conv[i] = -plan->conv[i];
    
  CGM(plan, plan->conv, alpha, nrays, nviews);
  
  // update: kronecker product of sino with plan.x (final result of CG's method)

  for(i=0; i<nrays; i++){
    for(j=0; j<nviews; j++)
      {
	//FMATRIX(newsino,j,i,nrays) = FMATRIX(sino,j,i,nrays) + plan->cgm_x[i];
	FMATRIX(newsino,j,i,nrays) = FMATRIX(sino,j,i,nrays) + plan->cgm_x[i];
      }
  }
  // fprintf(stdout,"\t Ring Filtering! CGM Convergence in <%d> iterations\n",plan->maxiter);
}


void ringAtKernel_getKernel(raft_tomo *workspace,
			    raft_ring *plan, 
			    int b, 
			    float *sino, 
			    int nrays, 
			    int nviews)
{
  int i;
  float alpha, sinomax, sinomin;
  
  //optional?
  removeNan(sino, nrays*nviews);
  
  maxminSumLines(plan, &sinomax, &sinomin, sino, nrays, nviews);
  
  alpha = 1.0 / (2.0 * (sinomax - sinomin));
  
  meanByLines(plan->mean, sino, nrays, nviews);

  // conv = convolution(w, h)
  matXvec(plan->conv, plan->mean, plan, nrays, nviews); 

  for(i=0; i<nrays; i++)
    plan->conv[i] = -plan->conv[i];
    
  CGM(plan, plan->conv, alpha, nrays, nviews);
  
  // update kernel (final result of CG's method)

  for(i=0;  i < nrays; i++)
    workspace->kernel[b][i] = plan->cgm_x[i];
}



/*--------------------------------
  Naive convolution, without 
  using FFT's, for ring filtering.
  Kernel has maximum length of 4,
  and there was no need to use 
  Fourier transformations to speed
  up calculations.
  ------------------------------*/

 void convolve_rings(float *output,
			     float *signal, int signal_len,
			     float *kernel, int kernel_len)
{
  /*-------------------------
    Convolution of two arrays
    TODO: use of FFT's ?
    ------------------------*/
  
  int n, nconv;
  float tmp;
  
  //nconv = MAX(signal_len,kernel_len);
  nconv = signal_len + kernel_len - 1;

  for (n = 0; n < nconv; n++)
    {
      int kmin, kmax, k;
      
      output[n] = 0;
      
      kmin = (n >= kernel_len - 1) ? n - (kernel_len - 1) : 0;
      kmax = (n < signal_len - 1) ? n : signal_len - 1;
      
      for (k = kmin; k <= kmax; k++)
	{
	  tmp = signal[k] * kernel[n-k];
	  output[n] += tmp;
	}
    }
}

/*---------------------------------
  Removing Nan's values of an input
  sinogram
  -------------------------------*/

void removeNan(float *sino, 
	       int length)
{
  int i;
  
  for (i = 0; i < length; i++)
    {
      if ( sino[i] != sino[i] )
	{
	  sino[i] = 0.0;
	}
    }
}
 
__global__ void removeNan_block(float *blockSino,
				int nviews,
				int nrays,
				int blockSize)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  int voxel;

  if( (tx < nrays) && (ty < nviews) && (tz<blockSize) )
    {
      voxel = tz*nrays*nviews + ty*nrays + tx;
      
      if (blockSino[ voxel ] != blockSino[ voxel ])
	{
	  blockSino[ voxel ] = 0.0;
	}

    }

}

/*---------------------------------------
  Max & Min values for the sum by lines,
  for a given sinogram
  
  Python: 
  
  max = sino.sum(0).max()
  min = sino.sum(0).min()
  ---------------------------------------*/
  
 void maxminSumLines(raft_ring *plan, 
			     float *maxv, 
			     float *minv, 
			     float *array, 
			     int nrays, 
			     int nviews)
{
  
  int i, j;
  float max, min, sum;

  for(j=0; j < nviews; j++)
    {
      sum = 0;
      for (i=0; i < nrays; i++)
	{
	  sum += FMATRIX(array, j, i, nrays);
	}
      plan->sum[j] = sum;
    }
  
  max = plan->sum[0];
  min = plan->sum[0];
  
  for (j = 1; j < nviews; j++)
    {
      if ( plan->sum[j] > max )
	{
	  max = plan->sum[j];
	}
      
      if (plan->sum[j] < min)
	{
	  min = plan->sum[j];
	}
    }

  *maxv = max;
  *minv = min;  
}


/*-------------------------------------------
  Compute mean by lines, for a given sinogram
  
  Python: sino.mean(1)
  -------------------------------------------*/

 void meanByLines(float *mean, 
		  float *img, 
		  int lines, 
		  int columns)
{
  int i, j;
  float sum, c;

  c = 1.0/((float)(columns));
  
  for(i = 0; i < lines; i++)
    {
      sum = 0;
      
      for(j = 0; j < columns; j++)
	{
	  //sum += FMATRIX(img,i,j,columns); 
	  sum += FMATRIX(img,j,i,lines); 
	}

      mean[i] = sum * c;
    }

}

/*---------------------------------
  Compute the product Matrix Vector
  for Conjugate Gradient's method
  --------------------------------*/

 void matXvec(float *output, 
		      float *signal, 
		      raft_ring *plan, 
		      int nrays, 
		      int nviews)
{
  int shift, nlen;

  shift = plan->length_kernel - 1; 
  nlen  = nrays - shift;
 
  //Example: N=|signal|=nrays=3277, K=|kernel|=2
  //array s => length: N + K-1 = 3278
  //array u => length: (K-1):N  = N - (K-1) = 3276 
  //array y => length: len(u) + K - 1 = N-K+1+k-1 = N

  convolve_rings(plan->conv_, 
		 signal, nrays, 
		 plan->flip_kernel, plan->length_kernel);
  
  convolve_rings(output, 
		 &plan->conv_[shift], nlen, 
		 plan->kernel, plan->length_kernel);
  
}

 float dot(float *array1, 
		   float *array2, 
		   int length)
{
  int i;
  float sum;
  
  sum = 0;
  for(i=0; i<length; i++)
      sum += array1[i] * array2[i];
  
  return sum;
}


 float norm(float *array, 
		    int length)
{
  int i;
  float sum;
  
  sum = 0;
  for(i=0; i<length; i++)
    {
      sum += fabs(array[i])*fabs(array[i]);
    }
  
  return sqrt(sum);
}


 float norm_of_projection(float *sino, 
			  int nrays,
			  int nviews,
			  int column)
 {
  int i;
  float v, sum;
  
  sum = 0;
  for(i=0; i < nrays; i++)
    {
      v = FMATRIX(sino, column, i, nrays); 
      sum += (v*v);
    }
  
  return sqrt(sum);
}

void max_of_projection(int *index,
		       float *max,
		       float *sino, 
		       int nrays,
		       int nviews,
		       int column)
{
  int i, indexi;
  float maxi;
  
  maxi = FMATRIX(sino, column, 0, nrays);
  indexi = 0;
  
  for (i = 1; i < nrays; i++)
    {
      if (  FMATRIX(sino, column, i, nrays) > maxi )
	{
	  maxi = FMATRIX(sino, column, i, nrays);
	  indexi = i;
	}
    }
  
  *max   = maxi;
  *index = indexi;
}

/* ----------------------------------------------
   Conjugate Gradient Method using Convolutions!!
   TO-DO: Speed-Up using FFT??
   
   CPU implementation.
   ----------------------------------------------*/

void CGM(raft_ring *plan, 
	 float *f, 
	 float alpha, 
	 int nrays, 
	 int nviews)
{
  int i,k;
  float a, b, inner[2], stopc;
  
  // r = f
  // w = (-1) r + 0
  for(i=0;i<nrays;i++){                                        
    plan->cgm_r[i] =  f[i];
    plan->cgm_w[i] = -f[i];
  }
  
  // conv = convolution(w, h)
  matXvec(plan->conv, plan->cgm_w, plan, nrays, nviews);

  // z = alpha * w + conv
  for(i=0; i<nrays; i++){
    plan->cgm_z[i] = plan->conv[i] + alpha * plan->cgm_w[i];
  }
  
  // i0 = < r, w >
  // i1 = < w, z >
  inner[0] = 0;
  for(i=0; i< nrays; i++)
    inner[0] += plan->cgm_r[i] * plan->cgm_w[i]; 

  inner[1] = 0;
  for(i = 0; i < nrays; i++)
    inner[1] += plan->cgm_w[i] * plan->cgm_z[i]; 

  a = inner[0]/inner[1];

  // x = a * w + 0
  for(i=0; i < nrays; i++)
    plan->cgm_x[i] = a * plan->cgm_w[i];
  
  for(k=0; k < plan->niter; k++)
    {
      // r = r + (-a)*z
      for(i=0;i<nrays;i++)
	plan->cgm_r[i] = plan->cgm_r[i] - a*plan->cgm_z[i];

      stopc = norm(plan->cgm_r, nrays);
      
      if( stopc < plan->eps )
	break;
      
      // i0 = < r, z >
      // i1 = < w, z >
  
      inner[0] = 0;
      for(i=0;i<nrays;i++)
	inner[0] += plan->cgm_r[i] * plan->cgm_z[i];
      
      inner[1] = 0;
      for(i=0;i<nrays;i++)
	inner[1] += plan->cgm_w[i] * plan->cgm_z[i];

      b = inner[0]/inner[1];
      
      // w = b * w - r
      for(i=0;i<nrays;i++)
	plan->cgm_w[i] = b*plan->cgm_w[i] - plan->cgm_r[i];

      // conv = convolution(w, h)
      matXvec(plan->conv, plan->cgm_w, plan, nrays, nviews);        
      
      // z = alpha * w + conv 
      for(i=0;i<nrays;i++)
	plan->cgm_z[i] = alpha * plan->cgm_w[i] + plan->conv[i];

      // i0 = < r, w >
      // i1 = < w, z >
      
      inner[0] = 0;
      for(i=0;i<nrays;i++)
	inner[0] += plan->cgm_r[i] * plan->cgm_w[i];
      
      inner[1] = 0;
      for(i=0;i<nrays;i++)
	inner[1] += plan->cgm_w[i] * plan->cgm_z[i];
      
      a = inner[0]/inner[1];
      
      // x = x + a*w
      for(i=0;i<nrays;i++)
	plan->cgm_x[i] = plan->cgm_x[i] + a * plan->cgm_w[i];
    }

  plan->maxiter = k;
}


/////////////////////////////////////////////////////////////////
//
//
//  GPU implementation
//
//
/////////////////////////////////////////////////////////////////

__host__ void debuga(float *array,
		     int size,
		     int blockSize)
{
  float *temp = (float *)malloc(size*blockSize*sizeof(float));
  int i, j;

  cudaMemcpy(temp, array, size*blockSize*sizeof(float),cudaMemcpyDeviceToHost);

  for(j = 0; j < blockSize; j++)
    {
      fprintf(stderr,"=>");
      for(i = 0; i < size; i++)
	fprintf(stderr,"[%f]",temp[j*size + i]);

      fprintf(stderr,"\n");
    }

  free(temp);
}


__global__ void kernel_sum_rows(float *array,
				float *sum,
				int nrays,
				int nviews)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x; 
  int i;
  float cumsum;
  
  cumsum = 0;
  for (i=0; i < nrays; i++)
    {
      cumsum += FMATRIX(array, tx, i, nrays);
    }
  sum[tx] = cumsum;
}


__global__ void kernel_cgm_innerproduct(float *sum,
					float *array,
					int nrays,
					int blockSize)
{
  int tz = blockIdx.x * blockDim.x + threadIdx.x; 
  int i;
  float cumsum;
  
  if( tz < blockSize )
    {
      cumsum = 0;
      for (i=0; i < nrays; i++)
	{
	  cumsum += array[ tz * nrays + i ];
	}
      sum[tz] = cumsum;
    }
}


__global__ void kernel_cgm_innerproduct_sqrt(float *sum,
					     float *array,
					     int nrays,
					     int blockSize)
{
  int tz = blockIdx.x * blockDim.x + threadIdx.x; 
  int i;
  float cumsum;
  
  if( tz < blockSize )
    {
      cumsum = 0;
      for (i=0; i < nrays; i++)
	{
	  cumsum += array[tz * nrays + i ];
	}
      sum[tz] = sqrtf( cumsum );
    }
}


__global__ void kernel_sum_rows_sqrt(float *array,
				     float *sum,
				     int nrays,
				     int nviews)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x; 
  int i;
  float cumsum;
  
  cumsum = 0;
  for (i=0; i < nrays; i++)
    {
      cumsum += FMATRIX(array, tx, i, nrays);
    }
  sum[tx] = sqrtf( cumsum );
}


__global__ void kernel_sum_rows_block(float *sum,
				      float *array,
				      int nrays,
				      int nviews,
				      int blockSize)
{
  int ty = blockIdx.x * blockDim.x + threadIdx.x; 
  int tz = blockIdx.y * blockDim.y + threadIdx.y; 
  
  int i;
  float cumsum;

  if( (ty < nviews) &&  (tz < blockSize) )
  { 
     cumsum = 0;
     for (i=0; i < nrays; i++)
      {
        cumsum += array[tz*nrays*nviews + ty*nrays + i];
      } 

     sum[tz*nviews + ty] = cumsum;
  }
}

__host__ void maxminSumLines_at_gpu(raft_ring *plan, 
				    float *d_array,
				    float *maxv, 
				    float *minv, 
				    float *array, 
				    int nrays, 
        			    int nviews)
{
  int  j;
  float max, min;
  float *d_sum;
  
  dim3 ThreadsPerBlock(TPBX,1,1); 
  dim3 gridArray((int)ceil(nviews/ThreadsPerBlock.x)+1,1,1);
  
  cudaMalloc((void **) &d_sum,  nviews*sizeof(float));
  
  kernel_sum_rows<<<gridArray,ThreadsPerBlock>>>(d_array,d_sum, nrays, nviews);
    
  cudaMemcpy(plan->sum, d_sum, nviews*sizeof(float),cudaMemcpyDeviceToHost);
  
  max = plan->sum[0];
  min = plan->sum[0];
  
  for (j = 1; j < nviews; j++)
    {
      if ( plan->sum[j] > max )
	{
	  max = plan->sum[j];
	}
      
      if (plan->sum[j] < min)
	{
	  min = plan->sum[j];
	}
    }

  *maxv = max;
  *minv = min;  

  cudaFree(d_sum);
}


__global__ void kernel_set_alpha(float *d_alpha, 
				 float *d_sum,
				 int idxmin,
				 int idxmax,
				 int nviews,
				 int blockSize)
{
  /*
  int tz = blockIdx.x * blockDim.x + threadIdx.x; 

  float maxv, minv;

  if( (tz < blockSize ))
  {
    maxv = d_sum[ tz*nviews + idxmax[tz] ] ;
    minv = d_sum[ tz*nviews + idxmin[tz] ];
      
    d_alpha[ tz ] = 1.0/ ( 2.0* ( maxv - minv ) ); 
  }
  */
  int tz = blockIdx.x * blockDim.x + threadIdx.x; 

  float maxv, minv;

  if( (tz < blockSize ))
  {
    maxv = d_sum[ idxmax ] ;
    minv = d_sum[ idxmin ];
      
    d_alpha[ tz ] = 1.0/ ( 2.0* ( maxv - minv ) ); 
  }
  
}


__host__ void maxminSumLines_at_gpu_block(raft_ring *plan, 
				          float *d_blockSino,
					  int nrays, 
					  int nviews,
					  int blockSize)
{
  float *temp = (float *)malloc(sizeof(float)*blockSize);
  int i;

  for(i=0;i<blockSize;i++) temp[i]=0.091;

  cudaMemcpy( plan->block.alpha, temp, blockSize*sizeof(float), cudaMemcpyHostToDevice );
  
  free(temp);

  /*
  int  j;
 
  dim3 ThreadsPerBlock(TPBX,TPBZ,1); 
  dim3 gridMatrix((int)ceil(nviews/ThreadsPerBlock.x)+1,(int)ceil(blockSize/ThreadsPerBlock.y)+1,1);
  
  kernel_sum_rows_block<<<gridMatrix,ThreadsPerBlock>>>(plan->block.sum, d_blockSino, nrays, nviews, blockSize);
  
  cublasHandle_t handle;
  cublasCreate(&handle);
  int amin, amax;

  
  if( cublasIsamin(handle, nviews, plan->block.sum, 1, &amin) != CUBLAS_STATUS_SUCCESS)
    fprintf(stderr,"putz!!");
      
  if ( cublasIsamax(handle, nviews, plan->block.sum, 1, &amax) != CUBLAS_STATUS_SUCCESS )
    fprintf(stderr,"putz!!");
  
  //kernel_set_alpha<<<1,blockSize>>>(plan->block.alpha, plan->block.sum, amin, amax, nviews, blockSize);

  cublasDestroy(handle);  
  */


  /*
  int  j;
 
  dim3 ThreadsPerBlock(TPBX,TPBZ,1); 
  dim3 gridMatrix((int)ceil(nviews/ThreadsPerBlock.x)+1,(int)ceil(blockSize/ThreadsPerBlock.y)+1,1);
  
  kernel_sum_rows_block<<<gridMatrix,ThreadsPerBlock>>>(plan->block.sum, d_blockSino, nrays, nviews, blockSize);
  
  cublasHandle_t handle;
  cublasCreate(&handle);

  int amax, amin, *idxmax, *idxmin, *argmin, *argmax;

  argmin = (int *)malloc(sizeof(int)*blockSize);
  argmax = (int *)malloc(sizeof(int)*blockSize);
  argmax = (int *)malloc(sizeof(int)*blockSize);

  cudaMalloc((void **) &idxmin, blockSize*sizeof(float)); 
  cudaMalloc((void **) &idxmax, blockSize*sizeof(float));  

  for(j = 0; j < blockSize; j ++)
    {
      if( cublasIsamin(handle, nviews, &plan->block.sum[ j*nviews ], 1, &amin) != CUBLAS_STATUS_SUCCESS)
	fprintf(stderr,"putz!!");
      
      if ( cublasIsamax(handle, nviews, &plan->block.sum[ j*nviews ], 1, &amax) != CUBLAS_STATUS_SUCCESS )
	fprintf(stderr,"putz!!");
      
      argmin[j] = amin;
      argmax[j] = amax;
    }
  
  cudaMemcpy( idxmax, argmax, sizeof(int)*blockSize, cudaMemcpyHostToDevice);
  cudaMemcpy( idxmin, argmin, sizeof(int)*blockSize, cudaMemcpyHostToDevice);

  kernel_set_alpha<<<1,blockSize>>>(plan->block.alpha, plan->block.sum, idxmin, idxmax, nviews, blockSize);

  //cublasSnrm2(handle, blockSize, stopc, 1, &norm);

  cublasDestroy(handle);
  cudaFree(idxmin);
  cudaFree(idxmax);
  free(argmin);
  free(argmax);
  */
}


__global__ void kernel_sum_columns(float *mean,
				   float *array,
				   int lines,
				   int columns)
{
  int ty = blockIdx.x * blockDim.x + threadIdx.x; 
  int j;
  float cumsum, c = 1.0/((float)(columns));

  cumsum = 0.0;
  for (j = 0; j < columns; j++)
    {
      cumsum += array[j*lines + ty];
    }

  mean[ty] = cumsum * c;
}


__global__ void kernel_sum_columns_block(float *mean,
					 float *array,
					 int lines,
					 int columns,
					 int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x; 
  int tz = blockIdx.y * blockDim.y + threadIdx.y; 

  int j;
  float cumsum, c = 1.0/((float)(columns));

  if( (tx < lines) && (tz<blockSize))
    {
      cumsum = 0.0;
      for (j = 0; j < columns; j++)
	{
	  cumsum += array[tz*lines*columns + j*lines + tx];
	}
      
      mean[ tz*lines + tx ] = cumsum * c;
    }
}


__host__ void meanByLines_at_gpu_block(float *d_mean, 
				       float *d_blockSino, 
				       int lines, 
				       int columns,
	 			       int blockSize)
{
  
  dim3 ThreadsPerBlock(TPBY,TPBZ);
  dim3 gridArray((int)ceil(lines/ThreadsPerBlock.x)+1, (int)ceil(blockSize/ThreadsPerBlock.y)+1 );  

  kernel_sum_columns_block<<<gridArray, ThreadsPerBlock >>>(d_mean, d_blockSino, lines, columns, blockSize);
}


__host__ void meanByLines_at_gpu(float *mean, 
				 float *d_array, 
				 int lines, 
				 int columns)
{
  float *d_mean;
  
  dim3 ThreadsPerBlock(TPBX,1);
  dim3 gridArray((int)ceil(lines/ThreadsPerBlock.x)+1, 1 );  

  cudaMalloc((void **)&d_mean, lines*sizeof(float));
  
  kernel_sum_columns<<<gridArray, ThreadsPerBlock >>>(d_mean, d_array, lines, columns);
  
  cudaMemcpy(mean, d_mean, lines*sizeof(float),cudaMemcpyDeviceToHost);
  
  cudaFree(d_mean);
}


__global__ void kernel_update_sino(float *d_sino,
				   float *d_cgm, 
				   int nrays,
				   int nviews)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int ty = threadIdx.y + blockIdx.y * blockDim.y;

  if( (tx < nviews) && (ty < nrays) )
    {
      d_sino[tx*nrays + ty] = d_sino[tx * nrays + ty] + d_cgm[ty];
    }
}


__global__ void kernel_convolve_rings_block(float *output,
					    float *signal,
					    int signal_len,
					    float *kernel,
					    int kernel_len,
					    int blockSize,
					    int nrays,
					    int shift)
					    

{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int tz = threadIdx.y + blockIdx.y * blockDim.y;
  int k, nconv;
  int kmin,kmax;
  float cumsum=0;

  nconv = signal_len + kernel_len - 1;

  if( (tx < nconv) && (tz < blockSize))
    {
      cumsum = 0;
      
      kmin = (tx >= kernel_len - 1) ? tx - (kernel_len - 1) : 0;
      kmax = (tx < signal_len - 1) ? tx : signal_len - 1;
    
      for (k = kmin; k <= kmax; k++)
	{
	  cumsum += signal[ tz*nconv + k + shift] * kernel[tx-k];  
	}
      
      output[ tz*nconv + tx] = cumsum;     
    }
}



__global__ void kernel_convolve_rings(float *output,
				      float *signal,
				      int signal_len,
				      float *kernel,
				      int kernel_len)

{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int k, nconv;
  int kmin,kmax;
  float cumsum=0;

  //nconv = MAX(signal_len,kernel_len);
  nconv = signal_len + kernel_len - 1;

  if(tx < nconv)
    {
      cumsum = 0;

      kmin = (tx >= kernel_len - 1) ? tx - (kernel_len - 1) : 0;
      kmax = (tx < signal_len - 1) ? tx : signal_len - 1;
    
      for (k = kmin; k <= kmax; k++)
	{
	  cumsum += signal[k] * kernel[tx-k];
	}

      output[tx] = cumsum;     
    }
}

__host__ void  matXvec_at_gpu_block(float *d_output,
				    float *d_signal,
				    raft_ring *plan,
				    int nrays,
				    int nviews,
				    int blockSize)
{
  int shift, nlen;
  
  shift = plan->length_kernel - 1; 
  nlen  = nrays - shift;
  //nconv = nrays + plan->length_kernel - 1;
  //
  //Example: N=|signal|=nrays=3277, K=|kernel|=2
  //array s => length: N + K-1 = 3278
  //array u => length: (K-1):N  = N - (K-1) = 3276 
  //array y => length: len(u) + K - 1 = N-K+1+k-1 = N
  
  dim3 ThreadsPerBlock(TPBX, TPBZ); 
  dim3 gridBlockA((int)ceil((nrays+plan->length_kernel-1)/ThreadsPerBlock.x)+1, (int)ceil(blockSize/ThreadsPerBlock.y)+1);
  
  kernel_convolve_rings_block<<<gridBlockA,ThreadsPerBlock>>>(plan->block.conv_, 
							      d_signal, nrays,
							      plan->block.flip_kernel, plan->length_kernel, 
							      blockSize,
							      nrays,
							      0);

  
  dim3 gridBlockC((int)ceil((nlen+plan->length_kernel-1)/ThreadsPerBlock.x)+1, (int)ceil(blockSize/ThreadsPerBlock.y)+1);
  
  kernel_convolve_rings_block<<<gridBlockC,ThreadsPerBlock>>>(d_output,
							      plan->block.conv_, nlen,
							      plan->block.kernel, plan->length_kernel, 
							      blockSize,
							      nrays,
							      shift);

} 


__host__ void  matXvec_at_gpu(float *output,
			      float *signal,
			      raft_ring *plan,
			      int nrays,
			      int nviews)
{
  int shift, nlen, nconv;
  float *d_output,*d_signal,*d_kernel;
  
  shift = plan->length_kernel - 1; 
  nlen  = nrays - shift;
  nconv = nrays + plan->length_kernel - 1;
 
  //Example: N=|signal|=nrays=3277, K=|kernel|=2
  //array s => length: N + K-1 = 3278
  //array u => length: (K-1):N  = N - (K-1) = 3276 
  //array y => length: len(u) + K - 1 = N-K+1+k-1 = N
  
  cudaMalloc((void **) &d_output,(nrays + plan->length_kernel)*sizeof(float));
  cudaMalloc((void **) &d_signal,nrays*sizeof(float));
  cudaMalloc((void **) &d_kernel,plan->length_kernel*sizeof(float));
  
  cudaMemcpy(d_signal, signal, nrays*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_kernel, plan->flip_kernel, plan->length_kernel*sizeof(float),cudaMemcpyHostToDevice);
  
  dim3 ThreadsPerBlock(TPBX); 
  dim3 gridBlock((int)ceil(nconv/ThreadsPerBlock.x)+1);

  kernel_convolve_rings<<<gridBlock,ThreadsPerBlock>>>(d_output, 
						       d_signal,
						       nrays,
						       d_kernel,
						       plan->length_kernel);

  cudaMemcpy(plan->conv_,d_output,(nrays+plan->length_kernel)*sizeof(float),cudaMemcpyDeviceToHost);
  
  cudaMemcpy(d_signal,&plan->conv_[shift],nlen*sizeof(float),cudaMemcpyHostToDevice);
  
  cudaMemcpy(d_kernel,plan->kernel,plan->length_kernel*sizeof(float),cudaMemcpyHostToDevice);  
  
  kernel_convolve_rings<<<gridBlock,ThreadsPerBlock>>>(d_output,
						       d_signal,
						       nlen,
						       d_kernel,
						       plan->length_kernel);  

  cudaMemcpy(output,d_output,(nrays+plan->length_kernel-1)*sizeof(float),cudaMemcpyDeviceToHost);

  cudaFree(d_kernel);
  cudaFree(d_signal);
  cudaFree(d_output);
} 

/////////////////////////////////////


__global__ void kernel_x_ay(float *x,
			    float *y,
			    float a,
			    int size,
			    int blockSize)
{
  //TODO: cublas
  // x = a * y

  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int tz = threadIdx.y + blockIdx.y * blockDim.y;

  if( (tx < size) && (tz < blockSize))
    {
      x[ tz*size + tx ] = y[ tz*size + tx ] * a;
    }
}


__global__ void kernel_x_ay_block(float *x,
				  float *y,
				  float *a,
				  int size,
				  int blockSize)
{
  //TODO: cublas
  // x = a * y

  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int tz = threadIdx.y + blockIdx.y * blockDim.y;

  if( (tx < size) && (tz < blockSize))
    {
      x[ tz*size + tx ] = y[ tz*size + tx] * a[ tz ];     
    }
}


__global__ void kernel_x_ax(float *x,
			    float a,
			    int size,
			    int blockSize)
{
  //TODO: cublas
  // x = a * x

  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int tz = threadIdx.y + blockIdx.y * blockDim.y;

  if( (tx < size) && (tz < blockSize))
    {
      x[ tz*size + tx ] = x[ tz*size + tx ] * a;     
    }
}

__global__ void kernel_x_aupy(float *x,
			      float *u,
			      float *y,
			      float *a,
			      int size,
			      int blockSize)
{
  //TODO: cublas
  // x = a * u + y

  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int tz = threadIdx.y + blockIdx.y * blockDim.y;

  if( (tx < size) && (tz < blockSize))
    {
      x[ tz*size + tx ] = u[ tz*size + tx] * a[ tz ] + y[ tz*size + tx ];
    }
}


__global__ void kernel_x_aumy(float *x,
			      float *u,
			      float *y,
			      float *a,
			      int size,
			      int blockSize)
{
  //TODO: cublas
  // x = a * u - y
  
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int tz = threadIdx.y + blockIdx.y * blockDim.y;
  
  if( (tx < size) && (tz < blockSize))
    {
      x[ tz*size + tx ] = u[ tz*size + tx ] * a[ tz ] - y[ tz*size + tx ];
    }
}


__global__ void kernel_x_maupy(float *x,
			       float *u,
			       float *y,
			       float *a,
			       int size,
			       int blockSize)
{
  //TODO: cublas
  // x = (-a) * u + y

  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int tz = threadIdx.y + blockIdx.y * blockDim.y;

  if( (tx < size) && (tz < blockSize))
    {
      x[ tz*size + tx ] = - u[ tz*size + tx ] * a[tz] + y[ tz*size + tx ];
    }
}

__global__ void kernel_pointwise_product(float *a,
					 float *b,
					 float *c,
					 int size,
					 int blockSize)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int tz = threadIdx.y + blockIdx.y * blockDim.y;
  int pixel;

  if( (tx < size) && (tz < blockSize))
    {
      pixel = tz*size + tx;
      
      a[ pixel ] =  b[ pixel ] * c[ pixel ];
    }
}

__host__ void cgm_innerproduct(float *inner,
			       float *x,
			       float *y,
			       int size,
			       int blockSize)
{
  float *temp;

  cudaMalloc((void **)&temp, size*blockSize*sizeof(float));
  
  dim3 threads(TPBX,TPBZ);
  dim3 grid((int)ceil(size/threads.x)+1,(int)ceil(blockSize/threads.y)+1);

  kernel_pointwise_product<<<grid,threads>>>(temp, x , y, size, blockSize);
  
  kernel_cgm_innerproduct<<<1,blockSize>>>(inner, temp, size, blockSize);      

  checkCudaErrors(cudaPeekAtLastError());


  cudaFree(temp);
}

__host__ void cgm_norm(float *inner,
		       float *x,
		       int size,
		       int blockSize)
{
  float *temp;

  cudaMalloc((void **)&temp, size*blockSize*sizeof(float));
  
  dim3 threads(TPBX,TPBZ);
  dim3 grid((int)ceil(size/threads.x)+1,(int)ceil(blockSize/threads.y)+1);

  kernel_pointwise_product<<<grid,threads>>>(temp, x , x, size, blockSize);

  kernel_cgm_innerproduct_sqrt<<<1,blockSize>>>(inner, temp, size, blockSize);      

  cudaFree(temp);
}


__global__ void kernel_x_pointwiseDivision(float *div,
					   float *x,
					   float *y,
					   int size)
{
  //TODO: cublas
  // div[i] = x[i]/y[i]

  int tz = threadIdx.x + blockIdx.x * blockDim.x;
  
  if(tz < size)
    {
      div[tz] = (x[tz]/y[tz]);
    }
}

__host__ int should_cgm_stop(float *stopc,
			     float eps,
			     int size,
			     int iter)
{
  /*
  float *host;
  int arg,criteria;
  cublasHandle_t handle;
  host = (float *)malloc(size*sizeof(float));
  cublasCreate(&handle);
  cublasIsamin(handle, size, stopc, 1, &arg);
  if(host[arg]<eps)
    criteria=1;
  else
    criteria=0;
  cublasDestroy(handle);
  free(host);
  return criteria;
  */
  
  float *host;
  int k, count=0, criteria;

  host = (float *)malloc(size*sizeof(float));
  cudaMemcpy(host, stopc, size*sizeof(float), cudaMemcpyDeviceToHost );

  fprintf(stderr,"***<%d>***=>",iter);
  for(k = 0; k < size; k++ )
    {
      fprintf(stderr,"[%lf]{%lf}",host[k],eps);
      if(host[k] < eps)
	count++;
    }
  fprintf(stderr,"\n");
  
  if(count==size)
    criteria = 1;
  else
    criteria = 0;
  
  free(host);

  return criteria;
}


__host__ void CGM_at_gpu_block(raft_ring *plan,
			       float *f, 
			       int nrays,
			       int nviews,
			       int blockSize,
			       int Idx)
{
  int k;
  float *a, *b, *inner0, *inner1, *stopc, *h_stopc;
  
  cudaMalloc((void **)&inner0, blockSize*sizeof(float));
  cudaMalloc((void **)&inner1, blockSize*sizeof(float));
  cudaMalloc((void **)&a, blockSize*sizeof(float));
  cudaMalloc((void **)&b, blockSize*sizeof(float));
  cudaMalloc((void **)&stopc, blockSize*sizeof(float));
  h_stopc = (float *)malloc(blockSize*sizeof(float));

  dim3 threads(TPBX,TPBZ);
  dim3 grid((int)ceil(nrays/threads.x)+1,(int)ceil(blockSize/threads.y)+1);
  
  // r = f
  // w = (-1) r
  kernel_x_ay<<<grid,threads>>>(plan->block.cgm_r, f,  1.0, nrays, blockSize);
  kernel_x_ay<<<grid,threads>>>(plan->block.cgm_w, f, -1.0, nrays, blockSize);

  // conv = convolution(w, h)
  matXvec_at_gpu_block(plan->block.conv, plan->block.cgm_w, plan, nrays, nviews, blockSize);

  // z = alpha * w + conv (x = alpha*u + y)
  kernel_x_aupy<<<grid,threads>>>(plan->block.cgm_z, plan->block.cgm_w, plan->block.conv, plan->block.alpha, nrays, blockSize);

  // i0 = < r, w >
  // i1 = < w, z >
  cgm_innerproduct(inner0, plan->block.cgm_r, plan->block.cgm_w, nrays, blockSize);
  cgm_innerproduct(inner1, plan->block.cgm_w, plan->block.cgm_z, nrays, blockSize);

  // a = inner0/inner1 
  kernel_x_pointwiseDivision<<<1,blockSize>>>(a, inner0, inner1, blockSize);

  // x = a * w + 0
  kernel_x_ay_block<<<grid,threads>>>(plan->block.cgm_x[Idx], plan->block.cgm_w, a, nrays, blockSize);

  cublasHandle_t handle;
  int stop;
  cublasCreate(&handle);

  //for(k = 0; k < plan->niter; k++)
  for(k = 0; k < plan->niter; k++)
    {
      // r = (-a)*z + r  ( or x = (-a)*u + y)
      kernel_x_maupy<<<grid,threads>>>(plan->block.cgm_r, plan->block.cgm_z, plan->block.cgm_r, a, nrays, blockSize);


      // stopc = norm( r )
      cgm_norm(stopc, plan->block.cgm_r, nrays, blockSize);

      // check stopping criteria!!!
      //cublasSnrm2(handle, blockSize, stopc, 1, &norm);

      stop = should_cgm_stop( stopc, plan->eps, blockSize, k) ;
      
      //
      //if( norm < (plan->eps) )
      if(stop)
	break;
      
      // i0 = < r, z >
      // i1 = < w, z >
      cgm_innerproduct(inner0, plan->block.cgm_r, plan->block.cgm_z, nrays, blockSize);
      cgm_innerproduct(inner1, plan->block.cgm_w, plan->block.cgm_z, nrays, blockSize);

      checkCudaErrors(cudaPeekAtLastError());
      
      // b = inner0/inner1;
      kernel_x_pointwiseDivision<<<1,blockSize>>>(b, inner0, inner1, blockSize);
      
      checkCudaErrors(cudaPeekAtLastError());

      // w = b * w - r
      kernel_x_aumy<<<grid,threads>>>(plan->block.cgm_w, plan->block.cgm_w, plan->block.cgm_r, b, nrays, blockSize);
      
      // conv = convolution(w, h)
      matXvec_at_gpu_block(plan->block.conv, plan->block.cgm_w, plan, nrays, nviews, blockSize);  
     
      // z = alpha * w + conv 
      kernel_x_aupy<<<grid,threads>>>(plan->block.cgm_z, plan->block.cgm_w, plan->block.conv, plan->block.alpha, nrays, blockSize);
      
      checkCudaErrors(cudaPeekAtLastError());
	
      // i0 = < r, w >
      // i1 = < w, z >
      cgm_innerproduct(inner0, plan->block.cgm_r, plan->block.cgm_w, nrays, blockSize);
      cgm_innerproduct(inner1, plan->block.cgm_w, plan->block.cgm_z, nrays, blockSize);
      
      //a = inner[0]/inner[1];
      kernel_x_pointwiseDivision<<<1,blockSize>>>(a, inner0, inner1, blockSize);
      
      // x = a*w + x
      kernel_x_aupy<<<grid,threads>>>(plan->block.cgm_x[Idx], plan->block.cgm_w, plan->block.cgm_x[Idx], a, nrays, blockSize);
    }

  plan->maxiter = k;

  cublasDestroy(handle);
  cudaFree(stopc);
  cudaFree(a);
  cudaFree(b);
  cudaFree(inner0);
  cudaFree(inner1);
  free(h_stopc);
}


__host__ void CGM_at_gpu(raft_ring *plan,
			 float *f,		
			 float alpha,
			 int nrays,
			 int nviews)
{
  int i,k;
  float a, b, inner[2], stopc;
  
  // r = f
  // w = (-1) r + 0
  for(i=0;i<nrays;i++){                                        
    plan->cgm_r[i] =  f[i];
    plan->cgm_w[i] = -f[i];
  }
  
  // conv = convolution(w, h)
  matXvec_at_gpu(plan->conv, plan->cgm_w, plan, nrays, nviews);

  // z = alpha * w + conv
  for(i=0; i<nrays; i++){
    plan->cgm_z[i] = plan->conv[i] + alpha * plan->cgm_w[i];
  }
  
  // i0 = < r, w >
  // i1 = < w, z >
  inner[0] = 0;
  for(i=0; i< nrays; i++)
    inner[0] += plan->cgm_r[i] * plan->cgm_w[i]; 

  inner[1] = 0;
  for(i = 0; i < nrays; i++)
    inner[1] += plan->cgm_w[i] * plan->cgm_z[i]; 

  a = inner[0]/inner[1];

  // x = a * w + 0
  for(i=0; i < nrays; i++)
    plan->cgm_x[i] = a * plan->cgm_w[i];
  
  for(k=0; k < plan->niter; k++)
    {
      // r = r + (-a)*z
      for(i=0;i<nrays;i++)
	plan->cgm_r[i] = plan->cgm_r[i] - a*plan->cgm_z[i];

      stopc = norm(plan->cgm_r, nrays);
      
      if( stopc < plan->eps )
	break;
      
      // i0 = < r, z >
      // i1 = < w, z >
  
      inner[0] = 0;
      for(i=0;i<nrays;i++)
	inner[0] += plan->cgm_r[i] * plan->cgm_z[i];
      
      inner[1] = 0;
      for(i=0;i<nrays;i++)
	inner[1] += plan->cgm_w[i] * plan->cgm_z[i];

      b = inner[0]/inner[1];
      
      // w = b * w - r
      for(i=0;i<nrays;i++)
	plan->cgm_w[i] = b*plan->cgm_w[i] - plan->cgm_r[i];

      // conv = convolution(w, h)
      matXvec_at_gpu(plan->conv, plan->cgm_w, plan, nrays, nviews);        
      
      // z = alpha * w + conv 
      for(i=0;i<nrays;i++)
	plan->cgm_z[i] = alpha * plan->cgm_w[i] + plan->conv[i];

      // i0 = < r, w >
      // i1 = < w, z >
      
      inner[0] = 0;
      for(i=0;i<nrays;i++)
	inner[0] += plan->cgm_r[i] * plan->cgm_w[i];
      
      inner[1] = 0;
      for(i=0;i<nrays;i++)
	inner[1] += plan->cgm_w[i] * plan->cgm_z[i];
      
      a = inner[0]/inner[1];
      
      // x = x + a*w
      for(i=0;i<nrays;i++)
	plan->cgm_x[i] = plan->cgm_x[i] + a * plan->cgm_w[i];
    }

  plan->maxiter = k;
}



/*----------------------------------------
  GPU
  
  Compute the ring filtering for a given
  plan, over a input sinogram.
  ---------------------------------------*/

__host__  void ringAtKernel_gpu(raft_ring *plan, 
				float *newsino, 
				float *sino, 
				int nrays, 
				int nviews)
{
  int i;
  float alpha, sinomax, sinomin, *d_sino, *d_cgm;

  removeNan(sino, nrays*nviews);

  dim3 threadsPerBlock(TPBX,TPBY,1);
  dim3 gridMatrix((int)ceil(nviews/threadsPerBlock.x)+1,(int)ceil(nrays/threadsPerBlock.y)+1,1);
  
  cudaMalloc((void **) &d_sino, nrays*nviews*sizeof(float)); 
  cudaMalloc((void **) &d_cgm, nrays*sizeof(float));

  cudaMemcpy(d_sino, sino, nviews*nrays*sizeof(float),cudaMemcpyHostToDevice);
   
  maxminSumLines_at_gpu(plan, d_sino, &sinomax, &sinomin, sino, nrays, nviews);

  alpha = 1.0 / (2.0 * (sinomax - sinomin));
  
  meanByLines_at_gpu(plan->mean, d_sino, nrays, nviews);
  
  // conv = convolution(w, h)
  matXvec_at_gpu(plan->conv,plan->mean,plan,nrays,nviews);
  
  for(i=0; i<nrays; i++){
    plan->conv[i] = -plan->conv[i];  
  }
     
  CGM_at_gpu(plan,plan->conv,alpha,nrays,nviews);
     
  // update: kronecker product of sino with plan.x (final result of CG's method)
  
  cudaMemcpy(d_cgm, plan->cgm_x, nrays*sizeof(float), cudaMemcpyHostToDevice);
  
  kernel_update_sino<<<gridMatrix, threadsPerBlock>>>(d_sino, d_cgm, nrays, nviews);
  
  cudaMemcpy(newsino, d_sino, nrays*nviews*sizeof(float), cudaMemcpyDeviceToHost);

  cudaFree(d_sino);  
  cudaFree(d_cgm);
}


__host__  void ringAtKernel_gpu_block(raft_ring *plan, 
				      float *dblockSino, 
				      int nrays, 
				      int nviews,
				      int blockSize,
				      int Idx)
{
     
  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil(nrays/threadsPerBlock.x)+1, (int)ceil(nviews/threadsPerBlock.y)+1, (int)ceil(blockSize/threadsPerBlock.z)+1);
  
  //removeNan_block<<<gridBlock,threadsPerBlock>>>(dblockSino, nviews, nrays, blockSize);
   
  maxminSumLines_at_gpu_block(plan, dblockSino, nrays, nviews, blockSize);

  meanByLines_at_gpu_block(plan->block.mean, dblockSino, nrays, nviews, blockSize);
  
  // conv = convolution(w, h)
  matXvec_at_gpu_block(plan->block.conv, plan->block.mean, plan, nrays, nviews, blockSize);
  
  // conv = - conv
  dim3 threads(TPBX,TPBZ);
  dim3 grid((int)ceil(nrays/threads.x)+1,(int)ceil(blockSize/threads.y)+1);

  kernel_x_ax<<<grid,threads>>>(plan->block.conv, -1.0, nrays, blockSize);

  CGM_at_gpu_block(plan, plan->block.conv, nrays, nviews, blockSize, Idx);
}



__global__ void kernel_pointwise_sinograms(float *d_sinoTemp,
					   float *d_sinoTemp2, 
					   int nrays,
					   int nviews)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int ty = threadIdx.y + blockIdx.y * blockDim.y;

  if( (tx < nrays) && (ty < nviews) )
    {
      d_sinoTemp[ty*nrays + tx] = d_sinoTemp[ty*nrays + tx] * d_sinoTemp2[ty*nrays + tx];
    }
}


__global__ void kernel_add_block(float *dsino,
				 float *kernel,
				 int nrays,
				 int nviews,
				 int blockSize)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int ty = threadIdx.y + blockIdx.y * blockDim.y;
  int tz = threadIdx.z + blockIdx.z * blockDim.z;
  int voxel;

  if( (tx < nrays) && (ty < nviews) && (tz < blockSize) )
    {
      voxel = tz*nrays*nviews + ty*nrays + tx;
      
      dsino[voxel ] = dsino[voxel] + kernel[ tz*nrays + tx ];
    }
}


__global__ void kernel_pointwiseProduct_block(float *X,
					      float *Y,
					      int nrays,
					      int nviews,
					      int blockSize)
{
  // X = X*Y
  //
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int ty = threadIdx.y + blockIdx.y * blockDim.y;
  int tz = threadIdx.z + blockIdx.z * blockDim.z;
  int voxel;

  if( (tx < nrays) && (ty < nviews) && (tz < blockSize) )
    {
      voxel = tz*nrays*nviews + ty*nrays + tx;
      
      X[voxel ] = X[voxel] * Y[ voxel ];
    }
}


__global__  void kernel_sino_update_geometric_mean(float *d_sino,
						   float alpha,
						   int argmin,
						   int nrays,
						   int nviews)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int ty = threadIdx.y + blockIdx.y * blockDim.y;

  if( (tx < nrays) && (ty < nviews) )
    {
      d_sino[ty*nrays + tx] = sqrtf( d_sino[ty*nrays + tx] + alpha * fabsf( d_sino[argmin] ) );
    }

}

__global__  void kernel_sino_update_geometric_mean_block(float *d_sino,
							 float alpha,
							 int argmin,
							 int nrays,
							 int nviews,
							 int blockSize)
{
  int tx = threadIdx.x + blockIdx.x * blockDim.x;
  int ty = threadIdx.y + blockIdx.y * blockDim.y;
  int tz = threadIdx.z + blockIdx.z * blockDim.z;
  int voxel;

  if( (tx < nrays) && (ty < nviews) && (tz < blockSize) )
    {
      voxel = tz*nrays*nviews + ty*nrays + tx;
      
      d_sino[voxel ] = sqrtf ( d_sino[voxel] +  alpha * fabsf( d_sino[argmin] ) );
    }
}


__host__ void raft_rings_gpu(raft_ring *plan, 
			     float *sino, 
			     int nrays, 
			     int nviews)
{
  int argmin;
  float alpha;
  float *d_sinoTemp, *d_sinoTemp2;

  /*-------------
    m = 0 & n = 0
    -------------*/
  
  raft_rings_plan_set(plan, 0, 0);
  
  ringAtKernel(plan, plan->sinoTemp, sino, nrays, nviews);

  /*--------------
    m = 1 & n = 0 
    -------------*/
  
  raft_rings_plan_set(plan, 1, 0);
  
  ringAtKernel(plan, plan->sinoTemp2, sino, nrays, nviews);

  /*---------------
    Geometric mean
   ---------------*/
  
  dim3 ThreadsPerBlock(TPBX,TPBY);
  dim3 gridMatrix((int)ceil(nrays/ThreadsPerBlock.x)+1,(int)ceil(nviews/ThreadsPerBlock.y)+1);

  alpha = 1.5;
 
  cudaMalloc((void **) &d_sinoTemp,  nrays * nviews * sizeof(float)); 
  cudaMalloc((void **) &d_sinoTemp2, nrays * nviews * sizeof(float));

  cudaMemcpy(d_sinoTemp, plan->sinoTemp, nviews*nrays*sizeof(float),cudaMemcpyHostToDevice);
  cudaMemcpy(d_sinoTemp2, plan->sinoTemp2, nviews*nrays*sizeof(float),cudaMemcpyHostToDevice);

  kernel_pointwise_sinograms<<<gridMatrix, ThreadsPerBlock >>>( d_sinoTemp, d_sinoTemp2, nrays, nviews );

  cublasHandle_t handle;
  cublasCreate(&handle);
  cublasIsamin(handle, nrays*nviews, d_sinoTemp, 1, &argmin);

  kernel_sino_update_geometric_mean<<<gridMatrix, ThreadsPerBlock >>>( d_sinoTemp, alpha, argmin, nrays, nviews );

  cudaMemcpy(sino, d_sinoTemp, nrays*nviews*sizeof(float), cudaMemcpyDeviceToHost);

  cudaFree(d_sinoTemp);
  cudaFree(d_sinoTemp2);
  cublasDestroy(handle);
}


__host__ void raft_rings_gpu_block(raft_ring *plan, 
				   float *dblockSino, 
				   int nrays, 
				   int nviews,
				   int blockSize)
{
  int kernelIdx;
  float *dtemp;
 
  cudaMalloc((void **) &dtemp, nrays * nviews * blockSize * sizeof(float));
  
  /*-------------
    m = 0 & n = 0
    -------------*/
  
  kernelIdx = 0;

  raft_rings_plan_set(plan, 0, 0);
  
  ringAtKernel_gpu_block(plan, dblockSino, nrays, nviews, blockSize, kernelIdx );
   
  /*--------------
    m = 1 & n = 0 
    -------------*/

  kernelIdx = 1;
  
  raft_rings_plan_set(plan, 1, 0);
  
  ringAtKernel_gpu_block(plan, dblockSino, nrays, nviews, blockSize, kernelIdx );

  /*--------------------
    Find min value of: 
    
    blocksino + kernel0
    blockSino + kernel1
    -------------------*/
  /*
  dim3 ThreadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil(nviews/ThreadsPerBlock.x)+1,(int)ceil(nrays/ThreadsPerBlock.y)+1,(int)ceil(blockSize/ThreadsPerBlock.z)+1);
  
  kernel_add_block<<<gridBlock, ThreadsPerBlock>>>(dblockSino, plan->block.cgm_x[1], nrays, nviews, blockSize);
  */
  
  int argmin;
  dim3 ThreadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil(nrays/ThreadsPerBlock.x)+1,(int)ceil(nviews/ThreadsPerBlock.y)+1,(int)ceil(blockSize/ThreadsPerBlock.z)+1);
  cublasHandle_t handle;
  float alpha = 1.5;
  
  cublasCreate(&handle);
  
  cudaMemcpy(dtemp, dblockSino, nviews * nrays *blockSize * sizeof(float),cudaMemcpyDeviceToDevice);
  
  kernel_add_block<<<gridBlock, ThreadsPerBlock>>>(dtemp, plan->block.cgm_x[0], nrays, nviews, blockSize);
  
  kernel_add_block<<<gridBlock, ThreadsPerBlock>>>(dblockSino, plan->block.cgm_x[1], nrays, nviews, blockSize);
  
  kernel_pointwiseProduct_block<<<gridBlock, ThreadsPerBlock>>>(dblockSino, dtemp, nrays, nviews, blockSize);
  
  cublasIsamin(handle, nrays*nviews*blockSize, dblockSino, 1, &argmin);

  kernel_sino_update_geometric_mean_block<<<gridBlock, ThreadsPerBlock>>>(dblockSino, alpha, argmin, nrays, nviews, blockSize);
  
  checkCudaErrors(cudaPeekAtLastError());
  
  cublasDestroy(handle);  
  cudaFree(dtemp);  
  
}

/*------------------------------------------------------------------------------

  VERSION WITH PTHREADS

  -----------------------------------------------------------------------------*/

/*
typedef struct{
  
  raft_ring plan;
  float *sinogram;
  int nrays, nviews;
  int nthread;
   
}param_t;


void *kernel_processOneRingFiltering(void *t)
{
  param_t *param;

  param = (param_t *)t;
  
  //raft_rings(param->plan, param->sinogram, param->nrays,  param->nviews);
  raft_rings_gpu(&param->plan, param->sinogram, param->nrays,  param->nviews);

  pthread_exit(NULL);
}

__host__ void raft_rings_gpu_block_threads(raft_ring *plan, 
					   float *blockSino, 
					   int nrays, 
					   int nviews,
					   int blockSize)
{
  pthread_attr_t attr;
  pthread_t *thread;
  int n, rc;    
  param_t *params; 
  
  thread = (pthread_t *)malloc(sizeof(pthread_t) * blockSize);
  params = (param_t *)malloc(sizeof(param_t) * blockSize);
  

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  for(n = 0; n < blockSize; n++) 
    {
      params[n].plan = raft_rings_plan_create(nrays, nviews, 1);
      params[n].nrays = nrays;
      params[n].nviews = nviews;
      params[n].nthread = n; 
      
      params[n].sinogram = (float *)malloc(sizeof(float)*nrays*nviews);
      
      memcpy( params[n].sinogram, &blockSino[n*nrays*nviews], sizeof(float)*nrays*nviews );
      
      pthread_create(&thread[n], 
		     NULL, 
		     kernel_processOneRingFiltering, 
		     (void *)&params[n]);
    }
  
  pthread_attr_destroy(&attr);
  for(n = 0; n < blockSize; n++) 
    {
      rc = pthread_join(thread[n], NULL);

      memcpy( &blockSino[n*nrays*nviews], params[n].sinogram, sizeof(float)*nrays*nviews );
      
      free(params[n].sinogram);
      raft_rings_plan_destroy(&params[n].plan);
    }  

  rc++;
  free(thread);
  free(params);
}
*/
