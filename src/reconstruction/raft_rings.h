#ifndef RAFT_RINGS_H
#define RAFT_RINGS_H

#include "../common/raft_types.h"

/*
#ifdef __cplusplus
extern "C" {
#endif
*/

void raft_rings(raft_ring plan, 
		float *sino, 
		int nrays, 
		int nviews);

void raft_rings_kernel(raft_tomo *workspace,
		       int b,
		       float *sino);

void raft_rings_blocks(raft_ring plan, 
		       float *sino, 
		       int nrays, 
		       int nviews, 
		       int nblocks);

raft_ring raft_rings_plan_create(int nrays, 
				 int nviews,
				 int blocksize);

void raft_rings_plan_destroy(raft_ring *plan);

void raft_rings_plan_set(raft_ring *plan, int m, int n);



__host__ void raft_rings_gpu(raft_ring *plan, 
			     float *sino, 
			     int nrays, 
			     int nviews);

__host__ void raft_rings_gpu_block(raft_ring *plan, 
				   float *blockSino, 
				   int nrays, 
				   int nviews,
				   int blockSize);


__host__ void raft_rings_gpu_block_threads(raft_ring *plan, 
					   float *blockSinxo, 
					   int nrays, 
					   int nviews,
					   int blockSize);


/*
#ifdef __cplusplus
} // exterc "C" {
#endif 
*/

#endif // RAFT_RINGS_H

