#ifndef RAFT_BACKPROJECTION_SLANTSTACK_H
#define RAFT_BACKPROJECTION_SLANTSTACK_H

#include "../common/raft_types.h"

__host__ void raft_backprojection_slantstack_gpu(float *blockBack,
						 float *blockSino,
						 raft_tomo *workspace);


__host__ void raft_allocate_backprojection_gpu_memory(raft_tomo* workspace);
__host__ void raft_deallocate_backprojection_gpu_memory(raft_tomo* workspace);


__host__ void raft_backprojection_slantstack_gpu_dev2host(float *blockBack,
							  float *blockSinoDev,
							  raft_tomo *workspace);
  

#endif // RAFT_BACKPROJECTION_SLANTSTACK_H
