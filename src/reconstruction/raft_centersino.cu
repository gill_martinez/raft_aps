#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "raft_centersino.h"
#include "../common/raft_types.h"

/*----------------------------------------------
  Shifting of a signal using FFT:

  signal: y1(t), y2(t)
  output: y1(t - c), y2(t + c)
  ----------------------------------------------*/

__host__ float translateSignal_(raft_tomo *workspace,
		 	        float *output1,
			        float *signal1,
			        float *output2,
			        float *signal2,
			        float c)
{
  float dw,w,wc,scale;
  int j,i,half, norm;

  scale = 1.0/((float)(workspace->Nrays));
  half = workspace->Nrays/2;
  wc = 1.0/(2*(workspace->dt));
  dw = 2.0*wc/((float)(workspace->Nrays-1));

  for(i=0; i < workspace->Nrays; i++)
    {
      workspace->fourier.in[i][0] = signal1[i];
      workspace->fourier.in[i][1] = 0.0;

      workspace->fourier.in_[i][0] = signal2[i];
      workspace->fourier.in_[i][1] = 0.0;
      
      j = i - SIGN(i - half) * half;
      w = -wc + j * dw;
      workspace->fourier.fkernel[i][0] = cos(-2.0 * PI * c * w);
      workspace->fourier.fkernel[i][1] = sin( 2.0 * PI * c * w);
    }
  
  fftw_execute( workspace->fourier.plan_forw );
 
  fftw_execute( workspace->fourier.plan_forw_ );

  for (i = 0 ; i < workspace->Nrays; i++ )
    {
      float aa,bb,cc,dd;
  
      aa = workspace->fourier.out[i][0];
      bb = workspace->fourier.out[i][1];
      cc = workspace->fourier.fkernel[i][0];
      dd = workspace->fourier.fkernel[i][1];

      workspace->fourier.in[i][0] = aa*cc - bb*dd;
      workspace->fourier.in[i][1] = aa*dd + bb*cc;

      aa = workspace->fourier.out_[i][0];
      bb = workspace->fourier.out_[i][1];
      cc =   workspace->fourier.fkernel[i][0];
      dd = - workspace->fourier.fkernel[i][1];

      workspace->fourier.in_[i][0] = aa*cc - bb*dd;
      workspace->fourier.in_[i][1] = aa*dd + bb*cc;
    }
  
  // IFFT:
 
  fftw_execute( workspace->fourier.plan_back );
  
  fftw_execute( workspace->fourier.plan_back_ );

  // Signal translated: f(t) to f(t-c)! + norm(output1 - output2)

  norm = 0;

  for(i = 0; i < workspace->Nrays; i++)
  {
    float diff;
    output1[i] = workspace->fourier.out[i][0] * scale;
    output2[i] = workspace->fourier.out_[i][0] * scale;
    
    diff = workspace->fourier.out[i][0] * scale - workspace->fourier.out_[i][0] * scale ;

    norm += diff*diff;	
  }
 
  return sqrt(norm);
}

/*---------------------------------------------------

  Find center of rotation for a sinogram
  and return a new shifted sinogram, satisfying
  the consistency condition

  sino(theta, -t) = sino(theta + pi, t)

  ---------------------------------------------------*/

__host__ float raft_find_center(raft_tomo *workspace, 
				float *sino)
{
  /*
  int shift, max, j, i, Nrays, Nangles;
  
  Nrays   = workspace->Nrays;
  Nangles = workspace->Nangles; 

  ////////////////////////////////////////////////////
  // A: last projection of input sinogram
  // A[i] = FMATRIX(sino, Nangles-1, i, Nrays);
  // Python: A = sino[:, 0] 
  //
  // B: flipped first projection of input sinogram
  // B[i] = FMATRIX(sino, 0, Nrays-1-i, Nrays);
  // Python: B = flipud( sino[:,Nangles-1] )
  //
  ////////////////////////////////////////////////////
  //////// Phase correlation with FFT's
  
  float norm0, norm180;
  max_of_projection(&j, &norm0, sino, Nrays, Nangles, 0);
  max_of_projection(&j, &norm180, sino, Nrays, Nangles, Nangles-1);

  norm0 = 1;
  norm180 = 1;

  for(i=0; i < Nrays; i++){
    workspace->A[i] = sino[ (Nangles-1)*Nrays + i ];
    workspace->B[i] = sino[ Nrays-1-i ];  
    
    workspace->fourier.in[i][0] = sino[ (Nangles-1)*Nrays + i]/norm180;
    workspace->fourier.in[i][1] = 0.0;
  }

  /////////////////////
  // Operation: 
  // out <- FFT[A] 
  
  fftw_execute( workspace->fourier.plan_forw );

  /////////////////////
  // Operation:
  // kernel <- FFT[A],  
  // in <- flipud( B ) = flipud(flipud(sino[:,0])) = sino[:,0]
  
  for(i=0; i < Nrays; i++){
    workspace->fourier.fkernel[i][0] = workspace->fourier.out[i][0];
    workspace->fourier.fkernel[i][1] = workspace->fourier.out[i][1];
    
    workspace->fourier.in[i][0] = sino[ i ]/norm0;
    workspace->fourier.in[i][1] = 0.0;
  }
  
  /////////////////////
  // out <- FFT[B]
  fftw_execute( workspace->fourier.plan_forw );
  
  ///////////////////////////////////////
  // Operation:  (i complex unity)
  //
  // in = FFT[A] . * conj FFT[B]   
  //    = kernel . * conj [ out ] 
  //    = (a+ib) . *  conj (c + id)
  //    = (ac + bd) + i (bc - ad)
  
  for (i = 0 ; i < Nrays; i++ ){
    float a,b,c,d,w;
    
    a = workspace->fourier.fkernel[i][0];
    b = workspace->fourier.fkernel[i][1];
    c = workspace->fourier.out[i][0];
    d = workspace->fourier.out[i][1];
    
    w =  sqrt( SQUARE((a*c + b*d)) + SQUARE((b*c - a*d)) );
    //w =  1.0;

    workspace->fourier.in[i][0] = (a*c + b*d)/w;
    workspace->fourier.in[i][1] = (b*c - a*d)/w;
  }
  
  /////////////////////////////
  // IFFT:
  // Output is not fft-shifted!

  fftw_execute( workspace->fourier.plan_back );

  ///////////////////////////////////////////
  /// Find maximum argument of correlation

  // fftshift: imag(out) = fftshift[ re(out) ] 
  for (i = 0; i < Nrays; i++)
    workspace->fourier.out[i][1] = workspace->fourier.out[(Nrays/2 + i)%Nrays][0];
    
  max = ( workspace->fourier.out[0][1] ); 
  shift = 0;
 
  for (i = 1; i < Nrays; i++){
    if ( ( workspace->fourier.out[i][1] ) > max )
      {
	max = ( workspace->fourier.out[i][1] );
	shift = i;
      }
  }

  shift = Nrays/2-shift;
  */

  /////////////////////////////////////////////////////////
  //////// Search for optimal shift (sub-pixel precision)
  //////// within the interval [-0.15,0.15] using 
  //////// Size=searchForCenter (within workspace)
  //////// points between them.
  ////////
  //////// Remark: correlation sometimes fails
  ////////         this is a brute-force algorithm
  ////////         using FFT to translate the signal
  ////////
  ////////
    
  int i, shift, Nangles, Nrays, m, Size;
  float dbeta, betamax, betamin, c, value, beta, norm;
  float dx;
  int numberOfPixelsToSearch = 1;

  Nrays   = workspace->Nrays;
  Nangles = workspace->Nangles; 
  
  for(i=0; i < Nrays; i++){
    workspace->A[i] = sino[ (Nangles-1)*Nrays + i ];
    workspace->B[i] = sino[ Nrays-1-i ];  
  }
  Size    = workspace->searchForCenter;
  
  //comment: top left & bottom right of sinograms can be any number
  dx = (2.0)/((float)(Nrays-1));  
  betamin = -1.0 + (Nrays/2 + workspace->offset - numberOfPixelsToSearch) * dx;
  betamax = -1.0 + (Nrays/2 + workspace->offset + numberOfPixelsToSearch) * dx;
    

  dbeta = (betamax-betamin)/((float)Size);

  for( m = 0; m < Size; m++)
    {
      c = betamin + m * dbeta;
      norm = translateSignal_(workspace, workspace->TB, workspace->B, workspace->TA, workspace->A, c);
         	
      norm = 0;
      for (i=0; i < Nrays; i++)
	norm += SQUARE((workspace->TA[i]-workspace->TB[i]));
      
      workspace->shift[m] = sqrt(norm);
    }
  
  value = workspace->shift[0];

  shift = 0;
  for (m = 0; m < Size; m++)
    {
      if ( (workspace->shift[m] < value) )
	{
	  value = workspace->shift[m];
	  shift = m;	  
	}
    }

  beta = betamin + shift * dbeta;

  /**/
  
  return -beta;
}

/*------------------------------------------------------
  Kernel: Pre-processing of block of sinograms
  ------------------------------------------------------*/

__global__ void kernel_pp_blocksino_gpu(cufftComplex *block, 
					float *dcenterblock,
					float regularization,
					float dt,
					int Nrays,
					int Nangles,
					int blockSize)
{
  int tx = blockIdx.x * blockDim.x + threadIdx.x;
  int ty = blockIdx.y * blockDim.y + threadIdx.y;
  int tz = blockIdx.z * blockDim.z + threadIdx.z;
  int TX;  //fourier shift

  float sigmaMax = 1/(2*dt);
  float delta_sigma = 2*sigmaMax/((float) Nrays-1); 
  float RegFBP;

  if ( (tx < Nrays) && (ty < Nangles) && (tz < blockSize) )
  {
    TX = ((tx + (Nrays/2)) % Nrays);
    float sigma = -sigmaMax + tx * delta_sigma;
    int offset = tz*Nangles*Nrays + ty*Nrays + TX;
        
    /*get real and complex values*/
    float a = block[offset].x;
    float b = block[offset].y; 
    float c, d, cos, sin, beta;
    
    beta = dcenterblock[tz];
    
    __sincosf( 2*PI*beta*sigma, &sin, &cos );
    
    RegFBP = 1.0 / (1 + (4*(regularization)*SQUARE(PI*sigma)) );

    c = RegFBP * cos;
    d = RegFBP * sin;
        
    /*set real and complex data */
    block[offset].x = ( a*c - b*d ) / ((float) Nrays);
    block[offset].y = ( a*d + b*c ) / ((float) Nrays);    

    // Division by Nrays: Fourier defition from CUFFT
    //                    without 1/N
  }
}


__global__ void kernel_pp_set_fourier_sino(cufftComplex *block, 
					   float *array, 
					   int Nangles, 
					   int Nrays,
					   int blockSize)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  int voxel;
  
  if( (tx< Nrays) && (ty<Nangles) && (tz < blockSize) )
    {
      voxel = tz*Nrays*Nangles+(ty*Nrays)+tx;
      
      block[ voxel ].x = array[ voxel ];
      block[ voxel ].y = 0;
    }

}


__global__ void kernel_pp_get_real(float *array,
				   cufftComplex *block, 
				   int Nangles, 
				   int Nrays,
				   int blockSize)
{
  int tx = threadIdx.x + blockIdx.x*blockDim.x; 
  int ty = threadIdx.y + blockIdx.y*blockDim.y; 
  int tz = threadIdx.z + blockIdx.z*blockDim.z;
  int voxel;

  if( (tx< Nrays) && (ty<Nangles) && (tz < blockSize) )
    {
      voxel = tz*Nrays*Nangles+(ty*Nrays)+tx;

      array[ voxel ] = block[voxel].x;
    }
}



/*------------------------------------------------------------
  This function execute the following process:
   
  sino = sino( t - c ) convoluted with K

  Fourier(K) = 1/(1 + 4pi*reg*w^2) 

  where 'reg' is a regularization parameter and 'c' an
  offset making 'sino' a correct sinogram.
  ---------------------------------------------------------*/

__host__ void raft_recentersino_withregularization_gpu(float *blockSino,
						       raft_tomo *workspace)
{
  int sliceFinal= workspace->sliceFinal;
  int delta     = MIN(sliceFinal,workspace->ls) - workspace->li;
  float *centerblockDev;
 
  struct timespec TimeStart, TimeEnd;

  int Nrays, Nangles;
  if(workspace->hybrid==1)
    {
      Nrays   = 2*workspace->axis;
      Nangles = (int) workspace->measured_nangles/2;	
    }
  else
    {
      Nrays   = workspace->measured_nrays + 2*workspace->zpad;
      Nangles = workspace->measured_nangles;
  }
  
  float *blockSinoDev;
  int voxels = Nrays * Nangles * delta;
  checkCudaErrors( cudaMalloc((void**)&blockSinoDev, voxels*sizeof(float)) );
  checkCudaErrors( cudaMalloc((void **)&centerblockDev, sizeof(float)*delta) );
  
  //cudaStream_t stream;
  //cudaStreamCreate(&stream);

  dim3 threadsPerBlock(TPBX,TPBY,TPBZ);
  dim3 gridBlock((int)ceil((Nrays)/threadsPerBlock.x)+1,
		 (int)ceil((Nangles)/threadsPerBlock.y)+1,
		 (int)ceil(delta/threadsPerBlock.z)+1);
  
  clock_gettime(CLOCK, &TimeStart);

  //--- Set imaginary part to zero: C2C FFT
  
  cudaMemcpy(blockSinoDev, blockSino, delta*(Nrays)*(Nangles)*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(centerblockDev, workspace->centerblock, delta*sizeof(float), cudaMemcpyHostToDevice);
  
  kernel_pp_set_fourier_sino<<<gridBlock, threadsPerBlock>>>(workspace->fourier.SinoCU, 
							     blockSinoDev, 
							     Nangles, 
							     Nrays,
							     delta);
  
  checkCudaErrors(cudaPeekAtLastError());
  
  //--- FFT: block of sinograms
  
  if( cufftExecC2C(workspace->fourier.SinoCUplan, 
		   workspace->fourier.SinoCU, 
		   workspace->fourier.SinoCU, CUFFT_FORWARD) != CUFFT_SUCCESS)
  {
    fprintf(stderr,"RafT Error: CUFFT error (Low-pass). ExecR2C forward failed\n ");
    return;
  }
  
  //--- Multiplication in the frequency domain: shift from centersino + regularization factor
  
  fprintf(stderr,"==>%d \n\n",Nrays);

  float dt = (workspace->tmax - workspace->tmin)/((float) Nrays-1); 

  kernel_pp_blocksino_gpu<<<gridBlock, threadsPerBlock>>>(workspace->fourier.SinoCU, 
							  centerblockDev, 
							  workspace->reg, dt,
							  Nrays,Nangles,delta);
  
  checkCudaErrors(cudaPeekAtLastError());

  //--- IFFT: block of sinograms

  if( cufftExecC2C(workspace->fourier.SinoCUplan, 
		   workspace->fourier.SinoCU, 
		   workspace->fourier.SinoCU, CUFFT_INVERSE ) != CUFFT_SUCCESS)
  {
    fprintf(stderr,"RafT Error: CUFFT error (Low-pass). ExecR2C forward failed\n ");
    return;
  }
 
  kernel_pp_get_real<<<gridBlock, threadsPerBlock>>>(blockSinoDev,
						     workspace->fourier.SinoCU,
						     Nangles, 
						     Nrays,
						     delta);

  checkCudaErrors(cudaPeekAtLastError());
  
  clock_gettime(CLOCK, &TimeEnd);
  if(workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Execute Recentering kernel",TIME(TimeEnd,TimeStart),workspace->nblock);  
  
  //copying data Device to Host
  
  clock_gettime(CLOCK, &TimeStart);
    
  cudaMemcpy(blockSino, 
	     blockSinoDev, 
	     delta*(Nrays)*(Nangles)*sizeof(float),
	     cudaMemcpyDeviceToHost);
  
  clock_gettime(CLOCK, &TimeEnd);
  if( workspace->printTimingK)	
    fprintf(stderr,"\n\t%lf\t|%d-->Copy Filtered block: Device to host",TIME(TimeEnd,TimeStart),workspace->nblock);  
  
  cudaFree(blockSinoDev);
  cudaFree(centerblockDev);
  
  //cudaStreamSynchronize(stream);
  //cudaStreamDestroy(stream);
}



