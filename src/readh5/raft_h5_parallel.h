#ifndef RAFT_H5_PARALLEL_H
#define RAFT_H5_PARALLEL_H

#include "../common/raft_workspace.h"
#include "../common/raft_types.h"


__host__ void raft_read_tomo_block(raft_tomo *workspace);


__host__ void raft_read_flat(raft_tomo *workspace,
			     char *path);


__host__ void raft_read_dark(raft_tomo *workspace,
			     char *path);


__host__ int number_of_blocks(int qtdSlices,
			      int blockSize);


#endif // RAFT_H5_PARALLEL_H
