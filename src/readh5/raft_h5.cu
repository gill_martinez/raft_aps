#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <hdf5.h>

#include "../common/raft_types.h"
#include "../common/raft_workspace.h"

extern "C" {
#include <time.h>
}



/*------------------------------
  Read counts from HDF file
  ------------------------------*/

__host__ void raft_read_tomo_block(raft_tomo *workspace)
{
  hsize_t dims_out[3];
  hsize_t start[3],count[3];
  hsize_t offset_out[3],count_out[3];
  hid_t dataspace,memspace,dataset;
  int slice;
  int Nangles,Nrays;
  int sliceInit  = workspace->sliceInit;
  int sliceFinal = workspace->sliceFinal;
  int blockSize;
  
  struct timespec TimeStart, TimeEnd;

  dataspace = workspace->dataspace;
  dataset   = workspace->dataset;
  Nangles   = workspace->Nangles;
  Nrays     = workspace->Nrays;
  blockSize = workspace->blockSize;
  
  int offset = 0;
  int delta  = MIN(sliceFinal,workspace->ls) - workspace->li; 

  /*---------------------
    define and select the 
    hyperslab to use for 
    reading 
    --------------------*/
  
  /*----------------------
    loop to get the slices
    ---------------------*/
  
  for(slice=0;slice<delta;slice++)
    {
      /* --------------------
	 start indicates the 
	 offset beginning in 
	 each dimension
	 -------------------*/

      start[0] = 0;
      start[1] = sliceInit + (workspace->nblock*blockSize) + slice;
      start[2] = 0;
      
      /* ------------------------
	 count indicates how many 
	 elements will be selected 
	 in each dimension 
	 ----------------------*/
      
      count[0] = Nangles;
      count[1] = 1;
      count[2] = Nrays;
      
      /* ---------------------
	 This function selects 
	 the stated hyperslab 
	 --------------------*/
      
      H5Sselect_hyperslab(dataspace,H5S_SELECT_SET,start,NULL,count,NULL);
      
      /*-------------------------
	define the dimensions of 
	sinogram
	------------------------*/
      
      dims_out[0] = Nrays;
      dims_out[1] = Nangles;
      memspace = H5Screate_simple(2,dims_out,NULL);
      
      /* -----------------------
	 define the final image
	 (sinogram) offset 
	 ----------------------*/
      
      offset_out[0] = 0;
      offset_out[1] = 0;
      
      /*----------------------------
	count_out indicates how many 
	elements will be transferred 
	in each dimension 
	---------------------------*/

      count_out [0] = Nrays;
      count_out [1] = Nangles;
      H5Sselect_hyperslab(memspace,H5S_SELECT_SET,offset_out,NULL,count_out,NULL);
      
      clock_gettime(CLOCK, &TimeStart);

      H5Dread(dataset,
	      H5T_NATIVE_FLOAT,
	      memspace,
	      dataspace,
	      H5P_DEFAULT,
	      workspace->tomo + (offset*Nangles*Nrays));
      
      clock_gettime(CLOCK, &TimeEnd);
      if(workspace->printTimingK)	
	fprintf(stderr,"\n\t%lf\t|%d-->H5Dread function (slice %d)",TIME(TimeEnd,TimeStart),workspace->nblock,slice);
 	      
      offset++;
      
    }
  
  //status = H5Sclose(memspace);
}

/*------------------------------
  Read Flat HDF File
  -------------------------------*/

__host__ void raft_read_flat(raft_tomo *workspace, char *path)
{
  
  hid_t file,dataset,memspace;
  hid_t dataspace;
  hsize_t dims_in[3],dims_out[2];
  hsize_t start[3],count[3];
  hsize_t offset_out[2],count_out[2];
  int Nrays,Nslices;
  char path_flat[100];

  /* Get path_flat */

  strcpy(path_flat,path);
  strcat(path_flat,"tomo_flat_before.h5");
  
  /* Open HDF5 file*/
  file = H5Fopen(path_flat,H5F_ACC_RDONLY,H5P_DEFAULT);

  /* Open dataset*/
  dataset = H5Dopen(file,DATAFLAT,H5P_DEFAULT);
  
  /* Get dataspace */

  dataspace = H5Dget_space(dataset);

  /* Get Number of dimensions */

  H5Sget_simple_extent_ndims(dataspace);
  H5Sget_simple_extent_dims(dataspace,dims_in,NULL);

  Nrays   = dims_in[1]; //Nrays
  Nslices = dims_in[2]; //Nslices
 
  /* Define and select the hyperslab to use for reading */
  
  /* Select which slice will be transferred */
    
  /* start indicates the offset beginning in each dimension */
  start[0] = 0;
  start[1] = 0;
  start[2] = 0;
  
  /* Count indicates how many elements will be selected in each dimension */
  count[0] = 1;
  count[1] = Nrays;
  count[2] = Nslices;
  
  /* this function selects the stated hyperslab */
  H5Sselect_hyperslab(dataspace,H5S_SELECT_SET,start,NULL,count,NULL);
  
  /* define the dimensions of sinogram*/
  dims_out[0] = Nrays;
  dims_out[1] = Nslices;
  memspace = H5Screate_simple(2,dims_out,NULL);
  
  /* define the final image(sinogram) offset */
  offset_out[0] = 0;
  offset_out[1] = 0;
  
  /* count_out indicates how many elements will be transferred in each dimension */
  count_out [0] = Nrays;
  count_out [1]	= Nslices;
  
  H5Sselect_hyperslab(memspace,H5S_SELECT_SET,offset_out,NULL,count_out,NULL);
  
  /* alloc data to flat sinogram */
  
  H5Dread(dataset,H5T_NATIVE_FLOAT,memspace,dataspace,H5P_DEFAULT, workspace->flatBefore);
  
  /* close dataset,dataspace,memspace,file */
  H5Dclose (dataset);
  H5Sclose (dataspace);
  H5Sclose(memspace);
  H5Fclose (file);

  //////////////////////////////////////////////////////////////////////
  ///////// FLAT AFTER

    /* Get path_flat */

  strcpy(path_flat,path);
  strcat(path_flat,"tomo_flat_after.h5");
  
  /* Open HDF5 file*/
  file = H5Fopen(path_flat,H5F_ACC_RDONLY,H5P_DEFAULT);

  /* Open dataset*/
  dataset = H5Dopen(file,DATAFLAT,H5P_DEFAULT);
  
  /* Get dataspace */

  dataspace = H5Dget_space(dataset);

  /* Get Number of dimensions */

  H5Sget_simple_extent_ndims(dataspace);
  H5Sget_simple_extent_dims(dataspace,dims_in,NULL);

  Nrays   = dims_in[1]; //Nrays
  Nslices = dims_in[2]; //Nslices
 
  /* Define and select the hyperslab to use for reading */
  
  /* Select which slice will be transferred */
    
  /* start indicates the offset beginning in each dimension */
  start[0] = 0;
  start[1] = 0;
  start[2] = 0;
  
  /* Count indicates how many elements will be selected in each dimension */
  count[0] = 1;
  count[1] = Nrays;
  count[2] = Nslices;
  
  /* this function selects the stated hyperslab */
  H5Sselect_hyperslab(dataspace,H5S_SELECT_SET,start,NULL,count,NULL);
  
  /* define the dimensions of sinogram*/
  dims_out[0] = Nrays;
  dims_out[1] = Nslices;
  memspace = H5Screate_simple(2,dims_out,NULL);
  
  /* define the final image(sinogram) offset */
  offset_out[0] = 0;
  offset_out[1] = 0;
  
  /* count_out indicates how many elements will be transferred in each dimension */
  count_out [0] = Nrays;
  count_out [1]	= Nslices;
  
  H5Sselect_hyperslab(memspace,H5S_SELECT_SET,offset_out,NULL,count_out,NULL);
  
  /* alloc data to flat sinogram */
  
  H5Dread(dataset,H5T_NATIVE_FLOAT,memspace,dataspace,H5P_DEFAULT, workspace->flatAfter);
  
  /* close dataset,dataspace,memspace,file */
  H5Dclose (dataset);
  H5Sclose (dataspace);
  H5Sclose(memspace);
  H5Fclose (file);
}

/*------------------------------
  Read Dark HDF File
  -------------------------------*/

__host__ void raft_read_dark(raft_tomo *workspace, char *path)
{
  hid_t file,dataset,memspace;
  hid_t dataspace;
  hsize_t dims_in[3],dims_out[2];
  hsize_t start[3],count[3];
  hsize_t offset_out[2],count_out[2];
  int Nrays,Nslices;
  char path_dark[100];
  
  /* get path_dark */
  strcpy(path_dark,path);
  strcat(path_dark,"tomo_dark_before.h5");
  
  /*open HDF5 file*/
  file    = H5Fopen(path_dark,H5F_ACC_RDONLY,H5P_DEFAULT);
  /*open dataset */
  dataset = H5Dopen(file,DATADARK,H5P_DEFAULT);
  
  /*get dataspace*/
  dataspace = H5Dget_space(dataset);
  /*get Number of dimensions*/
  H5Sget_simple_extent_ndims(dataspace);
  H5Sget_simple_extent_dims(dataspace,dims_in,NULL);
 
  Nrays   = dims_in[1]; //Nrays
  Nslices = dims_in[2]; //Nslices
 
  /* define and select the hyperslab to use for reading */
  
  /* select which slice will be transferred */
  int slice = 0;
  
  /* start indicates the offset beginning in each dimension */
  start[0] = 0;
  start[1] = slice;
  start[2] = 0;
  
  /* count indicates how many elements will be selected in each dimension */
  count[0] = 1;
  count[1] = Nrays;
  count[2] = Nslices;
  
  /* this function selects the stated hyperslab */
  H5Sselect_hyperslab(dataspace,H5S_SELECT_SET,start,NULL,count,NULL);
  
  /* define the dimensions of sinogram*/
  dims_out[0] = Nrays;
  dims_out[1] = Nslices;
  memspace = H5Screate_simple(2,dims_out,NULL);
  
  /* define the final image(sinogram) offset */
  offset_out[0] = 0;
  offset_out[1] = 0;
  
  /* count_out indicates how many elements will be transferred in each dimension */
  count_out [0] = Nrays;
  count_out [1]	= Nslices;
  
  H5Sselect_hyperslab(memspace,H5S_SELECT_SET,offset_out,NULL,count_out,NULL);
  
  H5Dread(dataset,H5T_NATIVE_FLOAT,memspace,dataspace,H5P_DEFAULT, workspace->dark);
  
  /* close dataset,dataspace,memspace,file */
  H5Dclose (dataset);
  H5Sclose (dataspace);
  H5Sclose(memspace);
  H5Fclose (file);
}

/* ---------------------------------------------------------
   Find number of blocks, given a blockSize. This function 
   calculates how many blocks are needed to process all data,
   based on number of slices and block size.
   --------------------------------------------------------*/

__host__ int number_of_blocks(int qtdSlices,
			      int blockSize)
{
  int nblocks;
  int rest;
  
  rest = (qtdSlices%blockSize);
  
  if(qtdSlices>blockSize && rest!=0)
  {
    nblocks = (int) floor(qtdSlices/blockSize);
    nblocks = nblocks + 1;
  }
  else
    if(qtdSlices>blockSize && rest==0)
      nblocks = (int) floor(qtdSlices/blockSize);
  else
    nblocks = 1;
  
  return nblocks;
}
