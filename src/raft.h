#ifndef RAFT_H
#define RAFT_H
#define FAIL -1

//#include "readh5/raft_h5.h"
#include "readh5/raft_h5_parallel.h"
#include "reconstruction/raft_processblock.h"
#include "reconstruction/raft_backprojection_bst.h"
#include "reconstruction/raft_backprojection_slantstack.h"
#include "reconstruction/raft_centersino.h"
#include "reconstruction/raft_rings.h"
#include "reconstruction/raft_sinogram.h"
#include "reconstruction/raft_filters.h"

#include "common/raft_types.h"
#include "common/raft_save.h"
#include "common/raft_workspace.h"

#include "pipeline/raft_pipeline_processingFunctions.h"
#include "pipeline/raft_pipeline.h"

#endif // RAFT_H
